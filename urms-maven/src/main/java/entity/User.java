/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Class này quản lý các thông tin liên quan đến user trong cơ sở dữ liệu.
 * @author CoTrang-Lecture
 */
public class User extends DataAccessHelper{
    private final String GET_LOGIN = "select * from User where email=? and password=?";
    private final String SEARCH_USER_BY_EMAIL = "select * from User where email = ?";
    private final String ADD_USER = "INSERT INTO User (email,password,firstName,lastName,phoneNumber,gender,requestToChange,status,birthday) VALUES (?,?,?,?,?,?,?,?,?)";
    private final String ADD_USER_ROLE = "INSERT INTO UserRole (email,roleId) VALUES (?,?)";
    private final String SEARCH_USER = "select firstName,lastName,User.email,roleName,birthday,phoneNumber from User,Role,UserRole where User.email = UserRole.email and Role.roleId = UserRole.roleId";
    private final String UPDATE_PASS = "update User set password = ?,requestToChange=? where email = ?";
    private final String UPDATE_PROFILE ="update User set password = ?,email=?,gender=?,firstName=?,lastName=?,phoneNumber=?,birthday=?,requestToChange=?, status=? where email = ?";
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String birthDay;
    private String phoneNumber;
    private String gender;
    private String roleName;
    private boolean requestToChange;
    private boolean  status;
    /**
    * Hàm này để lấy ra roleName trong bảng User
    * @return String là roleName
    */ 
    public String getRoleName() {
        return roleName;
    }
    /**
    * Hàm này để gán giá trị cho roleName trong bảng User
    * @param roleName là roleName
    */ 
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    /**
    * Hàm này để lấy ra email trong bảng User
    * @return String là email
    */ 
    public String getEmail() {
        return email;
    }
    /**
    * Hàm này để gán giá trị cho email trong bảng User
    * @param email là email
    */ 
    public void setEmail(String email) {
        this.email = email;
    }
    /**
    * Hàm này để lấy ra password trong bảng User
    * @return String là password
    */ 
    public String getPassword() {
        return password;
    }
    /**
    * Hàm này để gán giá trị cho password trong bảng User
    * @param password là password
    */ 
    public void setPassword(String password) {
        this.password = password;
    }
    /**
    * Hàm này để lấy ra firstName trong bảng User
    * @return String là firstName
    */ 
    public String getFirstName() {
        return firstName;
    }
    /**
    * Hàm này để gán giá trị cho firstName trong bảng User
    * @param firstName là firstName
    */ 
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
    * Hàm này để lấy ra lastName trong bảng User
    * @return String là lastName
    */ 
    public String getLastName() {
        return lastName;
    }
    /**
    * Hàm này để gán giá trị cho lastName trong bảng User
    * @param lastName là lastName
    */ 
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
    * Hàm này để lấy ra birthDay trong bảng User
    * @return String là birthDay
    */ 
    public String getBirthDay() {
        return birthDay;
    }
    /**
    * Hàm này để gán giá trị cho birthDay trong bảng User
    * @param birthDay là birthDay
    */ 
    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }
    /**
    * Hàm này để lấy ra phoneNumber trong bảng User
    * @return String là phoneNumber
    */ 
    public String getPhoneNumber() {
        return phoneNumber;
    }
    /**
    * Hàm này để gán giá trị cho phoneNumber trong bảng User
    * @param phoneNumber là phoneNumber
    */ 
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    /**
    * Hàm này để lấy ra gender trong bảng User
    * @return String là gender
    */ 
    public String getGender() {
        return gender;
    }
    /**
    * Hàm này để gán giá trị cho gender trong bảng User
    * @param gender là gender
    */ 
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
    * Hàm này để lấy ra requestToChange trong bảng User
    * @return boolean là requestToChange
    */ 
    public boolean getRequestToChange() {
        return requestToChange;
    }
    /**
    * Hàm này để gán giá trị cho requestToChange trong bảng User
    * @param requestToChange là requestToChange
    */ 
    public void setRequestToChange(boolean requestToChange) {
        this.requestToChange = requestToChange;
    }
    /**
    * Hàm này để lấy ra status trong bảng User
    * @return boolean là status
    */ 
    public boolean getStatus() {
        return status;
    }
    /**
    * Hàm này để gán giá trị cho status trong bảng User
    * @param status là status
    */ 
    public void setStatus(boolean status) {
        this.status = status;
    }
    /**
    *Hàm này kiểm tra xem email và pass word có khớp với csdl không
    * và kiểm tra các trạng thài khóa hoặc yêu cầu thay đổi pass của người dùng.
    * @param email là email người dùng
    * @param password là mật khẩu người dùng
    *@return int return là : 0. tài khoản bị khóa, 1 yêu cầu thay đổi pass
    * 2 là đúng, 3 là sai mật khẩu, 4 là email không tồn tại.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */     
    public int checkLogin(String email, String password) throws SQLException, ClassNotFoundException {
        if(findByEmail(email)){
            connectDB();
            PreparedStatement ps = conn.prepareStatement(GET_LOGIN);
            ps.setString(1, email);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs != null && rs.next()) {
                if(rs.getBoolean("status")){

                    closeDB();
                    return 0; // bị khóa
                }else if(rs.getBoolean("requestToChange")){
                    closeDB();
                    return 1; // yêu cầu thay đổi mật khẩu
                }else{
                    closeDB();
                    return 2; // đăng nhập thành công
                }
            }else{
                closeDB();
                return 3; // sai mật khẩu
            }
        }
        return 4; // email không tồn tại
    }
     /**
    * Chỉnh sửa mật khẩu của người dùng theo email và password mới
    * @param email là email người dùng
    * @param password là mật khẩu người dùng
    * @return boolean cho thấy có sửa được trong cá sở dữ liệu không, nếu không, do lỗi sql.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */        
    public void updatePassword(String email,String password) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(UPDATE_PASS);
        ps.setString(1, password);
        ps.setInt(2,0);
        ps.setString(3, email);
        ps.executeUpdate();
        closeDB();
    }
    
    /**
    *Thêm mới người dùng
    *@return boolean cho thấy có sửa được trong cá sở dữ liệu không, nếu không, do lỗi sql.
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */ 
    public boolean addUser() throws SQLException, ClassNotFoundException {
        connectDB();
        PreparedStatement ps;
        ps = conn.prepareStatement(SEARCH_USER_BY_EMAIL);
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        if (rs != null && rs.next()){
            closeDB();
            return false;
        }else{
            ps = conn.prepareStatement(ADD_USER);
            ps.setString(1, email);
            ps.setString(2, password);
            ps.setString(3, firstName);
            ps.setString(4, lastName);
            ps.setString(5, phoneNumber);
            ps.setString(6, gender);
            ps.setBoolean(7, requestToChange);
            ps.setBoolean(8, status);
            ps.setString(9, birthDay);
            ps.executeUpdate();
            ps = conn.prepareStatement(ADD_USER_ROLE);
            ps.setString(1, email);
            ps.setInt(2, 2);
            ps.executeUpdate();
        }
        closeDB();
        return true;
    }
        /**
    *Thêm mới người dùng bẳng 1 hoặc 1 vài trong các đầu vào đã cho
    * @param lastName tên
    * @param firstName họ
    * @param email  email
    * @param groupId vai trò
    * @return ArrayList<User> list user;
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */ 
    public ArrayList<User> searchUser(String lastName,String firstName, String email,int groupId) throws SQLException, ClassNotFoundException{
        ArrayList<User> users = new ArrayList<User>();
        ResultSet rs;
        String queryForLastName = " and lastName like ?";
        String queryForFirstName = " and firstName like ?";
        String queryForEmail = " and User.email like ?";
        String queryForGroupId = " and Role.roleId = ?";
        String query = SEARCH_USER;
        if(!lastName.isEmpty())
            query+=queryForLastName;
        if(!firstName.isEmpty())
            query+=queryForFirstName;
        if(!email.isEmpty())
            query+=queryForEmail;
        if(groupId!=0)
            query+=queryForGroupId;
        connectDB();
        PreparedStatement ps = conn.prepareStatement(query);
        int count=1;
        if(!lastName.isEmpty()){
            ps.setString(count, "%"+lastName+"%");
            count++;
        }
        if(!firstName.isEmpty()){
            ps.setString(count, "%"+firstName+"%");
            count++;
        }
        if(!email.isEmpty()){
            ps.setString(count, "%"+email+"%");
            count++;
        }
        if(groupId!=0)
            ps.setInt(count, groupId);
        rs = ps.executeQuery();
        while(rs.next()){
            User user = new User();
            user.setFirstName(rs.getString("firstName"));
            user.setLastName(rs.getString("lastName"));
            user.setEmail(rs.getString("email"));
            user.setPhoneNumber(rs.getString("phoneNumber"));
            user.setRoleName(rs.getString("roleName"));
            user.setBirthDay(rs.getString("birthday"));
            users.add(user);
        }
        closeDB();
        return users;
    }
    /**
    *Tìm người dùng theo email
    * @param email email
    *@return boolean nếu user tồn tại return true, ngược lại thì return false;
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */ 
    public boolean findByEmail(String email) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(SEARCH_USER_BY_EMAIL);
        ps.setString(1, email);
        ps.executeQuery();
        ResultSet rs = ps.executeQuery();
        if(rs !=null && rs.next()){
            this.setFirstName(rs.getString("firstName"));
            this.setLastName(rs.getString("lastName"));
            this.setEmail(rs.getString("email"));
            this.setPhoneNumber(rs.getString("phoneNumber"));
            this.setBirthDay(rs.getString("birthday"));
            this.setPassword(rs.getString("password"));
            this.setGender(rs.getString("gender"));
            closeDB();
            return true;
        }
        closeDB();
        return false;
        
    }
    
    public void updateProfile(String oldEmail) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(UPDATE_PROFILE);
        ps.setString(1, password);
        ps.setString(2, email);
        ps.setString(3, gender);
        ps.setString(4, firstName);
        ps.setString(5, lastName);
        ps.setString(6, phoneNumber);
        ps.setString(7, birthDay);
        ps.setBoolean(8, requestToChange);
        ps.setBoolean(9, status);
        ps.setString(10, oldEmail);
        ps.executeUpdate();
        closeDB();
    }
}
