/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class này quản lý các thông tin liên quan đến RoleFunction trong cơ sở dữ liệu
 * @author CoTrang-Lecture
 */
public class RoleFunction extends DataAccessHelper {
    private int functionId,roleId;
    private final String ADD = "insert into RoleFunction (functionId, roleId) values(?,?)";
    private final String DELETE = "delete from RoleFunction where functionId=? and roleId = ?";
    private final String DELETE_BY_FUNCTION_ID ="delete from RoleFunction where functionId=?";
    public static final String GET_BY_FUNCTION_ID_NOT_IN_ROLE  ="select roleId from RoleFunction where functionId=? and roleId <> ?";
    public static final String GET_BY_FUNCTION_ID_IN_ROLE="select roleId from RoleFunction where functionId=? and roleId = ?";
    /**Hàm này để xóa các bản ghi có functionId tương ứng trong bảng RoleFunction
     * @param functionId  là id của function
     * true nếu không có lỗi
     * false nếu có lỗi trong cơ sở dữ liệu
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    public void deleteByFunctionId(int functionId) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(DELETE_BY_FUNCTION_ID);
        ps.setInt(1, functionId);
        ps.executeUpdate();
        closeDB();
    }
    /**
     * Hàm này để lấy roleId theo functionId
     * @param functionId là id của function
     * @return int là roleId
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    public int getRoleIdByFunctionId(int functionId,int roleId,String query) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, functionId);
        ps.setInt(2,roleId);
        ResultSet rs = ps.executeQuery();
        RoleFunction roleFunction = new RoleFunction();
        while(rs.next()){
            roleFunction.setRoleId(rs.getInt("roleId"));
            
        }
//        System.out.println("Role id "+rs.getInt("roleId"));
        closeDB();
        return roleFunction.getRoleId();
    }
    /**
     *Hàm này để thêm 1 bản ghi vào bảng RoleFunction
     * true nếu không có lỗi
     * false nếu có lỗi trong cơ sở dữ liệu
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */   
    public void add() throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(ADD);
        ps.setInt(1, functionId);
        ps.setInt(2, roleId);
        ps.executeUpdate();
        closeDB();
    }
    /**Hàm này để xóa các bản ghi có functionId và roleId tương ứng trong bảng RoleFunction
     * @param functionId  là id của function
     * @param roleId là id của vai trò
     * true nếu không có lỗi
     * false nếu có lỗi trong cơ sở dữ liệu
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */       
    public void delete(int functionId, int roleId) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(DELETE);
        ps.setInt(1, functionId);
        ps.setInt(2, roleId);
        ps.executeUpdate();
        closeDB();
    }
    /**
    * Hàm này để lấy ra functionId trong bảng RoleFunction
    * @return int là functionId
    */ 
    public int getFunctionId() {
        return functionId;
    }
    /**
    * Hàm này để gán giá trị cho functionId trong bảng RoleFunction
    * @param functionId là functionId
    */ 
    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }
    /**
    * Hàm này để lấy ra roleId trong bảng RoleFunction
    * @return int là roleId
    */ 
    public int getRoleId() {
        return roleId;
    }
    /**
    * Hàm này để gán giá trị cho roleId trong bảng RoleFunction
    * @param roleId là roleId
    */ 
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    
}
