/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package common;

import entity.DataAccessHelper;
import static entity.DataAccessHelper.conn;
import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Class này để đọc file xml vào lưu vào cơ sở dữ liệu test
 * @author CoTrang-Lecture
 */
public class ReadXML extends DataAccessHelper{
    /**
     * Hàm này để đọc file xml và đưa vào cơ sở dữ liệu
     * @param xmlPath là đường dẫn file xml cần đọc
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws ParserConfigurationException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */      
    public void readXmlStoreInDataBase(String xmlPath) throws ParserConfigurationException, SQLException, ClassNotFoundException{
        try {
            File file = new File(xmlPath);
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            if (doc.hasChildNodes()) {
		storeNodeToDatabase(doc.getChildNodes());
         }
        } catch (IOException | SAXException ex) {
            Logger.getLogger(ReadXML.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    /**
     * Hàm này để đưa dữ liệu của 1 node trong file xml vào cơ sở dữ liệu
     * @param nodeList là tất cả các node trong file xml
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */   
    private void storeNodeToDatabase(NodeList nodeList) throws SQLException, ClassNotFoundException {
    boolean isFirstNode=true;
    for (int count = 0; count < nodeList.getLength(); count++) {
	Node tempNode = nodeList.item(count);
	// make sure it's element node.
	if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                if(count!=0){
                    String sqlCmd= "insert into ";
                    // get node name and value
                    sqlCmd+= tempNode.getNodeName()+" (";
                    if (tempNode.hasAttributes()) {
                            // get attributes names and values
                            NamedNodeMap nodeMap = tempNode.getAttributes();
                            int len = nodeMap.getLength();
                            sqlCmd+=getAttributeNames(nodeMap,len);
                            sqlCmd+=getAttributeValues(nodeMap, len);

                    }
                    System.out.println(sqlCmd);
                    insertToDatabase(sqlCmd);
                }
                if (tempNode.hasChildNodes()) {
                    storeNodeToDatabase(tempNode.getChildNodes());
                }
	}
    }
  }
    
    /**
     * Hàm này chạy 1 câu lệnh sql
     * @param sqlCmd là câu lệnh sql
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */     
  public void insertToDatabase(String sqlCmd) throws SQLException, ClassNotFoundException{
    connectDB();
    PreparedStatement ps = conn.prepareStatement(sqlCmd);
    ps.executeUpdate();
    closeDB();
  }
/**
 * Hàm này để ghép tên các cột vào câu lệnh insert
 * @param nodeMap là node trong file xml
 * @param len là số lượng item trong nodeMap
 * @return String tên các cột sẽ được ghép
 */    
  private String getAttributeNames(NamedNodeMap nodeMap,int len){
    String sqlCmd="";
    for (int i = 0; i < len; i++) {
        Node node = nodeMap.item(i);
        if(i!=len-1)
            sqlCmd += node.getNodeName()+",";
        else
            sqlCmd += node.getNodeName()+") values (";
    }
    return sqlCmd;
  }
 /**
 * Hàm này để ghép dư liệu các cột vào câu lệnh insert
 * @param nodeMap là node trong file xml
 * @param len là số lượng item trong nodeMap
 * @return String dữ liệu các cột sẽ được ghép
 */    
  private String getAttributeValues(NamedNodeMap nodeMap,int len){
    String sqlCmd="";
    for(int i=0;i<len;i++){
        Node node = nodeMap.item(i);
        if(i!=len-1)
            sqlCmd += "'"+node.getNodeValue()+"'"+",";
        else
            sqlCmd += "'"+node.getNodeValue()+"'"+");";
    }      
    return sqlCmd;
  }
}
