/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package user;

import common.TestHelper;
import entity.User;
import user.boundary.RegisterForm;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class này để kiểm tra chức năng đăng ký
 * @author CoTrang-Lecture
 */
public class TestRegister extends TestHelper{
    private RegisterForm registerForm = new RegisterForm();
    
    /**
     *  Hàm này để khởi tạo test 
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
    @BeforeClass
    public static void setup() throws ParserConfigurationException, SQLException, ClassNotFoundException{
        new TestHelper().intiTest();
    }
    /**
     *  Hàm này để test chức năng đăng ký
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
    @Test
    public void testCheckRegister() throws SQLException, ClassNotFoundException{
        // Khi chưa có thông tin gì
        assertFalse(registerForm.checkRegister());
        // Khi chỉ đúng 1 điều kiện
        registerForm.setTxtEmail("Kien@gmail.com");
        assertFalse(registerForm.checkRegister());
        //Khi chỉ đúng 3 điều kiện
        registerForm.setTxtFirstName("AAAA");
        registerForm.setTxtPass("123456A@a");
        assertFalse(registerForm.checkRegister());
        //Sai 1 điều kiện
        registerForm.setTxtConfirmPass("123456A@");
        assertFalse(registerForm.checkRegister());
        //Đúng hết
        registerForm.setTxtConfirmPass("123456A@a");
        registerForm.setTxtLastName("ASSD");
        registerForm.setTxtPhoneNumber("123456789");
        registerForm.setCcBirthday(12,0,30,12,1994);
        registerForm.setCbbGender(1);
        assertTrue(registerForm.checkRegister());
    }
        /**
     *  Hàm này để kiểm tra email có tồn tại không
     */   
    @Test
    public void testCheckEmailExist() throws SQLException, ClassNotFoundException{
        assertTrue(new User().findByEmail("abc@gmail.com"));
        assertFalse(new User().findByEmail("abcad@gmail.com"));
    }
    

}
