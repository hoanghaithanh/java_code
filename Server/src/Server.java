import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
/*
 * Server.java
 *
 * Created on 21 ����, 2008, 09:41 �
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Mohamed Talaat Saad
 */
public class Server extends Thread {
    
    /**
     * Creates a new instance of Server
     */
    
    private ArrayList<ClientInfo> clients;
    private	ArrayList<EnemyInfo> enemies;
    private ServerSocket serverSocket;
    private int serverPort=11111;
    private int lvl;
   
    private DataInputStream reader;
    private DataOutputStream writer;
   
    private Protocol protocol;
    private boolean running=true;
    
    public int numOfEnemies()
    {
    	int i=0;
    	if(enemies==null)
    	{
    		return 0;
    	}
    	for(EnemyInfo ei:enemies)
    	{
    		if(ei!=null) i++;
    	}
    	//System.out.println(i);
    	return i;
    }
    
    public Thread thread2 = new Thread(){
    	boolean endPlay;
    	public void run()
    	{
    		while(lvl<=10)
    		{
    			System.out.println(numOfEnemies());
	    		if(numOfEnemies()==0)
	    		{
	    			lvl++;
	    			Level level = new Level(lvl);
	    			enemies = level.getEnemyList();
	    			for(EnemyInfo e:enemies)
	                {
	                	if(e!=null)
	                	{
	                		String message = protocol.RegisterEnemy(e.getX(), e.getY(), e.getID());
	                		try {
	        					BroadCastMessage(message);
	        				} catch (IOException e1) {
	        					// TODO Auto-generated catch block
	        					e1.printStackTrace();
	        				}
	                	}
	               	
	                }
	    			if(!thread1.isAlive()) thread1.start();
	    			else
	    			{
	    				synchronized(thread1){
	    					thread1.notify();
	    				}
	    			}
	    		}
	    		
    		}
    	}
    };
    
    public Thread thread1 = new Thread(){
    	
    	public void run()
    	{
    		while(true)
    		{
	    		if(numOfEnemies()>0)
	    		{
	    			if(enemies!=null)
	    			{
	    				for(int i=0;i<enemies.size();i++)
	    				{
	    					if(enemies.get(i)!=null) 
	    						{
	    						enemies.get(i).moveBackward();
			    					try {
										BroadCastMessage(protocol.UpdateEnemy(enemies.get(i).getX(), enemies.get(i).getY(), enemies.get(i).getID()));
										
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
	    						}
	    				}
	    				try {
							this.sleep(3000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	    			}
	    		}
	    		else
	    		{
	    		try {
					synchronized(thread2){
						thread2.wait();
					};
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		}
    	}
    }
    };
    //Khoi tao server
    public Server() throws SocketException 
    {
        //Mang thong tin cac client hien tai
        clients=new ArrayList<ClientInfo>();
        this.lvl=0;
        //Giao thuc
        protocol=new Protocol();
        try {
            serverSocket=new ServerSocket(serverPort);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    

    //Cong viec server
    public void run()
    {
        Socket clientSocket=null;
        while(running)
        {     
            try {
                //Chap nhan ket noi
                clientSocket=serverSocket.accept();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            String sentence="";
            try {
                //Doc thong diep tu client
                reader=new DataInputStream(clientSocket.getInputStream());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                sentence=reader.readUTF();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            
            //System.out.println(sentence);
            //Neu thong diep bat dau bang hello thi tao them mot player moi
            if(sentence.startsWith("Hello"))
            {
                //Lay toa do cua player moi
                int pos=sentence.indexOf(',');
                int x=Integer.parseInt(sentence.substring(5,pos));
                int y=Integer.parseInt(sentence.substring(pos+1,sentence.length()));
              
                try {
                    //Lay out put de gui thong diep cho client sau nay
                    writer=new DataOutputStream(clientSocket.getOutputStream());
                    
                    //if(!thread1.isAlive()) thread1.start();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                sendToClient(protocol.IDPacket(clients.size()+1));
                try {
                    //Phat thong diep tao player moi tren tat ca cac ung dung client
                    BroadCastMessage(protocol.NewClientPacket(x,y,1,clients.size()+1));
                    //Hien thi cac player khac tren man hinh cua new player
                    sendAllClients(writer);
                    
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                //Them thong tin cua player moi vao mang clients
                clients.add(new ClientInfo(writer,x,y,1));
                
                if(!thread2.isAlive()) thread2.start();
                else
                {
                	for(EnemyInfo e:enemies)
                    {
                    	if(e!=null)
                    	{
                    		String message = protocol.RegisterEnemy(e.getX(), e.getY(), e.getID());
                    		//System.out.println(message);
                    		try {
            					BroadCastMessage(message);
            				} catch (IOException e1) {
            					// TODO Auto-generated catch block
            					e1.printStackTrace();
            				}
                    	}
                   	
                    }
                }
                
                
            }
            
            //Neu Message bat dau bang update thi thay doi thong tin cua player co id trong mang clients
            else if(sentence.startsWith("Update"))
            {
                    int pos1=sentence.indexOf(',');
                    int pos2=sentence.indexOf('-');
                    int pos3=sentence.indexOf('|');
                    int x=Integer.parseInt(sentence.substring(6,pos1));
                    int y=Integer.parseInt(sentence.substring(pos1+1,pos2));
                    int dir=Integer.parseInt(sentence.substring(pos2+1,pos3));
                    int id=Integer.parseInt(sentence.substring(pos3+1,sentence.length()));
                    //Do trong mang bat chi so bat dau tu 0 nen client co id la x thi se la phan tu thu x-1
                    if(clients.get(id-1)!=null)
                    {
                        clients.get(id-1).setPosX(x);
                        clients.get(id-1).setPosY(y);
                        clients.get(id-1).setDirection(dir);
                        try {
                            BroadCastMessage(sentence);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                    
            }
            // Xu ly message ban
            else if(sentence.startsWith("Shot"))
            {
                try {
                    BroadCastMessage(sentence);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            // Xu ly message loai nguoi choi
            else if(sentence.startsWith("Remove"))
            {
                int id=Integer.parseInt(sentence.substring(6));

                try {
                    BroadCastMessage(sentence);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                //Xoa nguoi choi co id trong message ra khoi mang thong tin client
                clients.set(id-1,null);
            }
            else if(sentence.startsWith("ERemove"))
            {
                int id=Integer.parseInt(sentence.substring(7));

                try {
                    BroadCastMessage(sentence);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                //Xoa nguoi choi co id trong message ra khoi mang thong tin client
                enemies.set(id,null);
            }
            //Thoat game
            else if(sentence.startsWith("Exit"))
            {
                int id=Integer.parseInt(sentence.substring(4));

                try {
                    BroadCastMessage(sentence);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                if(clients.get(id-1)!=null)
                    clients.set(id-1,null);
            }
        }
        
        try {
            reader.close();
        writer.close();
        serverSocket.close();
            clientSocket.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    //Ngung server
    public void stopServer() throws IOException
    {
        running=false;
    }

    //Ham gui thong diep den tat ca cac client
    public void BroadCastMessage(String mess) throws IOException
    {
        for(int i=0;i<clients.size();i++)
        {
            if(clients.get(i)!=null)
                clients.get(i).getWriterStream().writeUTF(mess);
        }
    }

    //Gui thong diep den client hien tai
    public void sendToClient(String message)
    {
         if(message.equals("exit"))
            System.exit(0);
        else
        {    
            try {
                writer.writeUTF(message);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    
    //Ham 
    public void sendAllClients(DataOutputStream writer)
    {
        int x,y,dir;
        for(int i=0;i<clients.size();i++)
        {
            if(clients.get(i)!=null) {
                x=clients.get(i).getX();
                y=clients.get(i).getY();
                dir=clients.get(i).getDir();
                try {
                    writer.writeUTF(protocol.NewClientPacket(x,y,dir,i+1));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    //Thong tin mot client
    public class ClientInfo
    {
        DataOutputStream writer;
        int posX,posY,direction;
        
        public ClientInfo(DataOutputStream writer,int posX,int posY,int direction)
        {
           this.writer=writer;
           this.posX=posX;
           this.posY=posY;
           this.direction=direction;
        }
        
        public void setPosX(int x)
        {
            posX=x;
        }
        public void setPosY(int y)
        {
            posY=y;
        }
        public void setDirection(int dir)
        {
            direction=dir;
        }
        public DataOutputStream getWriterStream()
        {
            return writer;
        }
        public int getX()
        {
            return posX;
        }
        public int getY()
        {
            return posY;
        }
        public int getDir()
        {
            return direction;
        }
    }
    
}
