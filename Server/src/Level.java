import java.util.ArrayList;

public class Level {
 private ArrayList<EnemyInfo> enemyArr;
 private int lvl;
 
 public Level(int lvl)
 {
	 this.lvl = lvl;
	 enemyArr = new ArrayList<EnemyInfo>(lvl*10);
	 for(int i=0;i<lvl;i++)
	 {
		 for(int j=0;j<=9;j++)
		 {
			 enemyArr.add(new EnemyInfo(70+70*(j%5+1),70*(1-i-j/5),10*i+j));
		 }
	 }
 }
 
 public ArrayList<EnemyInfo> getEnemyList()
 {
	 return this.enemyArr;
 }
 
 public int getLevel()
 {
	 return this.lvl;
 }
 
}
