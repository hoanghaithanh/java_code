package graphBaseSegmentation;


import java.awt.Point;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.Stack;

/**
 *  The {@code Graph} class represents an undirected graph of vertices
 *  named 0 through <em>V</em> � 1.
 *  It supports the following two primary operations: add an edge to the graph,
 *  iterate over all of the vertices edgeArrayacent to a vertex. It also provides
 *  methods for returning the number of vertices <em>V</em> and the number
 *  of edges <em>E</em>. Parallel edges and self-loops are permitted.
 *  By convention, a self-loop <em>v</em>-<em>v</em> appears in the
 *  edgeArrayacency list of <em>v</em> twice and contributes two to the degree
 *  of <em>v</em>.
 *  <p>
 *  This implementation uses an edgeArrayacency-lists representation, which 
 *  is a vertex-indexed array of {@link Bag} objects.
 *  All operations take constant time (in the worst case) except
 *  iterating over the vertices edgeArrayacent to a given vertex, which takes
 *  time proportional to the number of such vertices.
 *  <p>
 *  For additional documentation, see <a href="http://algs4.cs.princeton.edu/41graph">Section 4.1</a>
 *  of <i>Algorithms, 4th Edition</i> by Robert Sedgewick and Kevin Wayne.
 *
 *  @author Robert Sedgewick
 *  @author Kevin Wayne
 */
public class GraphInt {
    private static final String NEWLINE = System.getProperty("line.separator");
    private static Mat matrix;

    private final int V;
    private int E;
    private Point[] vertexArray;
    private Bag<Point>[] edgeArray;
    
    /**
     * Initializes an empty graph with {@code V} vertices and 0 edges.
     * param V the number of vertices
     *
     * @param  V number of vertices
     * @throws IllegalArgumentException if {@code V < 0}
     */
    public GraphInt(int V) {
        if (V < 0) throw new IllegalArgumentException("Number of vertices must be nonnegative");
        this.V = V;
        this.E = 0;
        vertexArray = new Point[V];
        edgeArray = (Bag<Point>[]) new Bag[V];
        for (int v = 0; v < V; v++) {
            edgeArray[v] = new Bag<Point>();
        }
    }
    
    public static void setMatrix(Mat mat)
    {
    	matrix = mat;
    }

    /**  
     * Initializes a graph from the specified input stream.
     * The format is the number of vertices <em>V</em>,
     * followed by the number of edges <em>E</em>,
     * followed by <em>E</em> pairs of vertices, with each entry separated by whitespace.
     *
     * @param  in the input stream
     * @throws IllegalArgumentException if the endpoints of any edge are not in prescribed range
     * @throws IllegalArgumentException if the number of vertices or edges is negative
     * @throws IllegalArgumentException if the input stream is in the wrong format
     */
    public GraphInt(Mat mat) {
        try {
            this.V = mat.cols()*mat.rows();
            vertexArray = new Point[V];
            edgeArray = (Bag<Point>[]) new Bag[V];
            for (int v = 0; v < V; v++) {
                edgeArray[v] = new Bag<Point>();
            }
            
            //Khoi tao mang cac canh
            //Khoi tao mang cac diem
            //Khoi tao tung diem, moi diem add vao tap canh cua cac lan can
            //Diem co toa do (col,row) trong matrix se co chi so mang la col*ROW+row
            for(int col=0;col<mat.cols();col++)
            {
            	for(int row = 0;row<mat.rows();row++)
            	{
            		Point p = new Point(col,row);
            		vertexArray[col*mat.rows()+row]=p;
            		
            		//Neu diem khong nam o tren cung thi no se co lan can voi diem ben tren
            		if(row>=1) edgeArray[col*mat.rows()+row-1].add(p);
            		//Neu diem khong nam mo hang duoi cung thi no se co lan can voi diem ben duoi
            		if(row<mat.rows()-1) edgeArray[col*mat.rows()+row+1].add(p);
            		//Neu diem khong nam o cot ngoai cung ben trai thi no co lan can trai
            		if(col>=1) edgeArray[(col-1)*mat.rows()+row].add(p);
            		//Neu diem khong nam o cot ngoai cung ben phai thi no co lan can phai
            		if(col<mat.cols()-1) edgeArray[(col+1)*mat.rows()+row].add(p);
            		
            	}
            }
            
        }
        catch (NoSuchElementException e) {
            throw new IllegalArgumentException("invalid input format in Graph constructor", e);
        }
    }


    /**
     * Initializes a new graph that is a deep copy of {@code G}.
     *
     * @param  G the graph to copy
     */
    public GraphInt(GraphInt G) {
        this(G.V());
        this.E = G.E();
        for (int v = 0; v < G.V(); v++) {
            // reverse so that edgeArrayacency list is in same order as original
            Stack<Point> reverse = new Stack<Point>();
            for (Point w : G.edgeArray[v]) {
                reverse.push(w);
            }
            for (Point w : reverse) {
                edgeArray[v].add(w);
            }
        }
    }

    /**
     * Returns the number of vertices in this graph.
     *
     * @return the number of vertices in this graph
     */
    public int V() {
        return V;
    }

    /**
     * Returns the number of edges in this graph.
     *
     * @return the number of edges in this graph
     */
    public int E() {
        return E;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }

    /**
     * Adds the undirected edge v-w to this graph.
     *
     * @param  v one vertex in the edge
     * @param  w the other vertex in the edge
     * @throws IllegalArgumentException unless both {@code 0 <= v < V} and {@code 0 <= w < V}
     */
    public void addEdge(int v, int w) {
        validateVertex(v);
        validateVertex(w);
        E++;
        edgeArray[v].add(vertexArray[w]);
        edgeArray[w].add(vertexArray[v]);
    }


    /**
     * Returns the vertices edgeArrayacent to vertex {@code v}.
     *
     * @param  v the vertex
     * @return the vertices edgeArrayacent to vertex {@code v}, as an iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<Point> edgeArray(int v) {
        validateVertex(v);
        return edgeArray[v];
    }

    /**
     * Returns the degree of vertex {@code v}.
     *
     * @param  v the vertex
     * @return the degree of vertex {@code v}
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public int degree(int v) {
        validateVertex(v);
        return edgeArray[v].size();
    }


    /**
     * Returns a string representation of this graph.
     *
     * @return the number of vertices <em>V</em>, followed by the number of edges <em>E</em>,
     *         followed by the <em>V</em> edgeArrayacency lists
     */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(V + " vertices, " + E + " edges " + NEWLINE);
        for (int v = 0; v < V; v++) {
            s.append(vertexArray[v].toString() + ": ");
            for (Point w : edgeArray[v]) {
                s.append(w.toString() + " ");
            }
            s.append(NEWLINE);
        }
        return s.toString();
    }


    /**
     * Unit tests the {@code Graph} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
    	String filePath = "../ImageSegmentation/src/graphBaseSegmentation/cathedral.jpg";
		Mat newImage = Imgcodecs.imread(filePath);
		if(newImage.dataAddr()==0)
		{
			System.out.println("Khong the mo file "+filePath);
		}
		else
		{
			GraphInt g = new GraphInt(newImage);
			g.toString();
		}
    }

}

/******************************************************************************
 *  Copyright 2002-2016, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
