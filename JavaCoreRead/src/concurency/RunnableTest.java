package concurency;

public class RunnableTest {
	static Runnable task = ()->{
		String threadName = Thread.currentThread().getName();
		System.out.println("Hello "+ threadName);
	};
	
	
	public static void main(String[] args)
	{
		task.run();
		Thread thread = new Thread(task);
		thread.start();
		System.out.println("Done!");
	}
	
}
