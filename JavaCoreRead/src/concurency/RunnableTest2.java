package concurency;

import java.util.concurrent.TimeUnit;

public class RunnableTest2 {
	static Runnable runnable = () -> {
	    try {
	        String name = Thread.currentThread().getName();
	        System.out.println("Foo " + name);
	        //TimeUnit.SECONDS.sleep(1);
	        Thread.sleep(1000);
	        System.out.println("Bar " + name);
	    }
	    catch (InterruptedException e) {
	        e.printStackTrace();
	    }
	};
	
	public static void main(String[] args)
	{
		Thread thread = new Thread(runnable);
		thread.start();
	}
}
