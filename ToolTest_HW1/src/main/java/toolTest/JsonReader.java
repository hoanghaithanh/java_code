package toolTest;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;


public class JsonReader {
	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(JsonReader.class);
	
	private static String readAll(Reader rd) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		int cp;
		while((cp=rd.read())!=-1)
		{
			sb.append((char)cp);
		}
		return sb.toString();
	}
	
	public static JSONObject readJsonFromUrl(String url) throws IOException
	{
		InputStream is = new URL(url).openStream();
		
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is,Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		}finally{
			is.close();
		}
		
	}
	
	
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		JFrame frame = new JFrame("Processing");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(-1);
			}
			
		});
		panel.add(exitButton);
		
		frame.setSize(50,70);
		frame.add(panel);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		JsonReader newReader = new JsonReader();
		String old_user = new String("");
		while(true)
		{
		JSONObject json =newReader.readJsonFromUrl("http://news.admicro.vn:10002/api/realtime?domain=kenh14.vn");
		if(!json.get("user").toString().equals(old_user))
		{
			logger.info(json.toString());
			old_user = new String(json.get("user").toString());
			
		}
		Thread.sleep(2000);
		}
	}
}
