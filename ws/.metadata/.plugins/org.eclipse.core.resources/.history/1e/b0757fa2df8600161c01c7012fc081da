/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package user.boundary;

import common.boundary.MainForm;
import user.controller.UserController;
import entity.User;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 * Class này là lớp biên của chức năng sửa thông tin người dùng
 * @author CoTrang-Lecture
 */
public class UpdateProfileForm extends javax.swing.JPanel {

    /**
     * Creates new form UpdateProfileForm
     */
    public UpdateProfileForm() {
        initComponents();
        genInfomation();
    }
    /**
     * Hàm này để tạo ra thông tin cá nhân 
     * theo email của người dùng đã login
     */
    private void genInfomation(){
        txtEmail.setText(MainForm.getEmail());
        UserController userController = UserController.getInstance();
        try {
            User user=userController.findUserByEmail(MainForm.getEmail());
            txtFirstName.setText(user.getFirstName());
            txtLastName.setText(user.getLastName());
            txtPhoneNumber.setText(user.getPhoneNumber());
            if(user.getGender().equals("Male"))
                cbbGender.setSelectedIndex(0);
            else
                cbbGender.setSelectedIndex(1);
            String[] splitResult = user.getBirthDay().split("/");
            Date date = new Date();
            date.setDate(Integer.parseInt(splitResult[0]));
            date.setMonth(Integer.parseInt(splitResult[1])-1);
            date.setYear(Integer.parseInt(splitResult[2])-1900);
            date.setHours(0);
            date.setMinutes(0);
            ccBirthday.setDate(date);
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Hàm này để cập nhật thông tin người dùng
     * và có gọi đến phương thức kiểm tra
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    private void updateProfile() throws SQLException, ClassNotFoundException{
        if(checkUpdateProfile()){
            updateUser();
        }
    }
    
    /**
     * Hàm này để cập nhật thông tin người dùng
     * lấy dữ liệu trên form để xử lý
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    private void updateUser(){
            try {
            User user = new User();
            String date = ccBirthday.getDate().toString();
            String password = new String(txtNewPass.getPassword());
            String gender =cbbGender.getItemAt(cbbGender.getSelectedIndex());
            user.setEmail(txtEmail.getText().toLowerCase());
            user.setPassword(AccountHelper.MD5encrypt(password));
            user.setFirstName(txtFirstName.getText());
            user.setLastName(txtLastName.getText());
            user.setPhoneNumber(txtPhoneNumber.getText());
            user.setBirthDay(AccountHelper.getDate(date));
            user.setGender(gender);
            user.setRequestToChange(false);
            user.setStatus(false);
            UserController.getInstance().updateProfile(user,MainForm.getEmail());
            JOptionPane.showMessageDialog(this, "Update success","Update",JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Hàm này để kiểm tra dữ liệu đầu vào trên form 
     */
    private boolean checkEmail() throws SQLException, ClassNotFoundException{
            boolean hasError=false;
            if(!MainForm.getEmail().equals(txtEmail.getText())){
            if(!AccountHelper.validateEmail(txtEmail.getText().toLowerCase())){
                lbEmail.setText("Email invalid form");
                hasError =true;
            }else{
                if(UserController.getInstance().findUserByEmail(txtEmail.getText())!=null){
                    hasError=true;
                    lbEmail.setText("Email is already exist");
                }
                else{
                    lbEmail.setText("");
                }
            }
        }
        return hasError;
    }
    /**
     * Hàm này để kiểm tra mật khẩu có đúng qui định không
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    private boolean checkPassWord() throws SQLException, ClassNotFoundException{
        boolean hasError=false;
        User user;
        user = UserController.getInstance().findUserByEmail(MainForm.getEmail());
        if(AccountHelper.MD5encrypt(txtOldPass.getText()).equals(user.getPassword())){
            lbOldPass.setText("");
        }else{
            lbOldPass.setText("Password was wrong");
            hasError=true;
        }
        return hasError;
    }
    
    /**
     *  Hàm này để kiểm tra first name có đúng định dạng không
     */
    private boolean checkFirstName(){
        switch(AccountHelper.validateFirstNameOrLastName(txtFirstName.getText())){
            case 0:
                lbFirstName.setText(""); 
                return true;
            case 1:
                lbFirstName.setText("Fist name must have information");
                return false;
            default:
                lbFirstName.setText("Fist name is too long");
                return false;
        }
    }
    /**
     *  Hàm này để kiểm tra last name có đúng định dạng không
     */
    private boolean checkLastName(){
        switch(AccountHelper.validateFirstNameOrLastName(txtLastName.getText())){
            case 0:
                lbLastName.setText(""); 
                return true;
            case 1:
                lbLastName.setText("Last name must have information");
                return false;
            default:
                lbLastName.setText("Last name is too long");
                return false;
        }
    }
    /**
     * Hàm này để kiểm tra thông tin cập nhật có  đúng không
     * @return boolean nếu dúng thỉ return true, nếu sai return false
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    private boolean checkUpdateProfile() throws SQLException, ClassNotFoundException{
        String password = new String(txtNewPass.getPassword());
        String confirmPass = new String(txtConfirmPass.getPassword());
        String phoneNumber = txtPhoneNumber.getText();
        boolean hasError=checkEmail()||checkPassWord()||!(checkFirstName()&&checkLastName());
        if(password.length()<8){
            lbNewPass.setText("Password must at leat 8 character");
        }else
            lbNewPass.setText("");
        if(!AccountHelper.validatePassword(password)){
            lbNewPass.setText("Password must have lower case, upper case, special character");
            hasError= true;
        }else
            lbNewPass.setText("");
        if(!password.equals(confirmPass)){
            lbConfirmPass.setText("Password did not match");
            hasError= true;
        }else
            lbConfirmPass.setText("");
        if(txtFirstName.getText().length()==0){
            lbFirstName.setText("Fist name must have information");
            hasError= true;
        }else
            lbFirstName.setText("");
        if(txtLastName.getText().length()==0){
            lbLastName.setText("Last name must have information");
            hasError= true;
        }else
            lbLastName.setText("");
        if(!AccountHelper.validatePhoneNumber(phoneNumber)){
            lbPhoneNumber.setText("Phone number only have . - and space character");
            hasError = true;
        }else
            lbPhoneNumber.setText("");
        return !hasError;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel8 = new javax.swing.JLabel();
        txtLastName = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPhoneNumber = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lbEmail = new javax.swing.JLabel();
        btnSubmit = new javax.swing.JButton();
        lbOldPass = new javax.swing.JLabel();
        lbFirstName = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        lbLastName = new javax.swing.JLabel();
        txtOldPass = new javax.swing.JPasswordField();
        lbPhoneNumber = new javax.swing.JLabel();
        txtFirstName = new javax.swing.JTextField();
        lbConfirmPass = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cbbGender = new javax.swing.JComboBox<>();
        txtConfirmPass = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        ccBirthday = new org.freixas.jcalendar.JCalendarCombo();
        jLabel3 = new javax.swing.JLabel();
        lbNewPass = new javax.swing.JLabel();
        txtNewPass = new javax.swing.JPasswordField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();

        jLabel8.setText("Confirm Password:");

        txtLastName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtLastNameActionPerformed(evt);
            }
        });

        jLabel4.setText("Last Name:");

        jLabel5.setText("Birthday:");

        jLabel7.setText("Phone Number:");

        jLabel6.setText("Gender:");

        lbEmail.setText(" ");

        btnSubmit.setText("Update");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        lbOldPass.setText(" ");

        lbFirstName.setText(" ");

        txtEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailActionPerformed(evt);
            }
        });

        lbLastName.setText(" ");

        txtOldPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOldPassActionPerformed(evt);
            }
        });

        lbPhoneNumber.setText(" ");

        txtFirstName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFirstNameActionPerformed(evt);
            }
        });

        lbConfirmPass.setText(" ");

        jLabel1.setText("Email:");

        cbbGender.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Male", "Female" }));

        txtConfirmPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtConfirmPassActionPerformed(evt);
            }
        });

        jLabel2.setText("Old Password:");

        ccBirthday.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ccBirthdayActionPerformed(evt);
            }
        });

        jLabel3.setText("First Name:");

        lbNewPass.setText(" ");

        txtNewPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNewPassActionPerformed(evt);
            }
        });

        jLabel11.setText("New Password:");

        jLabel12.setText("Cập nhật thông tin cá nhân:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnSubmit)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(jLabel4)
                                                .addComponent(jLabel3)
                                                .addComponent(jLabel2)
                                                .addComponent(jLabel1)
                                                .addComponent(jLabel7)
                                                .addComponent(jLabel5))
                                            .addGap(38, 38, 38))
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                            .addComponent(jLabel8)
                                            .addGap(22, 22, 22)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel11)
                                            .addComponent(jLabel6))
                                        .addGap(62, 62, 62)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNewPass, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
                                    .addComponent(cbbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(ccBirthday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtConfirmPass)
                                    .addComponent(txtOldPass)
                                    .addComponent(txtEmail)
                                    .addComponent(txtFirstName)
                                    .addComponent(txtLastName)
                                    .addComponent(txtPhoneNumber))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbEmail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbOldPass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbFirstName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbLastName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lbPhoneNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                                    .addComponent(lbConfirmPass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(lbNewPass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbEmail))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtOldPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbOldPass))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txtNewPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbNewPass))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtConfirmPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbConfirmPass))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtFirstName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbFirstName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtLastName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbLastName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtPhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbPhoneNumber))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ccBirthday, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbbGender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel6))
                .addGap(15, 15, 15)
                .addComponent(btnSubmit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtLastNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtLastNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLastNameActionPerformed
    
    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        try {
            updateProfile();
        }catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void txtEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailActionPerformed

    private void txtOldPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOldPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOldPassActionPerformed

    private void txtFirstNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFirstNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFirstNameActionPerformed

    private void txtConfirmPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtConfirmPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtConfirmPassActionPerformed

    private void txtNewPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNewPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNewPassActionPerformed

    private void ccBirthdayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ccBirthdayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ccBirthdayActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox<String> cbbGender;
    private org.freixas.jcalendar.JCalendarCombo ccBirthday;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel lbConfirmPass;
    private javax.swing.JLabel lbEmail;
    private javax.swing.JLabel lbFirstName;
    private javax.swing.JLabel lbLastName;
    private javax.swing.JLabel lbNewPass;
    private javax.swing.JLabel lbOldPass;
    private javax.swing.JLabel lbPhoneNumber;
    private javax.swing.JPasswordField txtConfirmPass;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtFirstName;
    private javax.swing.JTextField txtLastName;
    private javax.swing.JPasswordField txtNewPass;
    private javax.swing.JPasswordField txtOldPass;
    private javax.swing.JTextField txtPhoneNumber;
    // End of variables declaration//GEN-END:variables
}
