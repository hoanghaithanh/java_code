package bookStore;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import account.BorrowerUI;
import account.StartingUI;
import account.UserInterfaceUI;
import common.Session;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
/**
 * Lớp giao diện cho việc duyệt sách
 * @author Thanh
 *
 */
public class BrowseCatalogUI extends JFrame {

	protected JPanel contentPane;
	protected JTextField textField;
	protected JScrollPane scrollPane;
	protected JTable table;
	protected JComboBox<Category> comboBox;
	JComboBox comboBox_1;
	protected BookController bookController = new BookController();
	protected String title = "", author = "", categoryID = "", publisherName = "",isbn = "";


	/**
	 * phương thức reset các thuộc tính của lớp
	 */
	void resetAttribute()
	{
		this.title = this.author = this.publisherName = this.isbn = "";
	}
	/**
	 * Phương thức khởi tạo giao diện, bố cực giao diện
	 */
	public void init(){
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 789, 522);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetAttribute();
				if(comboBox_1.getSelectedItem().toString().equals("Ten sach")) title = textField.getText();
				if(comboBox_1.getSelectedItem().toString().equals("Tac gia")) author = textField.getText();
				if(comboBox_1.getSelectedItem().toString().equals("NXB")) publisherName = textField.getText();
				if(comboBox_1.getSelectedItem().toString().equals("ISBN")) isbn = textField.getText();
				genTableContent();
			}
		});
		btnSearch.setBounds(437, 66, 89, 23);
		contentPane.add(btnSearch);

		textField = new JTextField();
		textField.setBounds(281, 67, 146, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(77, 113, 619, 281);
		contentPane.add(scrollPane);

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        if (me.getClickCount() == 2 && !me.isConsumed()) {
		            // your valueChanged overridden method 
		        	me.consume();
		        	TableModel tablemodel = table.getModel();
		        	int rowIndex = table.getSelectedRow();
		        	String bookID = tablemodel.getValueAt(rowIndex, 1).toString();
		        	String bookTitle = tablemodel.getValueAt(rowIndex, 2).toString();
		        	String bookAuthor = tablemodel.getValueAt(rowIndex, 3).toString();
		        	String bookPublisher = tablemodel.getValueAt(rowIndex, 4).toString();
		        	String bookCategory = tablemodel.getValueAt(rowIndex, 5).toString();
		        	Books book = new Books();
		        	book.setBookFromID(bookID);
		        	DetailBookUI detailUI = new DetailBookUI(book);
		        	detailUI.setVisible(true);
		        	//System.out.println(tablemodel.getValueAt(rowIndex, 1));
		        }
		    }
		});
		genTableContent();
		table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		scrollPane.setViewportView(table);
		
		comboBox = new JComboBox<Category>();
		comboBox.setEditable(true);
        //comboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
		ArrayList<Category> listCategory = bookController.getAllCategory();
		comboBox.removeAllItems();
		comboBox.addItem(new Category());
		for (Category bc : listCategory) {
			comboBox.addItem(bc);
		}
		comboBox.setBounds(139, 35, 124, 20);
		contentPane.add(comboBox);
		
		JLabel lblCategory = new JLabel("Category: ");
		lblCategory.setBounds(77, 38, 85, 14);
		contentPane.add(lblCategory);
		
		JLabel lblField = new JLabel("Field:");
		lblField.setBounds(77, 70, 46, 14);
		contentPane.add(lblField);
		
		comboBox_1= new JComboBox();
		comboBox_1.setBounds(139, 67, 124, 20);
		comboBox_1.addItem("Ten sach");
		comboBox_1.addItem("Tac gia");
		comboBox_1.addItem("ISBN");
		comboBox_1.addItem("NXB");
		contentPane.add(comboBox_1);
	}
	/**
	 * Phuông thức khởi tạo giao diện
	 */
	public BrowseCatalogUI() {
		init();
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				categoryID = ((Category) comboBox.getSelectedItem()).getCategoryID();
				genTableContent();
			}
		});

	}
	/**
	 * Phương thức sinh dữ liệu cho table trong giao diện
	 */
	public void genTableContent() {
		try {
			ArrayList<Books> listBook = bookController.searchBook(categoryID, title, author, publisherName,isbn);
			Vector<String> row, colunm;
			int count = 1;
			DefaultTableModel model = new DefaultTableModel() {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			String[] colunmNames = { "No.","Ma Sach", "Ten Sach","ISBN", "Tac gia", "Nha xuat ban","The loai" };
			colunm = new Vector<String>();
			int numberColumn;
			numberColumn = colunmNames.length;
			for (int i = 0; i < numberColumn; i++) {
				colunm.add(colunmNames[i]);
			}
			model.setColumnIdentifiers(colunm);
			for (Books book : listBook) {
				row = new Vector<String>();
				row.add(count + "");
				row.add(book.getBookID());
				row.add(book.getTitle());
				row.add(book.getIsbn());
				row.add(book.getAuthor());
				row.add(book.getPublisher().getPublisherName());
				row.add(book.getCategory().getCategoryName());
				count++;
				model.addRow(row);
			}
			table.setModel(model);
			table.setVisible(true);
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Loi", "Co loi xay ra!", JOptionPane.ERROR_MESSAGE);
		}
	}
}
