package bookStore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
/**
 * Giao diện thông tin chi tiết của sách
 * @author Thanh
 *
 */
public class DetailBookUI extends JFrame {

	private JPanel contentPane;


	/**
	 * Khởi tạo giao diện
	 */
	
	public void init(String bookNumber, String bookTitle, String bookAuthor, String bookCategory, String bookPublisher){
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 498, 454);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBookNumber = new JLabel("Book Number:");
		lblBookNumber.setBounds(32, 37, 95, 14);
		contentPane.add(lblBookNumber);
		
		JLabel lblNewLabel = new JLabel("Book Title:");
		lblNewLabel.setBounds(32, 86, 95, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblAuthor = new JLabel("Author:");
		lblAuthor.setBounds(32, 136, 95, 14);
		contentPane.add(lblAuthor);
		
		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setBounds(32, 184, 95, 14);
		contentPane.add(lblCategory);
		
		JLabel lblPublisher = new JLabel("Publisher:");
		lblPublisher.setBounds(32, 235, 95, 14);
		contentPane.add(lblPublisher);
		
		JLabel lblbookNumber = new JLabel(bookNumber);
		lblbookNumber.setBounds(172, 37, 95, 14);
		contentPane.add(lblbookNumber);
		
		JLabel lblbookTitle = new JLabel(bookTitle);
		lblbookTitle.setBounds(172, 86, 195, 14);
		contentPane.add(lblbookTitle);
		
		JLabel lblbookAuthor = new JLabel(bookAuthor);
		lblbookAuthor.setBounds(172, 136, 195, 14);
		contentPane.add(lblbookAuthor);
		
		JLabel lblbookCategory = new JLabel(bookCategory);
		lblbookCategory.setBounds(172, 184, 195, 14);
		contentPane.add(lblbookCategory);
		
		JLabel lblbookPublisher = new JLabel(bookPublisher);
		lblbookPublisher.setBounds(172, 235, 195, 14);
		contentPane.add(lblbookPublisher);
		
	}
	public DetailBookUI(Books book) {
		init( book.getBookID(),  book.getTitle(),  book.getIsbn(),  book.getCategory().getCategoryName(),  book.getPublisher().getPublisherName());
	}
}
