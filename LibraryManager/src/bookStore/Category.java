package bookStore;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import common.DataAccessHelper;

/**
 * Lớp trừu tượng hóa các thông tin của phân loại sách
 * @author Thanh
 *
 */
public class Category extends DataAccessHelper {
	private String categoryID;
	private String categoryName;

	public Category() {
		categoryID = "";
		categoryName = "";
	}
	/**
	 * Phương thức đặt thuộc tính của đối tượng theo các thông tin phân loại có id cho trước
	 * @param categoryID
	 * @return thành công hoặc thất bại
	 */
	public boolean setCategoryFromID(String categoryID)
	{
		String sql = "SELECT * FROM book_category WHERE category_id = '"+categoryID+"'";
		try {
			getConnection();
			ResultSet rs = getStmt().executeQuery(sql);
			
			if(!rs.next())
			{
				JOptionPane.showMessageDialog(null, "Khong ton tai Category co ID la: "+categoryID);
				closeConnection();
				return false;
			}
			else
			{
				this.categoryID = rs.getString("category_id");
				this.categoryName = rs.getString("category_name");
				closeConnection();
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			closeConnection();
			e.printStackTrace();
			return false;
		}
		
	}
	/**
	 * Khởi tạo một phân loại theo mã phân loại và tên phân loại
	 * @param classificationID
	 * @param classificationName
	 */
	public Category(String classificationID, String classificationName) {
		super();
		this.categoryID = classificationID;
		this.categoryName = classificationName;
	}

	public String getCategoryID() {
		return categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}
/**
 * Phương thức tạo mảng chứa tất cả các phân loại sách trong csdl
 * @return
 * @throws SQLException
 */
	public ArrayList<Category> getListBookCategory() throws SQLException {
		ArrayList<Category> list = new ArrayList<Category>();
		getConnection();
		String sql = "SELECT category_id, category_name FROM book_category";
		ResultSet rs = getStmt().executeQuery(sql);
		while (rs.next()) {
			list.add(new Category(rs.getString("category_id"), rs.getString("category_name")));
		}
		return list;
	}
/**
 * Phương thức trả về tên phân loại
 */
	public String toString() {
		if (categoryName.equals(""))
			return "Tat ca";
		return categoryName;
	}


}
