package bookStore;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import account.UserInterfaceUI;
import common.Session;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
/**
 * Lớp giao diện cho việc Đăng ký sách mới vào csdl
 * @author Thanh
 *
 */
public class RegisterNewBookUI extends JFrame {

	private JPanel contentPane;
	private JTextField titleTextField;
	private JTextField authorTextField;
	private JTextField isbnTextField;
	private JComboBox<Category> categoryComboBox;
	private JComboBox<Publisher> publisherComboBox;
	private JLabel statusLabel;
	private BookController bookController = new BookController();
	private String title,author,isbn;
	private String bookID;
	

	

	/**
	 * Create the frame.
	 */
	public RegisterNewBookUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBookTitle = new JLabel("Book Title:");
		lblBookTitle.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBookTitle.setBounds(61, 56, 100, 33);
		contentPane.add(lblBookTitle);
		
		JLabel lblAuthor = new JLabel("Author:");
		lblAuthor.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblAuthor.setBounds(61, 100, 100, 33);
		contentPane.add(lblAuthor);
		
		JLabel lblCategory = new JLabel("Category:");
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCategory.setBounds(61, 188, 100, 33);
		contentPane.add(lblCategory);
		
		JLabel lblPublisher = new JLabel("ISBN:");
		lblPublisher.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPublisher.setBounds(61, 144, 100, 33);
		contentPane.add(lblPublisher);
		
		JLabel lblPublisher_1 = new JLabel("Publisher:");
		lblPublisher_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblPublisher_1.setBounds(61, 232, 100, 33);
		contentPane.add(lblPublisher_1);
		
		titleTextField = new JTextField();
		titleTextField.setBounds(171, 63, 200, 20);
		contentPane.add(titleTextField);
		titleTextField.setColumns(10);
		
		authorTextField = new JTextField();
		authorTextField.setColumns(10);
		authorTextField.setBounds(171, 107, 200, 20);
		contentPane.add(authorTextField);
		
		isbnTextField = new JTextField();
		isbnTextField.setColumns(10);
		isbnTextField.setBounds(171, 151, 200, 20);
		contentPane.add(isbnTextField);
		
		categoryComboBox = new JComboBox<Category>();
		categoryComboBox.setBounds(171, 195, 200, 20);
		categoryComboBox.removeAllItems();
		ArrayList<Category> categoryArr = bookController.getAllCategory();
		for(Category ca:categoryArr)
		{
			categoryComboBox.addItem(ca);
		}
		contentPane.add(categoryComboBox);
		
		publisherComboBox = new JComboBox<Publisher>();
		publisherComboBox.setBounds(171, 239, 200, 20);
		publisherComboBox.removeAllItems();
		ArrayList<Publisher> publisherArr = bookController.getAllPublisher();
		for(Publisher pa:publisherArr)
		{
			publisherComboBox.addItem(pa);
		}
		contentPane.add(publisherComboBox);
		
		JButton btnRegisterBook = new JButton("Register Book");
		btnRegisterBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				title = titleTextField.getText();
				author = authorTextField.getText();
				isbn = isbnTextField.getText();
				boolean check = true;
				if(check&title.isEmpty())
				{
					statusLabel.setText("* Can nhap ten sach!");
				} 
				else
					if(check&author.isEmpty())
					{
						statusLabel.setText("* Can nhap ten tac gia!");
					}
				else
					if(check&isbn.isEmpty())
					{
						statusLabel.setText("* Can nhap isbn!");
					}
					else
						if(check)
						{
							statusLabel.setText("");
							boolean result = registerBook(title,author,isbn);
							System.out.println(result);
							if(result)
								{
								int input = JOptionPane.showConfirmDialog(null, "Them sach moi thanh cong, bam Yes de xem chi tiet!");
								if(input==JOptionPane.YES_OPTION)
								{
									Books b = new Books();
									b.setBookFromID(bookID);
									DetailBookUI detailUI = new DetailBookUI(b);
									detailUI.setVisible(true);
								}
								
								};
						}
			}

			
		});
		btnRegisterBook.setBounds(171, 300, 121, 23);
		contentPane.add(btnRegisterBook);
		
		statusLabel = new JLabel("");
		statusLabel.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusLabel.setBounds(171, 350, 243, 20);
		contentPane.add(statusLabel);
	}
	/**
	 * Phương thức đăng ký sách có thông tin cho trước vào csdl
	 * @param title
	 * @param author
	 * @param isbn
	 * @return
	 */
	private boolean registerBook(String title, String author, String isbn) {
		// TODO Auto-generated method stub
		try {
			String categoryID = ((Category)categoryComboBox.getSelectedItem()).getCategoryID();
			String publisherName = publisherComboBox.getSelectedItem().toString();
			ArrayList<Books> bookList = bookController.searchBook(categoryID, title, author, publisherName,isbn);
			if(bookList.isEmpty())
			{
				Books book = new Books();
				boolean check = book.create(categoryID, title, author, isbn, ((Publisher)publisherComboBox.getSelectedItem()).getPublisherID());
				System.out.println(check);
				if(check)
				{
					this.bookID = book.getBookID();
				}
				return check;
			}
			else
			{
				int input = JOptionPane.showConfirmDialog(this, "Da ton tai sach trong thu vien! Ban co muon them mot copy khong");
						if(input == JOptionPane.YES_OPTION)
						{
							RegisterNewBookCopy newCopy = new RegisterNewBookCopy(bookList.get(0).getBookID());
							newCopy.setVisible(true);
						}
			}
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
}
