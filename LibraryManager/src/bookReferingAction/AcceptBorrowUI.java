package bookReferingAction;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import account.UserInterfaceUI;
import bookStore.Books;
import common.Session;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class AcceptBorrowUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;
	private ReturnBorrowController rbController = new ReturnBorrowController();
	private String cardID;
	private String copyID;
	private JComboBox comboBox;
	private JButton btnConfirmBorrowed;
	private JButton btnCancelRegistration;
	private JButton btnConfirmReturned;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AcceptBorrowUI frame = new AcceptBorrowUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AcceptBorrowUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 872, 399);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(comboBox.getSelectedItem().toString().equals("Book Copy ID"))
					{
					copyID = textField.getText();
					genTableContentFromCopyID();
					}
				if(comboBox.getSelectedItem().toString().equals("Borrower Card"))
					{
					cardID = textField.getText();
					genTableContentFromCardId();
					}
				
			}
		});
		btnSearch.setBounds(705, 32, 89, 23);
		contentPane.add(btnSearch);
		
		textField = new JTextField();
		textField.setBounds(548, 33, 134, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		comboBox = new JComboBox();
		comboBox.setBounds(393, 33, 112, 20);
		comboBox.addItem("Book Copy ID");
		comboBox.addItem("Borrower Card");
		contentPane.add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 87, 748, 184);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
	        public void valueChanged(ListSelectionEvent event) {
	        	btnConfirmBorrowed.setEnabled(true);
            	btnCancelRegistration.setEnabled(true);
            	btnConfirmReturned.setEnabled(true);
	            int row = table.getSelectedRow();
	            if(row==-1) return;
	            if(!table.getModel().getValueAt(row, 6).toString().equals("pending"))
	            {
	            	btnConfirmBorrowed.setEnabled(false);
	            	btnCancelRegistration.setEnabled(false);
	            	btnConfirmReturned.setEnabled(true);
	            }
	            else
	            {
	            	btnConfirmBorrowed.setEnabled(true);
	            	btnCancelRegistration.setEnabled(true);
	            	btnConfirmReturned.setEnabled(false);
	            }
	            
	        }
	    });
		scrollPane.setViewportView(table);
		
		btnConfirmBorrowed = new JButton("Confirm Borrowed");
		btnConfirmBorrowed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(table.getSelectedRow()==-1)
				{
					JOptionPane.showMessageDialog(null, "Chon mot ban copy truoc");
					return;
				}
				else
				{
					int selectedRow = table.getSelectedRow();
					String copyID = table.getModel().getValueAt(selectedRow, 2).toString();
					String borrowingID = table.getModel().getValueAt(selectedRow, 1).toString();
					BorrowingDetail borrDetail = new BorrowingDetail();
					try {
						borrDetail.acceptRequest(borrowingID, copyID);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					genTableContent();
				}
			}
		});
		btnConfirmBorrowed.setBounds(371, 282, 134, 23);
		contentPane.add(btnConfirmBorrowed);
		
		btnCancelRegistration = new JButton("Delete Registration");
		btnCancelRegistration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(table.getSelectedRow()==-1)
				{
					JOptionPane.showMessageDialog(null, "Chon mot ban copy truoc");
					return;
				}
				else
				{
					int selectedRow = table.getSelectedRow();
					String copyID = table.getModel().getValueAt(selectedRow, 2).toString();
					String borrowingID = table.getModel().getValueAt(selectedRow, 1).toString();
					BorrowingDetail borrDetail = new BorrowingDetail();
					try {
						borrDetail.cancelRequest(borrowingID, copyID);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					genTableContent();
				}
			}
		});
		btnCancelRegistration.setBounds(660, 280, 134, 23);
		contentPane.add(btnCancelRegistration);
		
		btnConfirmReturned = new JButton("Confirm Returned");
		btnConfirmReturned.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(table.getSelectedRow()==-1)
				{
					JOptionPane.showMessageDialog(null, "Chon mot ban copy truoc");
					return;
				}
				else
				{
					int ans = Integer.parseInt( (String) JOptionPane.showInputDialog(contentPane,
					        "Nhap so tien phat (Neu co)",
					        null, JOptionPane.INFORMATION_MESSAGE,
					        null,
					        null,
					        "0"));
					int selectedRow = table.getSelectedRow();
					String copyID = table.getModel().getValueAt(selectedRow, 2).toString();
					String borrowingID = table.getModel().getValueAt(selectedRow, 1).toString();
					BorrowingDetail borrDetail = new BorrowingDetail();
					try {
						borrDetail.setBorrowingDetailFrom(copyID, borrowingID);
						borrDetail.returnCopy(ans);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					genTableContent();
				}
			}
		});
		btnConfirmReturned.setBounds(516, 282, 134, 23);
		contentPane.add(btnConfirmReturned);
	}
	public void genTableContent()
	{
		if(comboBox.getSelectedItem().toString().equals("Book Copy ID"))
		{
		copyID = textField.getText();
		genTableContentFromCopyID();
		}
	if(comboBox.getSelectedItem().toString().equals("Borrower Card"))
		{
		cardID = textField.getText();
		genTableContentFromCardId();
		}
	}
	public void genTableContentFromCardId() {
		try {
			ArrayList<BorrowingDetail> listBorr = rbController.searchLentBookCopyFromCardID(cardID);
			if(listBorr.isEmpty())
			{
				JOptionPane.showMessageDialog(null, "Khong tim thay ket qua");
				TableModel dm = table.getModel();
				int rowCount = dm.getRowCount();
				//Remove rows one by one from the end of the table
				for (int i = rowCount - 1; i >= 0; i--) {
				    ((DefaultTableModel) dm).removeRow(i);
				}
			}
			else
			{
			Vector<String> row, colunm;
			int count = 1;
			DefaultTableModel model = new DefaultTableModel() {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			String[] colunmNames = { "No.","BorrowingID","CopyID", "Title","BorrowerName", "BorrowerID","Status" };
			colunm = new Vector<String>();
			int numberColumn;
			numberColumn = colunmNames.length;
			for (int i = 0; i < numberColumn; i++) {
				colunm.add(colunmNames[i]);
			}
			model.setColumnIdentifiers(colunm);
			for (BorrowingDetail book : listBorr) {
				row = new Vector<String>();
				row.add(count + "");
				row.add(book.getBorrowingID());
				row.add(book.getCopyID());
				row.add(book.getBookTitle());
				row.add(book.getBorrowerFullName());
				row.add(book.getBorrowerID());
				row.add(book.getStatus());
				count++;
				model.addRow(row);
			}
			table.setModel(model);
			table.setVisible(true);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Loi", "Co loi xay ra!", JOptionPane.ERROR_MESSAGE);
		}
	}
	public void eraseTableContent()
	{
		Vector<String> row, colunm;
		int count = 1;
		DefaultTableModel model = new DefaultTableModel() {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		String[] colunmNames = { "No.","BorrowingID","CopyID", "Title","BorrowerName", "BorrowerID","Status" };
		colunm = new Vector<String>();
		int numberColumn;
		numberColumn = colunmNames.length;
		for (int i = 0; i < numberColumn; i++) {
			colunm.add(colunmNames[i]);
		}
		model.setColumnIdentifiers(colunm);
		table.setModel(model);
		table.setVisible(true);
	}
	public void genTableContentFromCopyID() {
		try {
			BorrowingDetail book = rbController.searchLentBookCopyFromCopyID(copyID);
			if(book==null)
			{
				JOptionPane.showMessageDialog(this, "Khong tim thay ket qua!");
				TableModel dm = table.getModel();
				int rowCount = dm.getRowCount();
				//Remove rows one by one from the end of the table
				for (int i = rowCount - 1; i >= 0; i--) {
				    ((DefaultTableModel) dm).removeRow(i);
				}
			}
			else
			{
			Vector<String> row, colunm;
			int count = 1;
			DefaultTableModel model = new DefaultTableModel() {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			String[] colunmNames = { "No.","BorrowingID","CopyID", "Title","BorrowerName", "BorrowerID","Status" };
			colunm = new Vector<String>();
			int numberColumn;
			numberColumn = colunmNames.length;
			for (int i = 0; i < numberColumn; i++) {
				colunm.add(colunmNames[i]);
			}
			model.setColumnIdentifiers(colunm);
			
				row = new Vector<String>();
				row.add(count + "");
				row.add(book.getBorrowingID());
				row.add(book.getCopyID());
				row.add(book.getBookTitle());
				row.add(book.getBorrowerFullName());
				row.add(book.getBorrowerID());
				row.add(book.getStatus());
				count++;
				model.addRow(row);
			
			table.setModel(model);
			table.setVisible(true);
		}
		}catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Loi", "Co loi xay ra!", JOptionPane.ERROR_MESSAGE);
		}
		
	}
}
