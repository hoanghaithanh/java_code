package bookReferingAction;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import account.BorrowerUI;
import bookStore.BookCopy;
import bookStore.Books;
import bookStore.BrowseCatalogUI;
import common.Session;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
/**
 * Lớp giao diện cho việc đăng ký mượn sách
 * @author Thanh
 *
 */
public class RegisterToBorrowBookUI extends BrowseCatalogUI {
	private Cart cart = new Cart();
	private JPanel contentPane;
	/**
	 * Phương thức Khởi tạo giao diện
	 */
	public RegisterToBorrowBookUI() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		JButton btnAddToCart = new JButton("Add to cart");
		btnAddToCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] selectedRows = table.getSelectedRows();
				ArrayList<Books> bookList = new ArrayList<Books>();
				if(selectedRows.length>5)
				{
					JOptionPane.showMessageDialog(null, "Khong the chon nhieu hon 5 dau sach");
					return;
				}
				for(int i:selectedRows)
				{
					Books book = new Books();
					book.setBookFromID(table.getModel().getValueAt(i, 1).toString());
					bookList.add(book);
				}
				boolean check = cart.addBookToCart(bookList);
				if(!check)
				{
					JOptionPane.showMessageDialog(null, "Loi khi them sach vao cart");
				}
				CartInfoUI cartUi = new CartInfoUI(cart);
				cartUi.setVisible(true);
			}
		});
		btnAddToCart.setBounds(77, 407, 89, 23);
		getContentPane().add(btnAddToCart);
		
		JButton btnViewCart = new JButton("View cart");
		btnViewCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CartInfoUI cartUi = new CartInfoUI(cart);
				cartUi.setVisible(true);
			}
		});
		btnViewCart.setBounds(194, 407, 89, 23);
		getContentPane().add(btnViewCart);
	}
	
}
