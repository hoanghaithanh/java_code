package bookReferingAction;

import java.sql.SQLException;
import java.util.ArrayList;

import bookStore.BookCopy;
import common.DataAccessHelper;
/**
 * Lớp điều khiển cho các hoạt động mượn, trả sách
 * @author Thanh
 *
 */
public class ReturnBorrowController  {
/**
 * Phương thức tìm kiếm các chi tiết mượn chưa hoàn thành của user có cardid cho trước
 * @param borrowerCardID
 * @return
 */
	public  ArrayList<BorrowingDetail> searchLentBookCopyFromCardID(String borrowerCardID)
	{
		ArrayList<BorrowingDetail> copyArr = new ArrayList<BorrowingDetail>();
		BorrowingDetail bd = new BorrowingDetail();
		try {
			copyArr = bd.getDetailArrFrom(borrowerCardID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return copyArr;
	}
	/**
	 * Phương thức trả về chi tiết mượn chưa hoàn thành có copyID cho trước
	 * @param copyID
	 * @return
	 */
	public BorrowingDetail searchLentBookCopyFromCopyID(String copyID)
	{
		BorrowingDetail bd = new BorrowingDetail();
		try {
			boolean check = bd.setUnfinishedBorrowingDetailFrom(copyID);
			if(check) return bd;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
