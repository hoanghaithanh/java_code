package bookReferingAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

import common.DataAccessHelper;
/**
 * Class trừu tượng hóa của một bản ghi trong bảng borrowing_detail, chứa các thuộc tính được sử dụng bởi hệ thống và phương thức tương tác với CSDL
 * @author Thanh
 *
 */
public class BorrowingDetail extends DataAccessHelper {
	private String borrowingID;
	private String copyID;
	public String getCopyID() {
		return copyID;
	}
	public String getBorrowerID()
	{
		return borrowerID;
	}
	public void setCopyID(String copyID) {
		this.copyID = copyID;
	}

	public String getBorrowingID() {
		return borrowingID;
	}

	public String getBorrowerFullName() {
		return borrowerFullName;
	}

	public String getBookTitle() {
		return bookTitle;
	}
	private String borrowerID;
	private String borrowerFullName;
	private String bookTitle;
	private Date returnDate;
	private int penalty;
	private String status;
	/**
	 * Phương thức khởi tạo một chi tiết mượn rỗng
	 */
	public BorrowingDetail(){
		
	}
	
	/**
	 * Phương thức khởi tạo một chi tiết mượn từ một id lượt đăng ký mượn và id bản sao sách
	 * @param borrowingID id mượn đăng ký mượn
	 * @param copyID id bản sao sách
	 */
	public BorrowingDetail(String borrowingID, String copyID)
	{
		this.borrowingID = borrowingID;
		this.copyID = copyID;
		this.status = "pending";
		this.penalty = 0;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * Phương thức thêm một chi tiết mượn vào bảng borrowing_detail
	 * @return thêm thành công hay thất bại
	 * @throws SQLException
	 */
	public boolean addToDB() throws SQLException
	{
		String sql = "insert into borrowing_detail(borrowing_id, copy_id, status, final) values ("
				+this.borrowingID+", '"
				+this.copyID+"', 'pending', "
				+this.penalty+")";
		System.out.println(sql);
		boolean check =  updateQuery(sql);
		if(!check)
		{
			JOptionPane.showMessageDialog(null, "Loi ghi du lieu vao bang borrowing_detail");
			return check;
		}
		else
		{
			sql = "update book_copy set status = 'unavailable' where copy_id ='"+this.copyID+"'";
			System.out.println(sql);
			check = updateQuery(sql);
			if(!check)
			{
				JOptionPane.showMessageDialog(null, "Loi update du lieu trong bang copy_id ");
				
			}
			return check;
		}
	}
	/**
	 * Hủy một yêu cầu mượn sách
	 * @param borrowingID id lượt mượn
	 * @param copyID id bản sao sách
	 * @return hủy thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean cancelRequest(String borrowingID, String copyID) throws SQLException
	{
		String sql = "update borrowing_detail set status = 'cancelled' where borrowing_id = '"+borrowingID+"' and copy_id = '"+copyID+"'";
		String sql2 = "update book_copy set status = 'available' where copy_id = '"+copyID+"'";
		System.out.println(sql+"; "+sql2);
		boolean check = updateQuery(sql);
		check=check&updateQuery(sql2);
		if(!check)
		{
			JOptionPane.showMessageDialog(null, "Loi update du lieu!");
		}
		return check;
	}
	
	/**
	 * Phương thức chấp nhận một yêu cầu mượn sách
	 * @param borrowingID
	 * @param copyID
	 * @return chấp nhận thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean acceptRequest(String borrowingID, String copyID) throws SQLException
	{
		Date lentDate = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 14);
		Date expectedReturnDate = calendar.getTime();
		String lentDateString = (new SimpleDateFormat("yyyy-MM-dd").format(lentDate));
		String expectedReturnDateString = (new SimpleDateFormat("yyyy-MM-dd").format(expectedReturnDate));

		String sql = "update borrowing_detail set status = 'borrowing' where borrowing_id = '"+borrowingID+"' and copy_id = '"+copyID+"'";
		String sql2 = "update borrowing_general set lent_date = '"+lentDateString+"', expected_return_date = '"+ expectedReturnDateString+"' where borrowing_id = '"+borrowingID+"'";
		System.out.println(sql);
		boolean check = updateQuery(sql);
		check = check&updateQuery(sql2);
		if(!check)
		{
			JOptionPane.showMessageDialog(null, "Loi update du lieu trong bang borrowing_detail");
		}
		return check;
	}
	
	/**
	 * Phương thức ghi nhận một bản sao sách được trả về thư viện
	 * @return thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean returnCopy(int price) throws SQLException
	{
		if(!(price>0)) price  = 0;
		this.penalty = price;
		this.status = "returned";
		this.returnDate = new Date();
		SimpleDateFormat dfmt = new SimpleDateFormat("yyyy-MM-dd");
		
		String returnDateString = dfmt.format(this.returnDate);

		String sql = "update borrowing_detail as b_d set b_d.return_date = '"+returnDateString+"', b_d.status = '"+this.status+"', b_d.final = '"+this.penalty+"'"
				+ " where b_d.borrowing_id = "+this.borrowingID+" and b_d.copy_id ='"+this.copyID+"'";
		System.out.println(sql);
		boolean check = updateQuery(sql);
		if(check)
		{
		if(this.status.equals("cancelled")||this.status.equals("returned"))
		{
			sql = "update book_copy set status = 'available' where copy_id = '"+this.copyID+"'";
			System.out.println(sql);
			check = updateQuery(sql);
		}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Loi update du lieu trong bang borrowing_detail");
		}
		return check;
	}
	
	/**
	 * Phương thức lấy dữ liệu từ một bản ghi trong bảng borrowing_detail
	 * @param copyID mã bản sao sách
	 * @param borrowingID mã lượt mượn
	 * @return tồn tại bản ghi có copyID và borrowID hay không
	 * @throws SQLException
	 */
	public boolean setBorrowingDetailFrom(String copyID, String borrowingID) throws SQLException
	{
		String sql = "SELECT * FROM "
				+"borrowing_detail as b_d inner join borrowing_general as b_g on b_d.borrowing_id = b_g.borrowing_id "
				+"inner join card as c on c.card_id = b_g.card_id "
				+"inner join user as u on c.user_id = u.user_id "
				+"inner join book_copy as b_c on b_c.copy_id=b_d.copy_id "
				+"inner join book as b on b.book_id = b_c.book_id "
				+"where b_g.borrowing_id = '"+borrowingID+"' and b_d.copy_id = '"+copyID+"'";
		System.out.println(sql);
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(rs.next())
		{
			this.borrowerID = rs.getString("user_id");
			this.bookTitle = rs.getString("title");
			this.borrowerFullName = rs.getString("user_full_name");
			this.borrowingID = rs.getString("borrowing_id");
			this.copyID = copyID;
			this.penalty=rs.getInt("final");
			try {
				if(rs.getString("return_date")!=null)
				this.returnDate= (new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("return_date")));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.status = rs.getString("status");
			return true;
		}
		else return false;
	}
	
	/**
	 * Phương thức lấy dữ liệu từ một chi tiết mượn có trạng thái pending hoặc borrowing có mã bản sao cho trước
	 * @param copyID mã bản sao
	 * @return thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean setUnfinishedBorrowingDetailFrom(String copyID) throws SQLException
	{
		String sql = "SELECT * FROM "
				+"borrowing_detail as b_d inner join borrowing_general as b_g on b_d.borrowing_id = b_g.borrowing_id "
				+"inner join card as c on c.card_id = b_g.card_id "
				+"inner join user as u on c.user_id = u.user_id "
				+"inner join book_copy as b_c on b_c.copy_id=b_d.copy_id "
				+"inner join book as b on b.book_id = b_c.book_id "
				+"where b_d.status in ('pending','borrowing') and b_d.copy_id = '"+copyID+"'";
		System.out.println(sql);
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(rs.next())
		{
			this.borrowerID = rs.getString("user_id");
			this.bookTitle = rs.getString("title");
			this.borrowerFullName = rs.getString("user_full_name");
			this.borrowingID = rs.getString("borrowing_id");
			this.copyID = copyID;
			this.penalty=rs.getInt("final");
			try {
				if(rs.getString("return_date")!=null)
				this.returnDate= (new SimpleDateFormat("yyyy-MM-dd").parse(rs.getString("return_date")));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.status = rs.getString("status");
			return true;
		}
		else return false;
	}
	/**
	 * phương thức lấy danh sách các chi tiết mượn có status pending hoặc borrowing trong bảng ghi mà có id thẻ mượn cho trước
	 * @param cardID id thẻ mượn
	 * @return thành công hoặc thất bại
	 * @throws SQLException
	 */
	public ArrayList<BorrowingDetail> getDetailArrFrom(String cardID) throws SQLException
	{
		String sql = "Select * from borrowing_detail as b_d"
				+ " inner join borrowing_general as b_g on b_d.borrowing_id = b_g.borrowing_id  where b_d.status in ('pending','borrowing') and b_g.card_id = "+cardID;
		ArrayList<BorrowingDetail> detailArr = new ArrayList<BorrowingDetail>();
		ArrayList<String> copyIDArr = new ArrayList<String>();
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		while(rs.next())
		{
			copyIDArr.add(rs.getString("copy_id"));
		}
		for(String string:copyIDArr)
		{
			BorrowingDetail bd = new BorrowingDetail();
			bd.setUnfinishedBorrowingDetailFrom(string);
			detailArr.add(bd);
		}
		return detailArr;
	}
	
}
