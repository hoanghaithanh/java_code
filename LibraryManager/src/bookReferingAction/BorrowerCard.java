package bookReferingAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import common.DataAccessHelper;
/**
 * Lớp trừu tượng hóa của Thẻ mượn, chứa các thuộc tính căn bản của thẻ được hệ thống thư viện sử dụng, các các phương thức tương tác với cơ sở dữ liệu
 * @author Thanh
 *
 */
public class BorrowerCard extends DataAccessHelper {
	private String cardID;
	private String activateCode;
	private String userID;
	private Date expiredDate;
	private SimpleDateFormat spdf = new SimpleDateFormat("yyyy-MM-dd");
	/**
	 * Phương thức khởi tạo một đối tượng "rỗng"
	 */
	public BorrowerCard()
	{
	this.cardID=this.userID=this.activateCode="";
	}
	
	/**
	 * Phương thức khởi tạo một thẻ có mã kích hoạt và ngày hết hạn cho trước
	 * @param activateCode
	 * @param expiredDate
	 */
	
	public BorrowerCard(String activateCode, Date expiredDate)
	{
		this.activateCode = activateCode;
		this.expiredDate = expiredDate;
	}
	/**
	 * Phương thức đặt thuộc tính của thẻ từ một bản ghi trong CSDL có mã thẻ cho trước
	 * @param cardID
	 * @return thẻ có mã thẻ cardID
	 * @throws SQLException
	 */
	public boolean setCardFromID(String cardID) throws SQLException
	{
		String sql = "select * from card where cardID="+cardID;
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(rs.next())
		{
			this.cardID = rs.getString("card_id");
			this.activateCode = rs.getString("activateCode");
			try {
				this.expiredDate = spdf.parse(rs.getString("expired_date"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.userID = rs.getString("user_id");
		}
		else {
			JOptionPane.showMessageDialog(null, "Khong ton tai card co Id da nhap trong he thong!");
			return false;
		}
		return true;
	}
	/**
	 * Thêm một thẻ vào CSDL
	 * @return thêm thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean addCardToDB() throws SQLException
	{
		String sql = "insert to card(activate_code, expired_date) values ('"+this.activateCode
				+ "', '"+spdf.format(this.expiredDate)+"'" ;
		return updateQuery(sql);
	}
	/**
	 * Update thông tin về thẻ
	 * @return update thành công hoặc thất bại
	 * @throws SQLException
	 */
	
	
	public boolean updateCard() throws SQLException
	{
		String sql = "update card set user_id = '"+this.userID+"' where card_id = '"+this.cardID+"'";
		return updateQuery(sql);
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getCardID() {
		return cardID;
	}
}
