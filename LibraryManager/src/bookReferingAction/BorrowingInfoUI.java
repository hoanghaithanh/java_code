package bookReferingAction;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import account.BorrowerUI;
import account.User;
import common.Session;
/**
 * Class giao diện hiển thị thông tin về một lượt mượn
 * @author Thanh
 *
 */
public class BorrowingInfoUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private ReturnBorrowController rbController = new ReturnBorrowController();
	private String cardID;
	
/**
 * Khởi tạo giao diện hiển thị các thông tin mượn sách chưa kết thúc của một người dùng borrower
 * @param user
 */
	public BorrowingInfoUI(User user) {
		try {
			this.cardID = user.getCardID();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "User chua kich hoat!");
			dispose();
		}
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 872, 399);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(46, 87, 748, 184);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		genTableContentFromCardId();
		scrollPane.setViewportView(table);
	}
	/**
	 * phương thức sinh dữ liệu cho bảng trong giao diện, nội dung bảng là thông tin các mã mượn chi tiết có trạng thái là pending hoặc borrowing
	 */
	public void genTableContentFromCardId() {
		try {
			ArrayList<BorrowingDetail> listBorr = rbController.searchLentBookCopyFromCardID(cardID);
			if(listBorr.isEmpty())
			{
				JOptionPane.showMessageDialog(null, "Khong tim thay ket qua");
			}
			else
			{
			Vector<String> row, colunm;
			int count = 1;
			DefaultTableModel model = new DefaultTableModel() {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			String[] colunmNames = { "No.","BorrowingID","CopyID", "Title","BorrowerName", "BorrowerID","Status","Expected Return Date" };
			colunm = new Vector<String>();
			int numberColumn;
			numberColumn = colunmNames.length;
			for (int i = 0; i < numberColumn; i++) {
				colunm.add(colunmNames[i]);
			}
			model.setColumnIdentifiers(colunm);
			for (BorrowingDetail book : listBorr) {
				row = new Vector<String>();
				row.add(count + "");
				row.add(book.getBorrowingID());
				row.add(book.getCopyID());
				row.add(book.getBookTitle());
				row.add(book.getBorrowerFullName());
				row.add(book.getBorrowerID());
				row.add(book.getStatus());
				Date expectedReturnDate = (new BorrowingGeneral(book.getBorrowingID())).getExpectedReturnDate();
				if(expectedReturnDate==null)
				{
					row.add("NA");
				}
				else{
				row.add(expectedReturnDate.toString());
				}
				count++;
				model.addRow(row);
			}
			table.setModel(model);
			table.setVisible(true);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Loi", "Co loi xay ra!", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
}
