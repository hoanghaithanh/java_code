package bookReferingAction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import common.DataAccessHelper;
/**
 * Class trừu tượng hóa mọt lượt mượn, có các thuộc tính là dữ liệu được sử dụng bởi hệ thống và các phương thức tác động với CSDL
 * @author Thanh
 *
 */
public class BorrowingGeneral extends DataAccessHelper {
	private String borrowingID;
	private String cardID;
	private Date registerDate;
	private Date lentDate;
	private Date expectedReturnDate;
	private SimpleDateFormat datefmt = new SimpleDateFormat("yyyy-MM-dd");
	
	
	/**
	 * Phương thức khởi tạo lượt mượn từ mã thẻ mượn và ngày đăng ký
	 * @param cardID mã thẻ mượn
	 * @param registerDate ngày đăng ký
	 */
	public BorrowingGeneral(String cardID, Date registerDate)
	{
		this.cardID = cardID;
		this.registerDate = registerDate;
	}

	public Date getLentDate() {
		return lentDate;
	}
/** 
 * Phương thức đăt ngày mượn và hạn trả, hạn trả được set mặc định sau 2 tuần kể từ ngày mượn
 * @param lentDate ngày mượn
 */
	public void setLentDate(Date lentDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(lentDate);
		calendar.add(Calendar.DATE, 14);
		this.lentDate = lentDate;
		this.expectedReturnDate = calendar.getTime();
	}

	public Date getExpectedReturnDate() {
		return expectedReturnDate;
	}
	
	/**
	 * Phương thức update thông tin về lượt mượn, bao gồm ngày mượn và ngày trả, rồi lưu vào csdl
	 * @return
	 * @throws SQLException
	 */
	public boolean updateInfo() throws SQLException
	{
		
		String lentDate = datefmt.format(this.lentDate);
		String expectedReturnDate = datefmt.format(this.expectedReturnDate);
		
		String sql = "update borrowing_general "
				+ "set lent_date = '"+lentDate+"', "
				+ "expected_return_date = '"+expectedReturnDate+"'"
				+ "where borrowing_id = '"+this.borrowingID+"'";
		return updateQuery(sql);
	}
	
	/**
	 * Phương thức khởi tạo một lượt mượn từ mã lượt mượn
	 * @param borrowingID
	 * @throws SQLException
	 */
	public BorrowingGeneral(String borrowingID) throws SQLException
	{
		String sql = "select * from borrowing_general where borrowing_id = '"+borrowingID+"'";
		System.out.println(sql);
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(rs.next())
		{
			this.borrowingID = borrowingID;
			this.cardID = rs.getString("card_id");
			try{
			this.expectedReturnDate = rs.getDate("expected_return_date");
			this.lentDate = rs.getDate("lent_date");
			}catch(Exception e)
			{
				
			}
			this.registerDate = rs.getDate("register_date");
		}
	}
	/**
	 * Phương thức thêm một bản ghi có thông tin lấy từ thuộc tính của lớp này vào csdl
	 * @return thêm thành công hoặc không
	 * @throws SQLException
	 */
	public boolean addToDB() throws SQLException
	{
		String registerDate = datefmt.format(this.registerDate);
		String sql = "insert into borrowing_general(card_id, register_date) values ('"+ this.cardID +"','" + registerDate + "')";
		String sql2 = "SELECT `AUTO_INCREMENT`"
				+" FROM  INFORMATION_SCHEMA.TABLES"
				+" WHERE TABLE_SCHEMA = 'librarydb'"
				+" AND   TABLE_NAME   = 'borrowing_general'";
		getConnection();
		int check = getStmt().executeUpdate(sql);
		ResultSet rs = getStmt().executeQuery(sql2);
		rs.next();
		this.borrowingID = Integer.toString(Integer.parseInt(rs.getString("AUTO_INCREMENT"))-1);
		closeConnection();
		if(check == 1) return true;
		else return false;
	}

	public String getBorrowingID() {
		return borrowingID;
	}
}
