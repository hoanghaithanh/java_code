package bookReferingAction;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bookStore.Books;

import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.ParseException;
import java.awt.event.ActionEvent;
/** 
 * Lớp giao diện thể hiện thông tin của một giỏ sách
 * @author Thanh
 *
 */
public class CartInfoUI extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private Cart cart;


	/**
	 * Create the frame.
	 */
	public CartInfoUI(Cart cart) {
		this.cart = cart;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnDeleteBookFrom = new JButton("Delete Book from cart");
		btnDeleteBookFrom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] selectedRows = table.getSelectedRows();
				for (int i:selectedRows)
				{
					Books book = new Books();
					book.setBookFromID(table.getModel().getValueAt(i, 1).toString());
					System.out.println(book.getBookID());
					System.out.println(cart.deleteFromCart(book));
					
				}
				genTableContent();
			}
		});
		btnDeleteBookFrom.setBounds(10, 239, 161, 23);
		contentPane.add(btnDeleteBookFrom);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					cart.submitCart();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSubmit.setBounds(385, 239, 89, 23);
		contentPane.add(btnSubmit);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 65, 464, 137);
		contentPane.add(scrollPane);
		
		table = new JTable();
		genTableContent();
		scrollPane.setViewportView(table);
	}
	/**
	 * Phương thức sinh dữ liệu cho bảng
	 */
	public void genTableContent() {
		try {
			ArrayList<Books> listBook = this.cart.getBookArray();
			Vector<String> row, colunm;
			int count = 1;
			DefaultTableModel model = new DefaultTableModel() {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			String[] colunmNames = { "No.","Ma Sach", "Ten Sach", "Tac gia", "Nha xuat ban","The loai" };
			colunm = new Vector<String>();
			int numberColumn;
			numberColumn = colunmNames.length;
			for (int i = 0; i < numberColumn; i++) {
				colunm.add(colunmNames[i]);
			}
			model.setColumnIdentifiers(colunm);
			for (Books book : listBook) {
				row = new Vector<String>();
				row.add(count + "");
				row.add(book.getBookID());
				row.add(book.getTitle());
				row.add(book.getAuthor());
				row.add(book.getPublisher().getPublisherName());
				row.add(book.getCategory().getCategoryName());
				count++;
				model.addRow(row);
			}
			table.setModel(model);
			table.setVisible(true);
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Loi", "Co loi xay ra!", JOptionPane.ERROR_MESSAGE);
		}
	}
}
