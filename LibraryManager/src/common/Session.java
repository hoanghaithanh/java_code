package common;

import java.sql.SQLException;

import account.StartingUI;
import account.User;
/**
 * Lớp trừu tượng hóa Phiên làm việc của một user
 * @author Thanh
 *
 */
public class Session {
	private static User user;

	private static boolean Sessionstatus;

	public Session(User user) throws SQLException
	{
		this.user=user;
		this.Sessionstatus = true;
	}
	
	public static String getCardID() throws SQLException
	{
		return user.getCardID();
	}
	
	public static String getUserID() {
		return user.getUserID();
	}

	public static void logout()
	{
		Sessionstatus = false;
		StartingUI sttUI = new StartingUI();
		sttUI.setVisible(true);
	}

	public static String getUserName() {
		return user.getUserName();
	}


	public static boolean getSessionstatus() {
		return Sessionstatus;
	}

	public static String getUserstatus() {
		return user.getStatus();
	}

	public static User getUser() {
		return user;
	}

	public static String getRole() {
		return user.getRole();
	}

}
