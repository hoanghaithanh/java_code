package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DataAccessHelper {

	protected static Connection connection;
	private static Statement stmt;
	protected static String userName = "root";
    protected static String password = "";
    protected static String connectionURL = "jdbc:mysql://localhost:3306/librarydb";

	protected static void getConnection() {
		
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		try {
			if (connection == null)
				connection = DriverManager.getConnection(connectionURL, userName, password);
			stmt = connection.createStatement();
			stmt.setQueryTimeout(30); // set timeout to 30 sec.
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Statement getStmt() {
		return stmt;
	}

	protected void closeConnection() {
		try {
			if (connection != null) {
				connection.close();
				connection = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	protected boolean updateQuery(String updateQuery) throws SQLException {
		getConnection();
		int result = -1;
		result = stmt.executeUpdate(updateQuery);
		closeConnection();
		if (result != -1)
			return true;
		else
			return false;
	}
}
