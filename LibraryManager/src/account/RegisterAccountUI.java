package account;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import common.Constant;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
/**
* 
* Class Giao diện cho việc đăng ký tài khoản mới
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/
public class RegisterAccountUI extends JFrame {

	private JPanel contentPane;
	private JTextField userNameTF;
	private JTextField emailTF;
	private JTextField contactTF;
	private JLabel lblGender;
	private JTextField studentIDTF;
	private JTextField courseTF;
	private JTextField fullNameTF;
	private JRadioButton rdbtnMale;
	private JRadioButton rdbtnFemale;
	private JPasswordField passTF;
	private JPasswordField rePassTF;


	/**
	 * Phương thức khởi tạo giao diện
	 */
	public RegisterAccountUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUsername = new JLabel("*Username:");
		lblUsername.setBounds(10, 63, 83, 14);
		contentPane.add(lblUsername);
		
		userNameTF = new JTextField();
		userNameTF.setBounds(153, 60, 148, 20);
		contentPane.add(userNameTF);
		userNameTF.setColumns(10);
		
		JLabel lblPassword = new JLabel("*Password: ");
		lblPassword.setBounds(10, 108, 83, 14);
		contentPane.add(lblPassword);
		
		JLabel lblRetypePassword = new JLabel("*Retype Password:");
		lblRetypePassword.setBounds(10, 156, 103, 14);
		contentPane.add(lblRetypePassword);
		
		JLabel lblEmail = new JLabel("*Email:");
		lblEmail.setBounds(10, 214, 46, 14);
		contentPane.add(lblEmail);
		
		emailTF = new JTextField();
		emailTF.setBounds(153, 211, 148, 20);
		contentPane.add(emailTF);
		emailTF.setColumns(10);
		
		JLabel lblContact = new JLabel("*Contact:");
		lblContact.setBounds(10, 275, 46, 14);
		contentPane.add(lblContact);
		
		contactTF = new JTextField();
		contactTF.setBounds(153, 272, 148, 20);
		contentPane.add(contactTF);
		contactTF.setColumns(10);
		
		lblGender = new JLabel("*Gender");
		lblGender.setBounds(10, 330, 46, 14);
		contentPane.add(lblGender);
		
		rdbtnFemale = new JRadioButton("Female");
		rdbtnFemale.setBounds(232, 326, 69, 23);
		contentPane.add(rdbtnFemale);
		
		rdbtnMale = new JRadioButton("Male");
		rdbtnMale.setBounds(153, 326, 69, 23);
		contentPane.add(rdbtnMale);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnMale);
		group.add(rdbtnFemale);
		group.setSelected(rdbtnMale.getModel(),true);
		
		JLabel lblNewLabel = new JLabel("Student ID:");
		lblNewLabel.setBounds(10, 386, 83, 14);
		contentPane.add(lblNewLabel);
		
		studentIDTF = new JTextField();
		studentIDTF.setBounds(153, 383, 148, 20);
		contentPane.add(studentIDTF);
		studentIDTF.setColumns(10);
		
		JLabel lblStudentCourse = new JLabel("Student Course:");
		lblStudentCourse.setBounds(10, 438, 83, 14);
		contentPane.add(lblStudentCourse);
		
		courseTF = new JTextField();
		courseTF.setBounds(153, 435, 148, 20);
		contentPane.add(courseTF);
		courseTF.setColumns(10);
		
		JLabel lblFullName = new JLabel("*Full Name:");
		lblFullName.setBounds(10, 22, 46, 14);
		contentPane.add(lblFullName);
		
		fullNameTF = new JTextField();
		fullNameTF.setBounds(153, 19, 148, 20);
		contentPane.add(fullNameTF);
		fullNameTF.setColumns(10);
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				actionButtonPerformed();
	}
	
	/**
	* Phương thức kiểm tra các inputField trong giao diện
	*/
	public boolean checkField()
	{
		boolean check = userNameTF.getText().isEmpty()||passTF.getText().isEmpty()||rePassTF.getText().isEmpty()||emailTF.getText().isEmpty()||contactTF.getText().isEmpty()||fullNameTF.getText().isEmpty();
		if (check)
		{
			JOptionPane.showMessageDialog(null, "Can dien day du thong tin yeu cau (*)!");
			return check;
		}
		else
		{
			if(!passTF.getText().equals(rePassTF.getText()))
			{
				JOptionPane.showMessageDialog(null, "Hai truong mat khau phai khop nhau!");
				return true;
			}
			else return false;
		}
		
	}
	/**
	* Phương thức xử lý khi nút submit được ấn
	*/
	private void actionButtonPerformed()
	{
		if(checkField()){
			return;
		}
		else{
			User user = new User();
			try {
				if(!(user.setUserFromUserName(userNameTF.getText())==Constant.USERID_NOT_EXIST))
					{
					JOptionPane.showMessageDialog(null, "Da ton tai username tren trong he thong!");
					return;
					}
				else
				{
					user.setUserName(userNameTF.getText());
					user.setPassWord(passTF.getText());
					user.setContact(contactTF.getText());
					user.setEmail(emailTF.getText());
					user.setFullName(fullNameTF.getText());
					if(group.isSelected(rdbtnMale.getModel()))
					{
						user.setGender('m');
					}
					else
					{
						user.setGender('f');
					}
					user.setRole("borrower");
					user.setStatus("inactivated");
					if(!studentIDTF.getText().isEmpty())
					{
					user.setStudent_id(studentIDTF.getText());
					user.setStudent_course(courseTF.getText());
					}
					else
					{
						user.setStudent_id("null");
						user.setStudent_course("");
					}
					if(user.addUserToDB())
						{
						JOptionPane.showMessageDialog(null, "Dang ky thanh cong!");
						StartingUI newUI = new StartingUI();
						newUI.setVisible(true);
						dispose();
						};
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
});
btnRegister.setBounds(153, 478, 89, 23);
contentPane.add(btnRegister);

passTF = new JPasswordField();
passTF.setBounds(153, 105, 148, 20);
contentPane.add(passTF);

rePassTF = new JPasswordField();
rePassTF.setBounds(153, 153, 148, 20);
contentPane.add(rePassTF);
	}
}
