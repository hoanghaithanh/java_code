package account;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
/**
* 
* Class giao diện dành cho Admin hệ thống
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/

public class AdminUI extends JFrame {

	private JPanel contentPane;
	/**
	* Khởi tạo giao diện
	*/
	public AdminUI(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAccount = new JMenu("Account");
		menuBar.add(mnAccount);
		
		JMenuItem mntmViewAccountInfo = new JMenuItem("View Account Info");
		mnAccount.add(mntmViewAccountInfo);
		
		JMenuItem mntmEditAccountInfo = new JMenuItem("Edit Account Info");
		mnAccount.add(mntmEditAccountInfo);
		
		JMenuItem mntmLogOut = new JMenuItem("Log out");
		mnAccount.add(mntmLogOut);
		
		JMenu mnAction = new JMenu("Action");
		menuBar.add(mnAction);
		
		JMenuItem mntmAddCard = new JMenuItem("Add card");
		mnAction.add(mntmAddCard);
		
		JMenuItem mntmAddLibrarianAccount = new JMenuItem("Add librarian Account");
		mnAction.add(mntmAddLibrarianAccount);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Browse Acount");
		mnAction.add(mntmNewMenuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
