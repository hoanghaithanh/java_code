package account;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import common.Constant;
import common.DataAccessHelper;
/**
* 
* Class thể hiện người dùng, bao gồm các thuộc tính của người dùng được sử dụng trong hệ thống và các phương thức tương tác với cơ sở dữ liệu
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/

public class User extends DataAccessHelper {
	private String userID;
	private String userName;
	private String passWord;
	private String email;
	private String fullName;
	private char gender;
	private String student_id;
	private String student_course;
	private String contact;
	private String status;
	private String role;
	
	/**
	* 
	* Phương thức khởi tạo một User với các trường thuộc tính "rỗng"
	*/
	
	public User()
	{
		this.userName = "";
		this.passWord = "";
		this.email = "";
		this.gender = ' ';
		this.student_id = "null";
		this.student_course = "null";
		this.contact = "";
		this.status = "";
		this.role = "";
	}
	/**
	* 
	* Phương thức khởi tạo một User với các trường thuộc tính sau
	* @param userName tên đăng nhập
	* @param passWord mật khẩu đăng nhập
	* @param email email người dùng
	* @param fullName tên đầy đủ
	* @param gender giới tính
	* @param student_id mã sinh viên
	* @param student_course khóa học
	* @param contact liên lạc (số điện thoại)
	* @param role vai trò
	*/
	public User(String userName,String passWord,String email,String fullName,char gender,String student_id, String student_course,String contact, String status,String role)
	{
		this.userName = userName;
		this.passWord = passWord;
		this.email = email;
		this.fullName=fullName;
		this.gender = gender;
		this.student_id = student_id;
		this.student_course = student_course;
		this.contact = contact;
		this.status = status;
		this.role = role;
	}
	
	/**
	 * 
	 * @return result trả về cardID của người dùng
	 * @throws SQLException
	 */
	public String getCardID() throws SQLException
	{
		String result = "";
		String sql = "select card_id from card where user_id = "+this.userID;
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(rs.next())
		{
			int i = rs.getInt("card_id");
			result = String.valueOf(i);
			System.out.println(result);
		}
		closeConnection();
		return result;
	}
	/**
	 * phương thức trả về tên đầy đủ người dùng
	 * @return fullName tên người dùng
	 */
	public String getFullName() {
		return fullName;
	}
	
	/**
	 * phương thức thiết lập tên đầy đủ
	 * @param fullName
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
/**
 * Phương thức thiết lập các thuộc tính của class theo bản ghi trong cơ sở dữ liệu có user_id = userID
 * @param userID
 * @return
 * @throws SQLException
 */
	public int setUserFromID(String userID) throws SQLException
	{
		String sql = "select * from user where user_id = '"+userID+"'";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(!rs.next())
		{
			closeConnection();
			return Constant.USERID_NOT_EXIST;
			
		}
		else
		{
			this.userName = rs.getString("username");
			this.email = rs.getString("email");
			this.fullName = rs.getString("user_full_name");
			this.passWord = rs.getString("password");
			this.gender = (rs.getString("gender")).charAt(0);
			this.student_id = rs.getString("student_id");
			this.student_course = rs.getString("student_course");
			this.contact = rs.getString("contact");
			this.status = rs.getString("status");
			this.role = rs.getString("role");
			closeConnection();
			return Constant.SUCCESS;
		}
	}
	/**
	 * Phương thức thiết lập các thuộc tính của class theo bản ghi trong cơ sở dữ liệu có userName cho trước
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public int setUserFromUserName(String username) throws SQLException
	{
		String sql = "select * from user where username = '"+username+"'";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(!rs.next())
		{
			closeConnection();
			return Constant.USERID_NOT_EXIST;
			
		}
		else
		{
			this.userID = rs.getString("user_id");
			this.userName = rs.getString("username");
			this.email = rs.getString("email");
			this.fullName = rs.getString("user_full_name");
			this.passWord = rs.getString("password");
			this.gender = (rs.getString("gender")).charAt(0);
			this.student_id = rs.getString("student_id");
			this.student_course = rs.getString("student_course");
			this.contact = rs.getString("contact");
			this.status = rs.getString("status");
			this.role = rs.getString("role");
			closeConnection();
			return Constant.SUCCESS;
		}
	}
	/**
	 * Phương thức trả về ID người dùng
	 * @return ID người dùng
	 */
	public String getUserID() {
		return userID;
	}
	
	/**
	 * Xóa bản ghi khỏi có sở dữ liệu mà từ đó class này được khởi tạo
	 * @return xóa thành công hay thất bại
	 * @throws SQLException
	 */
	public boolean deleteUser() throws SQLException
	{
		String userID = this.student_id;
		String sql = "delete from user where user_id = '"+userID+"'";
		getConnection();
		boolean check = updateQuery(sql);
		closeConnection();
		return check;
	}
/**
 * phương thức lấy username
 * @return tên đăng nhập
 */
	public String getUserName() {
		return userName;
	}
	/**
	 * phương thức đặt username
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * phương thức lấy mật khẩu
	 * @return mật khẩu
	 */
	public String getPassWord() {
		return passWord;
	}
	/**
	 * phương thức đặt mật khẩu
	 */
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	/**
	 * phương thức lấy email
	 * @return email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * phương thức đặt email
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
/**
 * phương thức lấy giới tính
 * @return giới tính
 */
	public char getGender() {
		return gender;
	}
/** phương thức đặt giới tính
 * @param gender
 */
	public void setGender(char gender) {
		this.gender = gender;
	}
/**
 * phương thức lấy mã sinh viên
 * @return mã sinh viên
 */
	public String getStudent_id() {
		return student_id;
	}
	/**
	 * phương thức đặt mã sinh viên
	 */
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	/**
	 * phương thức lấy khóa học
	 */
	public String getStudent_course() {
		return student_course;
	}
	/**
	 * phương thức đặt khóa học
	 */
	public void setStudent_course(String student_course) {
		this.student_course = student_course;
	}
	/**
	 * phương thức lấy liên hệ
	 * @return liên hệ
	 */
	public String getContact() {
		return contact;
	}
	/**
	 * phương thức đặt liên hệ
	 */
	public void setContact(String contact) {
		this.contact = contact;
	}
	/**
	 * phương thức lấy tình trạng tài khoản
	 * @return tình trạng tài khoản
	 */
	public String getStatus() {
		return status;
	}
/**
 * phương thức đặt tình trạng tài khoản
 * @param status
 */
	public void setStatus(String status) {
		this.status = status;
	}
/**
 * phương thức lấy vai trò trong hệ thống
 * @return vai trò trong hệ thống
 */
	public String getRole() {
		return role;
	}
/**
 * phương thức đặt vai trò cho user
 * @param role
 */
	public void setRole(String role) {
		this.role = role;
	}
	
//	public boolean updateUser() throws SQLException
//	{
//		String sql = "update user set password = '"+this.passWord+"', email = '"+this.email+"', user_full_name = '"+this.fullName+"', contact = '"+this.contact+"', gender = '"+this.gender+"', student_id = '"
//				+this.student_id+"', student_course = '"+this.student_course+"', status = '"+this.status+"'";
//		getConnection();
//		boolean check = updateQuery(sql);
//		closeConnection();
//		return check;
//	}
	/**
	 * phương thức thêm một bản ghi vào bảng user có thông tin như thông tin hiện tại của object
	 * @return thành công hoặc thất bạ
	 * @throws SQLException
	 */
	public boolean addUserToDB() throws SQLException
	{
		String sql = "insert into user(username, password, email, user_full_name, contact, gender, student_id, student_course,status,role )" 
				+ " values ('"+this.userName+"','"+this.passWord+"', '"+this.email+"', '"+this.fullName+"', '"+this.contact+"', '"
				+this.gender+"', "+this.student_id+", '"+this.student_course+"', '"+this.status+"', '"+this.role+"')";
		System.out.println(sql);
		getConnection();
		boolean check = updateQuery(sql);
		closeConnection();
		return check;
	}
	/**
	 * Phương thức kích hoạt tài khoản
	 * @param activateCode mã kích hoạt trên thẻ
	 * @return kích hoạt thành công hoặc thất bại
	 * @throws SQLException
	 */
	public boolean activateAccount(String activateCode) throws SQLException
	{
		String sql = "Select * from card where activate_code = '"+activateCode+"' and user_id is null and expired_date > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY)";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if(rs.next())
		{
			this.setStatus("activated");
			String sql2 = "Update card set user_id = '"+this.userID+"' where activate_code = '"+activateCode+"'";
			String sql3 = "Update user set status ='activated' where user_id ='"+this.userID+"'";
		getStmt().execute(sql2);
		getStmt().execute(sql3);
		closeConnection();
		return true;
		}
		else
		{
			closeConnection();
			JOptionPane.showMessageDialog(null, "The khong ton tai hoac da het han!");
			return false;
		}
		
	}
}
