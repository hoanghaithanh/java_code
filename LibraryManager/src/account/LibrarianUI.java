package account;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bookReferingAction.AcceptBorrowUI;
import bookStore.BrowseCatalogUI;
import bookStore.RegisterNewBookUI;
import common.Session;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
* 
* Class giao diện dành cho Thủ thư
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/
public class LibrarianUI extends JFrame {

	private JPanel contentPane;


	/**
	 * Phương thức khởi tạo giao diện cho thủ thư
	 * @param user đối tượng user khởi tạo với vai trò thủ thư
	 */
	public LibrarianUI(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAccount = new JMenu("Account");
		menuBar.add(mnAccount);
		
		JMenuItem mntmAccountInfo = new JMenuItem("Account Info");
		mnAccount.add(mntmAccountInfo);
		
		JMenuItem mntmLogOut = new JMenuItem("Log out");
		mntmLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Session.logout();
				dispose();
			}
		});
		mnAccount.add(mntmLogOut);
		
		JMenu mnAction = new JMenu("Action");
		menuBar.add(mnAction);
		
		JMenuItem mntmBrowseBookCatalog = new JMenuItem("Browse Book Catalog");
		mntmBrowseBookCatalog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BrowseCatalogUI browCataUI = new BrowseCatalogUI();
				browCataUI.setVisible(true);
				
			}
		});
		mnAction.add(mntmBrowseBookCatalog);
		
		JMenuItem mntmRegisterNewBook = new JMenuItem("Register New Book");
		mntmRegisterNewBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterNewBookUI regNewBookUI = new RegisterNewBookUI();
				regNewBookUI.setVisible(true);
				
			}
		});
		mnAction.add(mntmRegisterNewBook);
		
		JMenuItem mntmCheckRequest = new JMenuItem("Check Unfinished Borrowing Info");
		mntmCheckRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AcceptBorrowUI accBorrUI = new AcceptBorrowUI();
				accBorrUI.setVisible(true);
				
			}
		});
		mnAction.add(mntmCheckRequest);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
