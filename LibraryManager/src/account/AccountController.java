package account;

import java.sql.SQLException;

import javax.swing.JOptionPane;
/**
* 
* Class chủ yếu cung cấp các phương thức điều khiển Account
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/
public class AccountController {
	 /**
	   * Phương thức cho phép đăng nhập vào hệ thống

	   * @param username Day la username của người dùng
	   * @param password Day là pasword của người dùng
	   * @return User trả về một đối tượng User nếu như đâng nhập thành công
	   */
	public User login(String username, String password) throws SQLException
	{
		User user = new User();
		int check = user.setUserFromUserName(username);
		if(check==common.Constant.USERID_NOT_EXIST)
		{
			JOptionPane.showMessageDialog(null, "Khong ton tai username trong he thong!");
			return null;
		}
		else
			if(!user.getPassWord().equals((String)password))
			{
				JOptionPane.showMessageDialog(null, "Mat khau sai!");
				return null;
			}
			else return user;
	}
	
	/**
	   * Phương thức cho phép khởi tạo một User từ một username có sẵn

	   * @param username Day la username của người dùng
	   *
	   * @return User trả về một đối tượng User nếu như tìm kiếm thành công
	   */
	public User searchUser(String username) throws SQLException
	{
		User user = new User();
		int check = user.setUserFromUserName(username);
		{
			if(check==common.Constant.USERID_NOT_EXIST)
			{
				JOptionPane.showMessageDialog(null, "Khong tim thay user!");
				return null;
			}
			else return user;
		}
	}
	
	/**
	   * Phương thức cho phép xóa một trường dữ liệu trong bảng user mà có username sẵn

	   * @param username Day la username của người dùng
	   *
	   * @return boolean true nếu xóa thành công.
	   */
	public boolean deleteUser(String username) throws SQLException
	{
		User user = new User();
		user.setUserFromUserName(username);
		return user.deleteUser();
	}
	
	/**
	   * Phương thức cho phép kích hoạt tài khoản của một user

	   * @param user Day la user cần kích hoạt tài khoản
	   * @param activateCode Đây là activate_code của thẻ
	   *
	   * @return boolean trả về true nếu kích hoạt thành công
	   */
	public boolean activateUser(User user, String activateCode) throws SQLException
	{
		return user.activateAccount(activateCode);
	}
	
}
