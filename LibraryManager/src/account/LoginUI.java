package account;

import java.awt.EventQueue;

import javax.swing.JFrame;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import common.Session;

import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.crypto.SealedObject;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class LoginUI extends JFrame {

	private JTextField textField;
	private JPasswordField passwordField;
	private AccountController accountController = new AccountController();
	private JLabel label;
	/**
	 * Launch the application.
	 */

	/**
	 * Phương thức khởi tạo giao diện
	 */
	public LoginUI() {
		initialize();
	}

	/**
	 * Phương thức tạo khung giao diện
	 */
	private void initialize() {
		this.setBounds(100, 100, 450, 300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getContentPane().setLayout(null);
		
		JLabel lblUsername = new JLabel("UserName:");
		lblUsername.setBounds(75, 65, 68, 14);
		this.getContentPane().add(lblUsername);
		
		textField = new JTextField();
		textField.setBounds(168, 59, 164, 20);
		this.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Password: ");
		lblNewLabel.setBounds(75, 107, 68, 14);
		this.getContentPane().add(lblNewLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(168, 104, 164, 20);
		this.getContentPane().add(passwordField);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean check = true;
				if(check&textField.getText().isEmpty())
				{
					label.setText("*Nhap username");
				}
				else
					if(check&passwordField.getText().isEmpty())
					{
						label.setText("*Nhap password");
					}
					else{
				User user;
				try {
					user = accountController.login(textField.getText(), passwordField.getText());
					if(user==null)
					{
						System.out.println("login failed");
					}
					else
					{
						Session sess = new Session(user);
						UserInterfaceUI UI = new UserInterfaceUI(Session.getUser());
						dispose();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					}
			}
		});
		btnLogin.setBounds(129, 170, 89, 23);
		this.getContentPane().add(btnLogin);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(-1);
			}
		});
		btnCancel.setBounds(243, 170, 89, 23);
		this.getContentPane().add(btnCancel);
		
		label = new JLabel("");
		label.setBounds(168, 237, 164, 14);
		this.getContentPane().add(label);
	}
}
