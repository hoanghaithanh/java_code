package account;
/**
 * Class khởi tạo các giao diện dựa trên vai trò của người dùng
 * @author Thanh
 *
 */
public class UserInterfaceUI {
	/**
	 * Khởi tạo theo user đăng nhập
	 * @param user
	 */
public UserInterfaceUI(User user)
{
	if(user.getRole().equals("borrower"))
	{
		BorrowerUI borrUI = new BorrowerUI(user);
		borrUI.setVisible(true);
		
	}
	if(user.getRole().equals("librarian"))
	{
		LibrarianUI libUI = new LibrarianUI(user);
		libUI.setVisible(true);
		
	}
	if(user.getRole().equals("admin"))
	{
		AdminUI adUI = new AdminUI(user);
		adUI.setVisible(true);
		
	}
}
}
