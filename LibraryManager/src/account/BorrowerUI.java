package account;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bookReferingAction.BorrowingInfoUI;
import bookReferingAction.RegisterToBorrowBookUI;
import bookStore.BrowseCatalogUI;
import common.Session;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
/**
* 
* Class giao diện dành cho người dùng với vai trò mượn sách
*
* @author  Hoang Hai Thanh
* @since   2016-10-20
*/
public class BorrowerUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Phương thức Khởi tạo Giao diện
	 * @param user người sử dụng giao diện
	 */
	public BorrowerUI(User user) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAccount = new JMenu("Account");
		menuBar.add(mnAccount);
		
		JMenuItem mntmAccountInfo = new JMenuItem("Account Info");
		mnAccount.add(mntmAccountInfo);
		
		JMenuItem mntmEditAccountInfo = new JMenuItem("Edit Account Info");
		mnAccount.add(mntmEditAccountInfo);
		
		JMenuItem mntmChangePassword = new JMenuItem("Change Password");
		mnAccount.add(mntmChangePassword);
		
		JMenuItem mntmActivateAccount = new JMenuItem("Activate Account");
		mntmActivateAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String activateCode = JOptionPane.showInputDialog("Nhap ma activate code tren the");
				if(activateCode.isEmpty())
				{
					JOptionPane.showMessageDialog(null, "Ma activate code rong!");
					return;
				}
				else
				{
					try {
						boolean check = Session.getUser().activateAccount(activateCode);
						if(check)
						{
							JOptionPane.showMessageDialog(null, "Activate thanh cong!");
							BorrowerUI newUI = new BorrowerUI(Session.getUser());
							newUI.setVisible(true);
							dispose();
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			}
		});
		mnAccount.add(mntmActivateAccount);
		
		
		
		JMenuItem mntmLogout = new JMenuItem("Logout");
		mntmLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Session.logout();
				dispose();
			}
		});
		mnAccount.add(mntmLogout);
		
		JMenu mnAction = new JMenu("Action");
		menuBar.add(mnAction);
		
		JMenuItem mntmBrowseBooksCatalog = new JMenuItem("Browse Books Catalog");
		mntmBrowseBooksCatalog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BrowseCatalogUI brscata = new BrowseCatalogUI();
				brscata.setVisible(true);
			
			}
		});
		mnAction.add(mntmBrowseBooksCatalog);
		
		JMenuItem mntmRegisterToBorrow = new JMenuItem("Register To Borrow Books");
		mntmRegisterToBorrow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterToBorrowBookUI reg = new RegisterToBorrowBookUI();
				reg.setVisible(true);
				
			}
		});
		mnAction.add(mntmRegisterToBorrow);
		
		JMenuItem mntmBorrowingBookInfo = new JMenuItem("Borrowing Book Info");
		mntmBorrowingBookInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BorrowingInfoUI borrInfoUI = new BorrowingInfoUI(user);
				borrInfoUI.setVisible(true);
			
			}
		});
		mnAction.add(mntmBorrowingBookInfo);
		if(Session.getUserstatus().equals("activated"))
		{
			mntmActivateAccount.setEnabled(false);
		}
		else
		{
			mntmRegisterToBorrow.setEnabled(false);
			mntmBorrowingBookInfo.setEnabled(false);
			
		}
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

}
