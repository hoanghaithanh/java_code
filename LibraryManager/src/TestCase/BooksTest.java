package TestCase;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import bookStore.Books;

public class BooksTest {
private Books book = new Books();
private ArrayList<Books> bookArr = new ArrayList<Books>();

@Test
public void testInitBookCass1()
{
	assertTrue(book.setBookFromID("CT0002"));
	assertEquals("title1", book.getTitle());
	assertEquals("CT0002", book.getBookID());
	assertEquals(12, book.getPublisher().getPublisherID());
	assertEquals("CT", book.getCategory().getCategoryID());
	assertEquals("1234567891", book.getIsbn());
}

@Test
public void testInitBookCase2()
{
	assertTrue(book.setBookFromID("KH0001"));
	assertEquals("title kh 1", book.getTitle());
	assertEquals("KH0001", book.getBookID());
	assertEquals(13, book.getPublisher().getPublisherID());
	assertEquals("KH", book.getCategory().getCategoryID());
	assertEquals("865423", book.getIsbn());
}


@Test
public void testSearchBook() throws SQLException
{
	assertTrue(book.setBookFromID("KH0001"));
	bookArr.add(book);
	book = new Books();
	book.setBookFromID("CT0002");
	bookArr.add(book);
	assertEquals(bookArr.get(0).getBookID(), book.getListBook("","","","","865423").get(0).getBookID());
	assertEquals(bookArr.get(1).getBookID(), book.getListBook("","","","","1234567891").get(0).getBookID());
}
}
