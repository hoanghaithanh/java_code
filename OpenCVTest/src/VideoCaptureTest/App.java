package VideoCaptureTest;


import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.javaopencvbook.utils.ImageProcessor;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.Videoio;
import org.opencv.videoio.VideoCapture;


import org.opencv.core.Core;

public class App {
	static{System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
	private JFrame frame;
	private JLabel imageLabel;
	
	public static void main(String[] args)
	{
		App app = new App();
		app.initGUI();
		app.runMainLoop(args);
	}
	
	private void initGUI(){
		frame = new JFrame("Camera test");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		imageLabel = new JLabel();
		frame.add(imageLabel);
		frame.setVisible(true);
	}
	
	private void runMainLoop(String[] args)
	{
		ImageProcessor imageProcessor = new ImageProcessor();
		Mat webcamMatImage = new Mat();
		Image tempImage;
		VideoCapture capture = new VideoCapture(0);
		capture.set(Videoio.CAP_PROP_FRAME_WIDTH, 700);
		capture.set(Videoio.CAP_PROP_FRAME_HEIGHT, 700);
		
		if(capture.isOpened())
		{
			while(true)
			{
				capture.read(webcamMatImage);
				if(!webcamMatImage.empty())
				{
					tempImage = imageProcessor.toBufferedImage(webcamMatImage);
					ImageIcon imageIcon = new ImageIcon(tempImage,"Captured Video");
					imageLabel.setIcon(imageIcon);
					frame.pack();
				}
				else
				{
					System.out.println("Frame not capturred");
					break;
				}
			}
		}
		else
		{
			System.out.println("couldnt open camera!");
		}
	}
}
