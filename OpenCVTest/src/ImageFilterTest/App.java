package ImageFilterTest;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class App {
	static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}
	public static void main(String[] args) throws Exception{
		String filePath = "IMG_9010.JPG";
		Mat newImage = Imgcodecs.imread(filePath);
		if(newImage.dataAddr()==0)
		{
			System.out.println("Khong the mo file "+filePath);
		}
		else
		{
			ImageViewer imgViewer = new ImageViewer();
			imgViewer.show(newImage,filePath);
		}
	}
}
