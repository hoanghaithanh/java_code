package ImageFilterTest;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class ImageViewer {
	private JLabel imageView;
	private JPanel buttonPanel;
	private JButton filterButton;
	private JButton segmentButton;
	private JSlider thresholdSlider;
	public void show(Mat image)
	{
		show(image,"");
	}
	
	
	public void show(Mat image, String windowName)
	{
		setSystemLookAndFeel();
		JFrame frame = createJFrame(windowName);
			Image loadedImage = toBufferedImage(image);
			imageView.setIcon(new ImageIcon(loadedImage));
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
			segmentButton.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					segment(image);
				}
				
			});
			filterButton.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					filter(image);
				}
			});
			imageView.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
		
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
	
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
	
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub

				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					// TODO Auto-generated method stub
					//filter(image);
					frame.repaint();
				}
			});		
	}
	
	private JFrame createJFrame(String windowName)
	{
		JFrame frame = new JFrame(windowName);
		imageView = new JLabel();
		buttonPanel = new JPanel();
		filterButton = new JButton("Filter");
		segmentButton = new JButton("Segment");
		thresholdSlider = new JSlider(0,255);
		thresholdSlider.setMajorTickSpacing(64);
		thresholdSlider.setMinorTickSpacing(16);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(filterButton);
		buttonPanel.add(segmentButton);
		buttonPanel.add(thresholdSlider);
		final JScrollPane imageScrollPane = new JScrollPane(imageView);
		imageScrollPane.setPreferredSize(new Dimension(640, 680));
		frame.add(imageScrollPane,BorderLayout.NORTH);
		frame.add(buttonPanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		return frame;
	}
	

	private void segment(Mat matrix)
	{
		Mat newMat = new Mat();
		Mat detectedEdge = new Mat();
		Imgproc.cvtColor(matrix, newMat, Imgproc.COLOR_BGR2GRAY);
		Imgproc.blur(newMat, detectedEdge, new Size(3,3));
		Imgproc.Canny(detectedEdge, detectedEdge, thresholdSlider.getValue(), thresholdSlider.getValue()*3, 3, true);
		Mat dest = new Mat();
		Core.add(dest, Scalar.all(0), dest);
		matrix.copyTo(dest, detectedEdge);
		Image loadedImage = toBufferedImage(dest);
		imageView.setIcon(new ImageIcon(loadedImage));
		imageView.repaint();
	}
	private void setSystemLookAndFeel()
	{
		try{
			UIManager.setLookAndFeel((UIManager.getSystemLookAndFeelClassName()));
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}catch(InstantiationException e)
		{
			e.printStackTrace();
		}catch(IllegalAccessException e)
		{
			e.printStackTrace();
		}catch(UnsupportedLookAndFeelException e)
		{
			e.printStackTrace();
		}
	}
	
	public void filter(Mat image)
	{
		Mat matrix = image;
		int totalByte = (int) (matrix.total()*matrix.elemSize());
		byte[] buffer = new byte[totalByte];
		matrix.get(0, 0, buffer);
		for(int i = 0;i<totalByte;i++)
		{
			if(i%3==0)
			{
				buffer[i]=0;
			}
		}
		matrix.put(0, 0, buffer);
		Image loadedImage = toBufferedImage(matrix);
		imageView.setIcon(new ImageIcon(loadedImage));
		imageView.repaint();
	}
	
	public Image toBufferedImage(Mat matrix){
		int type = BufferedImage.TYPE_BYTE_GRAY;
		if(matrix.channels()>1)
		{
			type = BufferedImage.TYPE_3BYTE_BGR;
			
		}
		Size size = new Size(640,(int)matrix.rows()*640/matrix.cols());
		Mat newMat = new Mat();
		Imgproc.resize(matrix, newMat, size);
		
		int bufferedSize = newMat.channels()*newMat.rows()*newMat.cols();
		byte[] buffer = new byte[bufferedSize];
		newMat.get(0, 0, buffer);
		BufferedImage image = new BufferedImage(newMat.cols(),newMat.rows(),type);
		final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);
		double[] pixel = matrix.get(1, 1);
		for(double db:pixel)
		{
			System.out.println(db);
		}
		return image;
	}
	
	
}
