
public interface Commons {

    public static final int BOARD_WIDTH = 800;
    public static final int BOARD_HEIGTH = 550;
    public static final int GROUND = 450;
    public static final int BOMB_HEIGHT = 20;
    public static final int ALIEN_HEIGHT = 32;
    public static final int ALIEN_WIDTH = 32;
    public static final int BORDER_RIGHT = 5;
    public static final int BORDER_LEFT = 5;
    public static final int GO_DOWN = 15;
    public static int NUMBER_OF_ALIENS_TO_DESTROY = 20;
    public static final int CHANCE = 5;
    public static final int DELAY = 7;
    public static final int PLAYER_WIDTH = 48;
    public static final int PLAYER_HEIGHT = 48;
}
