
public class Level {
	private int lvl;
	private int numRow;
	private int numCol;
	private int down;
	public int getLvl() {
		return lvl;
	}
	public void setLvl(int lvl) {
		this.lvl = lvl;
	}
	public int getNumRow() {
		return numRow;
	}
	public void setNumRow(int numRow) {
		this.numRow = numRow;
	}
	public int getNumCol() {
		return numCol;
	}
	public void setNumCol(int numCol) {
		this.numCol = numCol;
	}
	public int getDown() {
		return down;
	}
	public void setDown(int down) {
		this.down = down;
	}
	public Level(){
	
	}
	public Level(int lvl) {
		super();
		this.lvl = lvl;
		
	}
	public void stat(){
		if (this.lvl==1)
		{
			this.numRow=1;
			this.numCol=3;
			this.down=15;
		}
		if (this.lvl==2)
		{
			this.numRow=2;
			this.numCol=3;
			this.down=15;
		}
		if (this.lvl==3)
		{
			this.numRow=2;
			this.numCol=4;
			this.down=25;
		}
		if (this.lvl==4)
		{
			this.numRow=3;
			this.numCol=4;
			this.down=25;
		}
		if (this.lvl==5)
		{
			this.numRow=3;
			this.numCol=5;
			this.down=25;
		}
		if (this.lvl==6)
		{
			this.numRow=4;
			this.numCol=5;
			this.down=25;
		}
		if (this.lvl==7)
		{
			this.numRow=4;
			this.numCol=6;
			this.down=35;
		}
		if (this.lvl==8)
		{
			this.numRow=4;
			this.numCol=7;
			this.down=35;
		}
		if (this.lvl==9)
		{
			this.numRow=4;
			this.numCol=8;
			this.down=40;
		}
		if (this.lvl==10)
		{
			this.numRow=5;
			this.numCol=10;
			this.down=50;
		}
		
	}
}
