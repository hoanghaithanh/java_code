import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;


public class Player extends Sprite implements Commons{

    private final int START_Y = 480; 
    private final int START_X = 270;

    private final String player = "spacepix/player.png";
    private int width;
    private int height;

    public Player() {

        ImageIcon ii = new ImageIcon(player);

        width = ii.getImage().getWidth(null); 
        height = ii.getImage().getHeight(null);
        setImage(ii.getImage());
        setX(START_X);
        setY(START_Y);
    }

    public void act() {
        x += dx;
        if (x <= 2) 
            x = 2;
        if (x >= BOARD_WIDTH - width-2) 
            x = BOARD_WIDTH - width-2;
        y += dy;
        if (y <= 2) 
            y = 2;
        if (y >= GROUND - height) 
            y = GROUND - height;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_A)
        {
            dx = -2;
        }

        if (key == KeyEvent.VK_D)
        {
            dx = 2;
        }
        if (key == KeyEvent.VK_W)
        {
            dy = -2;
        }
        if (key == KeyEvent.VK_S)
        {
            dy = 2;
        }
    }

    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_A)
        {
            dx = 0;
        }

        if (key == KeyEvent.VK_D)
        {
            dx = 0;
        }
        if (key == KeyEvent.VK_W)
        {
            dy = 0;
        }
        if (key == KeyEvent.VK_S)
        {
            dy = 0;
        }
    }
}
