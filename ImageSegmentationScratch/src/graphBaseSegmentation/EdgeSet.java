package graphBaseSegmentation;

import java.util.ArrayList;
import java.util.HashSet;

public class EdgeSet {
		private HashSet<Edge> edgeSet;
		public HashSet<Edge> getEdgeSet()
		{
			return this.edgeSet;
		}
		public EdgeSet()
		{
			
		}
		public EdgeSet(VertexSet verSet)
		{
			edgeSet = new HashSet<Edge>();
			HashSet<Vertex> vertexSet = verSet.getVertexSet();
			for(Vertex v:vertexSet)
			{
				ArrayList<Vertex> neighbour = verSet.findNeighbour(v.getPos_Col(), v.getPos_Row());
				for(Vertex u: neighbour)
				{
					Edge newEdge = new Edge(u,v);
					this.edgeSet.add(newEdge);
				}
			}
		}
		
		public void printEdge()
		{
			for(Edge e:this.edgeSet)
			{
				e.getVertex1().printVertex();
				e.getVertex2().printVertex();
				System.out.println(e.getWeight());
				System.out.println("--------------");
			}
		}
}
