package graphBaseSegmentation;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;


public class Main {
	static {System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

	public static void main(String[] args)
	{
		String filePath = "../ImageSegmentation/src/graphBaseSegmentation/cathedral.jpg";
		Mat newImage = Imgcodecs.imread(filePath);
		if(newImage.dataAddr()==0)
		{
			System.out.println("Khong the mo file "+filePath);
		}
		else
		{
			Graph g = new Graph();
			g.initial(newImage);
		}
	}
}
