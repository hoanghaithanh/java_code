package graphBaseSegmentation;

public class Vertex {
	private int pos_Col;
	private int pos_Row;
	private double redValue;
	private double blueValue;
	private double greenValue;
	private double grayValue;
	
	//Initial a vertex with all the value equal zero.
	public Vertex()
	{
		this.redValue = this.blueValue = this.greenValue = this.grayValue = 0;
	}
	
	//Initial a vertex with position, all the value equal zero
	public Vertex(int pos_Col, int pos_Row)
	{
		this.pos_Col = pos_Col;
		this.pos_Row = pos_Row;
		this.redValue = this.blueValue = this.greenValue = this.grayValue = 0;
	}
	
	//Initial a vertex with its Grey value, others value equal Grey value
	public Vertex(int pos_Col, int pos_Row,double grayValue)
	{
		this.pos_Col = pos_Col;
		this.pos_Row = pos_Row;
		this.redValue = grayValue;
		this.blueValue = grayValue;
		this.greenValue = grayValue;
		this.grayValue = grayValue;
	}
	
	//Fully initial a vertex
	public Vertex(int pos_Col, int pos_Row, double redValue, double greenValue, double blueValue)
	{
		this.pos_Col = pos_Col;
		this.pos_Row = pos_Row;
		this.redValue = redValue;
		this.greenValue = greenValue;
		this.blueValue = blueValue;
		this.grayValue = ((double)1/3)*(redValue+greenValue+blueValue);
	}
	
	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		if(arg0 instanceof Vertex)
		{
			if((((Vertex) arg0).getPos_Col()==this.pos_Col)&&(((Vertex)arg0).getPos_Row()==this.pos_Row))
			{
				return true;
			}
		}
		return false;
	}

	public double getIntensity()
	{
		return this.grayValue;
	}
	
	public int getPos_Col() {
		return pos_Col;	
	}

	public void setPos_Col(int pos_Col) {
		
		this.pos_Col = pos_Col;
	}

	public int getPos_Row() {
		return pos_Row;
	}

	public void setPos_Row(int pos_Row) {
		
		this.pos_Row = pos_Row;
	}

	public double getRedValue() {
		return redValue;
	}

	public void setRedValue(double redValue) {
		this.redValue = redValue;
		this.grayValue = 1/3*(redValue+greenValue+blueValue);
	}

	public double getBlueValue() {
		return blueValue;
	}

	public void setBlueValue(double blueValue) {
		this.blueValue = blueValue;
		this.grayValue = 1/3*(redValue+greenValue+blueValue);
	}

	public double getGreenValue() {
		return greenValue;
	}

	public void setGreenValue(double greenValue) {
		this.greenValue = greenValue;
		this.grayValue = 1/3*(redValue+greenValue+blueValue);
	}

	public double getGrayValue() {
		return grayValue;
	}

	//When use this method, RGB value change to grayValue
	public void setGrayValue(double grayValue) {
		this.redValue = this.blueValue = this.greenValue = this.grayValue = grayValue;
	}
	
	public void printVertex()
	{
		System.out.format("Location: (%d,%d), Value: (%.2f,%.2f,%.2f,%.2f)\n", this.pos_Col, this.pos_Row, this.redValue, this.greenValue,this.blueValue, this.grayValue);
	}
	
}
