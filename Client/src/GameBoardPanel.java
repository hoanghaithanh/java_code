import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
/*
 * GameBoardPanel.java
 *
 * Created on 25 ����, 2008, 09:21 �
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Mohamed Talaat Saad
 */
public class GameBoardPanel extends JPanel {
    
    /** Creates a new instance of GameBoardPanel */
    private Tank tank;
    private int width=800;
    private int height=600;
    private static ArrayList<Tank> tanks;
    private static ArrayList<Enemy> enemies;
    private boolean gameStatus;
    public GameBoardPanel(Tank tank,Client client, boolean gameStatus) 
    {
        this.tank=tank;
        this.gameStatus=gameStatus;
        setSize(width,height);
        setBounds(0,0,width,height);
        addKeyListener(new InputManager(tank));
        setFocusable(true);
        
        tanks=new ArrayList<Tank>(100);
        enemies=new ArrayList<Enemy>(100);
        for(int i=0;i<100;i++)
        {
            tanks.add(null);
        }
        for(int i=0;i<100;i++)
        {
            enemies.add(null);
        }
        tanks.add(tank.getTankID(), tank);
    }
    public void paintComponent(Graphics gr) {
        super.paintComponent(gr);
        Graphics2D g=(Graphics2D)gr;
 
        g.setColor(Color.BLACK);
        g.fillRect(0,0, getWidth(),getHeight());
        
        g.setColor(Color.GREEN);
        g.fillRect(0,0, getWidth(),getHeight());
        Image img =  (new ImageIcon("Images/bg1.jpg")).getImage();
        //Image bufferedImg = img.getScaledInstance(800, 580, Image.SCALE_SMOOTH);
        g.drawImage(img,0,0,width,height,null);

        if(gameStatus) 
        {
            g.drawImage(tank.getBuffImage(),tank.getXposition(),tank.getYposition(),this);
            for(int j=0;j<1000;j++)
            {
                if(tank.getBomb()[j]!=null) 
                {
                    if(tank.getBomb()[j].stop==false){
                        g.drawImage(tank.getBomb()[j].getBomBufferdImg(),tank.getBomb()[j].getPosiX(),tank.getBomb()[j].getPosiY(),this);
                    }
                }
            }
            //ve enemy
            for(int i=0;i<enemies.size();i++) 
            {
                if(enemies.get(i)!=null)
                    g.drawImage(enemies.get(i).getBuffImage(),enemies.get(i).getXposition(),enemies.get(i).getYposition(),this);
                
            }
            
            
            for(int i=1;i<tanks.size();i++) 
            {
                if(tanks.get(i)!=null)
                    g.drawImage(tanks.get(i).getBuffImage(),tanks.get(i).getXposition(),tanks.get(i).getYposition(),this);
                
                for(int j=0;j<1000;j++)
                {
                    if(tanks.get(i)!=null)
                    {
                        if(tanks.get(i).getBomb()[j]!=null) 
                        {
                            if(tanks.get(i).getBomb()[j].stop==false){
                            g.drawImage(tanks.get(i).getBomb()[j].getBomBufferdImg(),tanks.get(i).getBomb()[j].getPosiX(),tanks.get(i).getBomb()[j].getPosiY(),this);
                            }
                        }
                    }
                }
            }

        }
        
        repaint();
    }
    public void setEnemy(ArrayList<EnemyInfo> enemyList)
    {
    	for(int i=0;i<enemyList.size();i++)
    	{
    		this.enemies.add(new Enemy(enemyList.get(i).posX,enemyList.get(i).posY,enemies.size()));
    	}
    }
    public void removeEnemy(int enemyID)
    {
    	enemies.set(enemyID, null);
    }
    public Enemy getEnemy(int id)
    {
    	return enemies.get(id);
    }
    public void registerNewTank(Tank newTank)
    {
        tanks.set(newTank.getTankID(),newTank);
    }
    
    public void registerNewEnemy(Enemy newEnemy)
    {
        enemies.set(newEnemy.getEnemyID(),newEnemy);
    }
    
    public void removeTank(int tankID)
    {
        tanks.set(tankID,null);
    }
    public Tank getTank(int id)
    {
        return tanks.get(id);
    }
    public void setGameStatus(boolean status)
    {
        gameStatus=status;
    }
  
    public static ArrayList<Tank> getClients()
    {
        return tanks;
    }
    
    public static ArrayList<Enemy> getEnemies()
    {
    	return enemies;
    }
}
