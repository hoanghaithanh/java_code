import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Enemy extends Thread {
	private Image enemyImg;
    private BufferedImage ImageBuff;
    private int enemyID;
    private int posiX=-1,posiY=-1;
    private int width=559,height=473;
    private boolean die;
    
    /** Creates a new instance of Tank */
    public Enemy(int x, int y) 
    {  
//        while(posiX<70|posiY<50|posiY>height-43|posiX>width-43)
//        {
//            posiX=(int)(Math.random()*width);
//            posiY=(int)(Math.random()*height);
//        }
    	posiX=x;
    	posiY=y;
        loadImage(4);
        
    }
    public Enemy(int x,int y,int id)
    {
        posiX=x;
        posiY=y;
        enemyID=id;
        loadImage(0);
    }
    
    public void loadImage(int a)
    {
        
            enemyImg=new ImageIcon("Images/enemy.png").getImage();
            

        
        ImageBuff=new BufferedImage(enemyImg.getWidth(null),enemyImg.getHeight(null),BufferedImage.TYPE_INT_RGB);
        ImageBuff.createGraphics().drawImage(enemyImg,0,0,null);
    }
    public BufferedImage getBuffImage()
    {
        return ImageBuff;
    }
    
    public int getXposition()
    {
        return posiX;
    }
    public int getYposition()
    {
        return posiY;
    }
    public void setXpoistion(int x)
    {
        posiX=x;
    }
    public void setYposition(int y)
    {
        posiY=y;
    }

    
    public void moveBackward()
    {
       
            int temp;
            temp=(int)(posiY+30);   
            if(checkCollision()==false)
            {
                posiY=temp;
            } 
        
    }
    
    public void setEnemyID(int id)
    {
        enemyID=id;
    }
    public int getEnemyID()
    {
        return enemyID;
    }

    
    //hit another tank
    public boolean checkCollision() 
    {
        ArrayList<Tank>clientTanks=GameBoardPanel.getClients();
        int x,y;
        if(posiY>505)
        {
        	Client.getGameClient().sendToServer(new Protocol().RemoveEnemyPacket(this.enemyID));
        	return true;
        }
        for(int i=0;i<clientTanks.size();i++) {
        	
            if(clientTanks.get(i)!=null) {
            	System.out.println(i);
                x=clientTanks.get(i).getXposition();
                y=clientTanks.get(i).getYposition();
                System.out.println(x+"-"+y);
                if((posiY>=y&&posiY<=y+43)&&(posiX>=x&&posiX<=x+43)) 
                {
                    System.out.println("hit");
                    
                    ClientGUI.gameStatusPanel.repaint();
                    
                    if(clientTanks.get(i)!=null)
                    Client.getGameClient().sendToServer(new Protocol().RemoveClientPacket(clientTanks.get(i).getTankID()));  
                    Client.getGameClient().sendToServer(new Protocol().RemoveEnemyPacket(this.enemyID));
                    return true;
                }
            }
        }
        return false;
    }
}
