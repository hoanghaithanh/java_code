import java.io.DataOutputStream;

public class EnemyInfo
    {

        int posX,posY,id;
        
        public EnemyInfo(int posX,int posY,int id)
        {
        	
           this.posX=posX;
           this.posY=posY;
           this.id=id;
        }
        
        public void setPosX(int x)
        {
            posX=x;
        }
        public void setPosY(int y)
        {
            posY=y;
        }

        public int getX()
        {
            return posX;
        }
        public int getY()
        {
            return posY;
        }
        
        public void moveBackward()
        {
        	this.posY += 30;
        }
        public int getID()
        {
            return id;
        }
    }