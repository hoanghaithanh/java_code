package graphBaseSegmentation;

import java.util.ArrayList;
import java.util.HashSet;

import org.opencv.core.Mat;

public class VertexSet {
	private HashSet<Vertex> vertexSet;
	
	public HashSet<Vertex> getVertexSet()
	{
		return this.vertexSet;
	}
	public VertexSet(Mat mat)
	{
		Vertex.setMatrix(mat);
		vertexSet = new HashSet<Vertex>();
		if(mat.channels()==3)
		{
			for(int x = 0;x<mat.cols();x++)
			{
				for(int y=0;y<mat.rows();y++)
				{
					Vertex vertex = new Vertex(x,y);
					vertex.printVertex();
					vertexSet.add(vertex);
				}
			}
		}
		
		if(mat.channels()==1)
		{
			for(int x = 0;x<mat.cols();x++)
			{
				for(int y=0;y<mat.rows();y++)
				{
					Vertex vertex = new Vertex(x,y);
					vertex.printVertex();
					vertexSet.add(vertex);
				}
			}
			
		}
	}
	
	public ArrayList<Vertex> findNeighbour(int x, int y)
	{
		ArrayList<Vertex> neighbour = new ArrayList<Vertex>();
		for(Vertex vertex:this.vertexSet)
		{
			if(Math.abs(x+y-vertex.getPos_Col()-vertex.getPos_Row())==1)
			{
				neighbour.add(vertex);
			}
		}
		return neighbour;
	}
		
}
