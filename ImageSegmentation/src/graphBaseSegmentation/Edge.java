package graphBaseSegmentation;

public class Edge {
	private Vertex vertex1;
	private Vertex vertex2;
	private double weight;
	
	public Edge(Vertex ver1, Vertex ver2)
	{
		this.vertex1 = ver1;
		this.vertex2 = ver2;
		this.weight = Math.abs(ver1.getIntensity()-ver2.getIntensity());
	}
	
	public double renewWeight()
	{
		this.weight = Math.abs(this.vertex1.getIntensity()-this.vertex2.getIntensity());
		return weight;
	}
	
	public Vertex getVertex1() {
		return vertex1;
	}

	public Vertex getVertex2() {
		return vertex2;
	}

	public double getWeight() {
		return weight;
	}

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		if(arg0 instanceof Edge)
		{
			if((((Edge) arg0).getVertex1().equals(this.vertex1)&&((Edge) arg0).getVertex2().equals(this.vertex2))||(((Edge) arg0).getVertex2().equals(this.vertex1)&&((Edge) arg0).getVertex1().equals(this.vertex2)))
			{
				return true;
			}
		}
		return false;
	}
}
