package graphBaseSegmentation;

import java.util.HashSet;

import org.opencv.core.Mat;

public class Graph {
	private VertexSet vertexSet;
	private EdgeSet edgeSet;
	
	
	public void initial(Mat matrix)
	{
		this.vertexSet = new VertexSet(matrix);
		this.edgeSet = new EdgeSet(vertexSet);
		this.edgeSet.printEdge();
		
	}
}
