package boundary;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controller.BookController;
import entity.Book;
import entity.BookClassification;
import entity.BookPublisher;

public class ListBook extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private BookController bookController = new BookController();
	private String title = "", author = "", classificationID = "", publisherName = "";
	private JComboBox<BookPublisher> cbPublisher;
	JButton btnAuthor;
	JScrollPane scrollPane;
	JComboBox<BookClassification> cbClassification;
	JButton btnTitle;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListBook frame = new ListBook();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public void init() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		cbPublisher = new JComboBox<BookPublisher>();
		cbPublisher.setBounds(141, 11, 93, 20);
		ArrayList<BookPublisher> listPublisher = bookController.getAllPublisher();
		cbPublisher.removeAllItems();
		cbPublisher.addItem(new BookPublisher());
		for (BookPublisher p : listPublisher) {
			cbPublisher.addItem(p);
		}
		contentPane.add(cbPublisher);

		cbClassification = new JComboBox<BookClassification>();
		cbClassification.setBounds(38, 11, 89, 20);
		ArrayList<BookClassification> listClassification = bookController.getAllClassification();
		cbClassification.removeAllItems();
		cbClassification.addItem(new BookClassification());
		for (BookClassification bc : listClassification) {
			cbClassification.addItem(bc);
		}
		contentPane.add(cbClassification);

		btnAuthor = new JButton("Ten tac gia");
		btnAuthor.setBounds(244, 41, 147, 23);
		contentPane.add(btnAuthor);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(38, 75, 374, 310);
		contentPane.add(scrollPane);

		table = new JTable();
		genTableContent();
		scrollPane.setViewportView(table);

		btnTitle = new JButton("Ten sach");
		btnTitle.setBounds(244, 10, 147, 23);
		contentPane.add(btnTitle);

	}

	public ListBook() {

		init();
		cbPublisher.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				publisherName = ((BookPublisher) cbPublisher.getSelectedItem()).getPublisherName();
				genTableContent();
			}
		});

		btnAuthor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				author = JOptionPane.showInputDialog("Nhap ten tac gia");
				if (author != null) {
					btnAuthor.setText("Tac gia: " + author);
					genTableContent();
				}
			}
		});

		cbClassification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				classificationID = ((BookClassification) cbClassification.getSelectedItem()).getClassificationID();
				System.out.println(classificationID);
				genTableContent();
			}
		});

		btnTitle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				title = JOptionPane.showInputDialog("Nhap ten sach");
				if (title != null) {
					btnTitle.setText("Ten sach: " + title);
					genTableContent();
				}
			}
		});

	}

	public void genTableContent() {
		try {
			ArrayList<Book> listBook = bookController.searchBook(classificationID, title, author, publisherName);
			Vector<String> row, colunm;
			int count = 1;
			DefaultTableModel model = new DefaultTableModel() {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
			String[] colunmNames = { "No.", "Ten Sach", "Tac gia", "Nha xuat ban" };
			colunm = new Vector<String>();
			int numberColumn;
			numberColumn = colunmNames.length;
			for (int i = 0; i < numberColumn; i++) {
				colunm.add(colunmNames[i]);
			}
			model.setColumnIdentifiers(colunm);
			for (Book book : listBook) {
				row = new Vector<String>();
				row.add(count + "");
				row.add(book.getTitle());
				row.add(book.getAuthor());
				row.add(book.getPublisher().getPublisherName());
				count++;
				model.addRow(row);
			}
			table.setModel(model);
			table.setVisible(true);
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(this, "Loi", "Co loi xay ra!", JOptionPane.ERROR_MESSAGE);
		}
	}
}
