package session;

import entity.Cart;

public class Session {
	Cart cart;

	private Session() {

	}

	public Cart getCart(int cardID) {
		if (cart == null)
			cart = new Cart(cardID);
		else {
			if (cart.getCardID() != cardID)
				return null;
		}
		return cart;
	}
}
