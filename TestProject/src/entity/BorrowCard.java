package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BorrowCard extends DataAccessHelper {
	private int cardID;
	private String expiredDate;
	private String activeCode;
	private int status;

	public int getCardID() {
		return cardID;
	}

	public String getExpiredDate() {
		return expiredDate;
	}

	public String getActiveCode() {
		return activeCode;
	}

	public int getStatus() {
		return status;
	}

	public Date getExpireDate(int cardID) throws SQLException, ParseException {
		String date;
		getConnection();
		String sql = "SELECT expired_date FROM borrower_card WHERE card_id = " + cardID;
		ResultSet rs = getStmt().executeQuery(sql);
		date = rs.getString("expired_date");
		closeConnection();
		Format formatter = new SimpleDateFormat("dd-MM-YYYY");
		return (Date) formatter.parseObject(date);
	}

	public Date getExpireDate() throws ParseException {
		Format formatter = new SimpleDateFormat("dd-MM-YYYY");
		return (Date) formatter.parseObject(expiredDate);
	}
	
	public int getNumberBorrowingBook() throws SQLException {
		int num = -1;
		getConnection();
		String sql = "SELECT count(bd.copy_id) AS num FROM borrowing_detail AS bd "
				+ "INNER JOIN borrowing AS b ON b.borrowing_id = bd.borrowing_id "
				+ "INNER JOIN book_copy AS bc ON bc.copy_id = bd.copy_id WHERE b.card_id = "
				+ cardID + " AND bc.status = " + BookCopy.STATUS_AVAILABLE;
		ResultSet rs = getStmt().executeQuery(sql);
		if (rs.next())
			num = rs.getInt("num");
		closeConnection();
		return num;
	}

	public int getNumberBorrowingBook(int cardID) throws SQLException {
		int num = -1;
		getConnection();
		String sql = "SELECT count(bd.copy_id) AS num FROM borrowing_detail AS bd "
				+ "INNER JOIN borrowing AS b ON b.borrowing_id = bd.borrowing_id "
				+ "INNER JOIN book_copy AS bc ON bc.copy_id = bd.copy_id WHERE b.card_id = "
				+ cardID + " AND bc.status = " + BookCopy.STATUS_AVAILABLE;
		ResultSet rs = getStmt().executeQuery(sql);
		if (rs.next())
			num = rs.getInt("num");
		closeConnection();
		return num;
	}
}
