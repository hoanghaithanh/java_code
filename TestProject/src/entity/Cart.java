package entity;

import java.util.ArrayList;

public class Cart {
	private int cardID;
	private ArrayList<Integer> listCopy;

	public Cart() {
		cardID = 0;
		listCopy = new ArrayList<Integer>();
	}

	public Cart(int cardID) {
		this.cardID = cardID;
		listCopy = new ArrayList<Integer>();
	}

	public int getCardID() {
		return cardID;
	}

	public ArrayList<Integer> getListCopy() {
		return listCopy;
	}

	public void setListCopy(ArrayList<Integer> listCopy) {
		this.listCopy = listCopy;
	}

}
