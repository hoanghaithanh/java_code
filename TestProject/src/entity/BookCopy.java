package entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookCopy extends DataAccessHelper {
	private int bookCopyID;
	private int price, status;

	public static final int STATUS_AVAILABLE = 0;
	public static final int STATUS_BORROWING = 1;
	public static final int STATUS_WAITING = 2;

	public BookCopy() {
		super();
	}

	public BookCopy(int bookCopyID, int price, int status) {
		super();
		this.bookCopyID = bookCopyID;
		this.price = price;
		this.status = status;
	}

	public int getBookCopyID() {
		return bookCopyID;
	}

	public int getPrice() {
		return price;
	}

	public int getStatus() {
		return status;
	}

	public int getAvailableCopy(String bookID) throws SQLException {
		int copyID = -1;
		String sql = "SELECT copy_id FROM book_copy WHERE book_id LIKE '" + bookID + "' AND status = "
				+ STATUS_AVAILABLE + " LIMIT 1)";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if (rs.next())
			copyID = rs.getInt("copy_id");
		closeConnection();
		return copyID;
	}

	public boolean editStatus(int copyID, int status) throws SQLException {
		if (status != STATUS_BORROWING && status != STATUS_AVAILABLE && status != STATUS_WAITING)
			return false;
		else {
			String sql = "UPDATE book_copy SET status = " + status + " WHERE copy_id = " + copyID + ")";
			return updateQuery(sql);
		}
	}

}
