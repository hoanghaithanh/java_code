package entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.spi.DirStateFactory.Result;

public class Borrowing extends DataAccessHelper {
	private int borrowingID;
	private int cardID;
	private String borrowedDate, expectedReturnDate, lentDate;
	private int status;

	public final int STATUS_WAITING = 0;
	public final int STATUS_BORROWING = 1;
	public final int STATUS_RETURN = 2;

	public Borrowing(){
		
	}
	
	public Borrowing(int borrowingID, int cardID, String borrowedDate, String expectedReturnDate, String lentDate,
			int status) {
		super();
		this.borrowingID = borrowingID;
		this.cardID = cardID;
		this.borrowedDate = borrowedDate;
		this.expectedReturnDate = expectedReturnDate;
		this.lentDate = lentDate;
		this.status = status;
	}

	public Borrowing(int cardID, String expectedReturnDate, int status) {
		super();
		this.cardID = cardID;
		this.expectedReturnDate = expectedReturnDate;
		this.status = status;
	}

	public int getBorrowingID() {
		return borrowingID;
	}

	public int getCardID() {
		return cardID;
	}

	public String getBorrowedDate() {
		return borrowedDate;
	}

	public String getExpectedReturnDate() {
		return expectedReturnDate;
	}

	public String getLentDate() {
		return lentDate;
	}

	public int getStatus() {
		return status;
	}

	public boolean createBorrowing(int cardID, String expectedReturnDate) throws SQLException {
		String sql = "INSERT INTO borrowing (card_id, expected_return_date, status) VALUES (" + cardID + ", '"
				+ expectedReturnDate + "', " + STATUS_WAITING + ")";
		return updateQuery(sql);
	}

	public boolean editStatus(int borrowingID, int status) throws SQLException {
		if (status != STATUS_BORROWING && status != STATUS_RETURN && status != STATUS_WAITING)
			return false;
		else {
			String sql = "UPDATE borrowing SET status = " + status + " WHERE borrowing_id = " + borrowingID + " )";
			return updateQuery(sql);
		}
	}

	public boolean checkOverDue(int cardID) throws SQLException {
		String sql = "SELECT count(borrowing_id) AS num FROM borrowing WHERE card_id = " + cardID
				+ " AND expected_return_date < (SELECT date('now')) AND status = "+STATUS_BORROWING;
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		int num = rs.getInt("num");
		closeConnection();
		if (num == 0)
			return true;
		else
			return false;
	}
}
