package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Book extends DataAccessHelper {
	private String bookID;
	private String title, author, isbn;
	private BookPublisher publisher;

	public Book() {

	}

	public Book(String bookID, String title, String author, String isbn, BookPublisher publisher) {
		super();
		this.bookID = bookID;
		this.title = title;
		this.author = author;
		this.isbn = isbn;
		this.publisher = publisher;
	}

	public String getBookID() {
		return bookID;
	}

	public String getTitle() {
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public String getIsbn() {
		return isbn;
	}

	public BookPublisher getPublisher() {
		return publisher;
	}

	public void setBookID(String bookID) {
		this.bookID = bookID;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public void setPublisher(BookPublisher publisher) {
		this.publisher = publisher;
	}

	public boolean update(String bookID, String title, String author, String isbn, int publisherID)
			throws SQLException {
		String sql = "UPDATE book SET title = " + title + ", author = " + author + ", isbn = " + isbn
				+ ", publisher_id = " + publisherID + " WHERE book_id LIKE " + bookID;
		return updateQuery(sql);
	}

	public boolean delete() throws SQLException {
		String sql = "DELETE FROM book WHERE book_id = " + bookID;
		return updateQuery(sql);
	}

	private String bookIdGenerator(int classificationID) throws SQLException {
		String bookID = "'" + classificationID + "%'";
		String sql = "SELECT book_id FROM book WHERE book_ID LIKE " + bookID + "ORDER BY DESC LIMIT 1";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		String id = "0001";
		if (rs != null) {
			String result = rs.getString("book_id");
			id = result.substring(1);
		}
		return "'" + classificationID + "" + (Integer.parseInt(id) + 1) + "'";
	}

	public boolean create(int classificationID, String title, String author, String isbn, int publisherID)
			throws SQLException {
		title = "'%" + title + "%'";
		author = "'%" + author + "%'";
		isbn = "'" + isbn + "'";
		String sql = "INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
				+ bookIdGenerator(classificationID) + ", " + title + ", " + author + ", " + isbn + ", " + publisherID
				+ ")";
		return updateQuery(sql);
	}

	private ArrayList<Book> fetchResult(ResultSet rs) throws SQLException {
		ArrayList<Book> arr = new ArrayList<Book>();
		if (rs != null) {
			try {
				while (rs.next()) {
					arr.add(new Book(rs.getString("book_id"), rs.getString("title"), rs.getString("author"),
							rs.getString("isbn"),
							new BookPublisher(rs.getInt("publisher_id"), rs.getString("publisher_name"))));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return arr;
	}

	public ArrayList<Book> getListBook() throws SQLException {
		String sql = "SELECT b.book_id, b.title, b.author, b.isbn,"
				+ " p.publisher_id, p.publisher_name FROM book AS b "
				+ "JOIN book_publisher AS p ON b.publisher_id = p.publisher_id ";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		ArrayList<Book> arr = fetchResult(rs);
		closeConnection();
		return arr;
	}

	public ArrayList<Book> getListBook(String title, String author, String classificationID, String publisherName)
			throws SQLException {
		title = "'%" + title + "%'";
		author = "'%" + author + "%'";
		String bookID = "'" + classificationID + "%'";
		publisherName = "'%" + publisherName + "%'";
		String sql = "SELECT b.book_id, b.title, b.author, b.isbn, "
				+ "p.publisher_id, p.publisher_name FROM book AS b "
				+ "JOIN book_publisher AS p ON b.publisher_id = p.publisher_id " + "WHERE b.title LIKE " + title
				+ " AND b.author LIKE " + author + " AND b.book_id LIKE " + bookID + " AND p.publisher_name LIKE "
				+ publisherName;
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		ArrayList<Book> arr = fetchResult(rs);
		closeConnection();
		return arr;
	}

}
