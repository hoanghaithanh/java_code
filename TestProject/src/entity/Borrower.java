package entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Borrower extends DataAccessHelper {
	private int borrowerID;
	private String username, password, facebookID, fullName, email;
	private int gender;
	private String contact, studentID, studentCourse;
	private int status;

	public Borrower(int borrowerID, String username, String password, String facebookID, String fullName, String email,
			int gender, String contact, String studentID, String studentCourse, int status) {
		super();
		this.borrowerID = borrowerID;
		this.username = username;
		this.password = password;
		this.facebookID = facebookID;
		this.fullName = fullName;
		this.email = email;
		this.gender = gender;
		this.contact = contact;
		this.studentID = studentID;
		this.studentCourse = studentCourse;
		this.status = status;
	}

	public Borrower() {
		super();
	}

	public int getBorrowerID() {
		return borrowerID;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getFacebookID() {
		return facebookID;
	}

	public String getFullName() {
		return fullName;
	}

	public String getEmail() {
		return email;
	}

	public int getGender() {
		return gender;
	}

	public String getContact() {
		return contact;
	}

	public String getStudentID() {
		return studentID;
	}

	public String getStudentCourse() {
		return studentCourse;
	}

	public int getStatus() {
		return status;
	}

	public boolean checkLogin(String username, String password) throws SQLException {
		boolean check = false;
		String sql = "SELECT * FROM borrower WHERE username = '" + username + "' AND password = '" + password+"'";
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if (rs != null)
			return check = true;
		closeConnection();
		return check;
	}
}
