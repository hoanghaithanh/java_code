package entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Manager extends DataAccessHelper {
	private String username, password;
	private int role;

	public Manager(String username, String password, int role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public int getRole() {
		return role;
	}

	public int checkLogin(String username, String password) throws SQLException {
		int role = -1;
		String sql = "SELECT * FROM manager WHERE username = " + username + " AND password = " + password;
		getConnection();
		ResultSet rs = getStmt().executeQuery(sql);
		if (rs != null)
			role = rs.getInt("role");
		closeConnection();
		return role;
	}
}
