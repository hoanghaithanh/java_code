package entity;

public class BorrowingDetail extends DataAccessHelper {
	private int borrowingID;
	private BookCopy copy;
	private String returnDate;;

	public int getBorrowingID() {
		return borrowingID;
	}

	public BookCopy getCopy() {
		return copy;
	}

	public String getReturnDate() {
		return returnDate;
	}
}
