package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookClassification extends DataAccessHelper {
	private String classificationID;
	private String classificationName;

	public BookClassification() {
		classificationID = "";
		classificationName = "";
	}

	public BookClassification(String classificationID, String classificationName) {
		super();
		this.classificationID = classificationID;
		this.classificationName = classificationName;
	}

	public String getClassificationID() {
		return classificationID;
	}

	public String getClassificationName() {
		return classificationName;
	}

	public ArrayList<BookClassification> getListBookClassification() throws SQLException {
		ArrayList<BookClassification> list = new ArrayList<BookClassification>();
		getConnection();
		String sql = "SELECT classification_id, classification_name FROM book_classification";
		ResultSet rs = getStmt().executeQuery(sql);
		while (rs.next()) {
			list.add(new BookClassification(rs.getString("classification_id"), rs.getString("classification_name")));
		}
		return list;
	}

	public String toString() {
		if (classificationName.equals(""))
			return "Tat ca the loai";
		return classificationName;
	}
}
