package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookPublisher extends DataAccessHelper {
	private int publisherID;
	private String publisherName;

	public BookPublisher() {
		publisherName = "";
	}

	public BookPublisher(int publisherID, String publisherName) {
		super();
		this.publisherID = publisherID;
		this.publisherName = publisherName;
	}

	public int getPublisherID() {
		return publisherID;
	}

	public String getPublisherName() {
		return publisherName;
	}

	public void setPublisherID(int publisherID) {
		this.publisherID = publisherID;
	}

	public void setPublisherName(String publisherName) {
		this.publisherName = publisherName;
	}

	public ArrayList<BookPublisher> getListBookPublisher() throws SQLException {
		ArrayList<BookPublisher> list = new ArrayList<BookPublisher>();
		getConnection();
		String sql = "SELECT publisher_id, publisher_name FROM book_publisher";
		ResultSet rs = getStmt().executeQuery(sql);
		while (rs.next()) {
			list.add(new BookPublisher(rs.getInt("publisher_id"), rs.getString("publisher_name")));
		}
		return list;
	}

	public String toString() {
		if (publisherName.equals(""))
			return "Tat ca nha xuat ban";
		return publisherName;
	}
}
