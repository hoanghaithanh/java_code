import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import entity.Book;
import entity.BookCopy;
import entity.BorrowCard;
import entity.Borrowing;
import entity.BorrowingDetail;
import entity.DataAccessHelper;

public class Database extends DataAccessHelper {
	
	private static void createDatabase() throws ClassNotFoundException {
		try {
			//connection = DriverManager.getConnection("jdbc:sqlite:db/library.db");
			getConnection();
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.

			statement.executeUpdate("CREATE TABLE manager (manager_id INTEGER(11) PRIMARY KEY AUTOINCREMENT,"
					+ " username VARCHAR(20), password VARCHAR(16), role INTEGER(1)");
			statement.executeUpdate("INSERT INTO manager (username, password, role) VALUES ('admin', 'admin', 1)");
			statement.executeUpdate(
					"INSERT INTO manager (username, password, role) VALUES ('librarian', 'librarian', 0)");

			statement.executeUpdate("CREATE TABLE borrower (borrower_id INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ "username STRING, password STRING, facebook_id STRING, full_name STRING, "
					+ "email STRING, gender INTEGER, contact STRING, student_id STRING, "
					+ "study_course STRING, status INTEGER)");
			statement.executeUpdate(
					"INSERT INTO borrower (username, password, facebook_id, full_name, email, gender, contact, student_id, "
							+ "study_course, status) VALUES ('kien', 'kien', '', 'NTK', 'kien@gmail.com', 1, '0123456789', '', '', 1)");
			
			statement.executeUpdate("CREATE TABLE borrower_card (card_id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " borrower_id INTEGER, activate_code STRING, " + "expired_date STRING, status INTEGER)");
			statement.executeUpdate(
					"INSERT INTO borrower_card (borrower_id, activate_code, expired_date, status) VALUES (1, '12345', '08-12-2016', 1)");

			statement.executeUpdate("CREATE TABLE borrowing (borrowing_id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " card_id INTEGER, borrowed_date DATE, " + "expected_return_date DATE, lent_date DATE, "
					+ "status INTEGER)");
			statement.executeUpdate("INSERT INTO borrowing (card_id, borrowed_date, expected_return_date, lent_date, status) "
					+ "VALUES (1, '2016-12-12', '2016-09-17', '2016-12-13', 1)");

			statement.executeUpdate("CREATE TABLE borrowing_detail (borrowing_id INTEGER, "
					+ " copy_id INTEGER, return_date DATE NULL, PRIMARY KEY(borrowing_id, copy_id))");
			statement.executeUpdate("INSERT INTO borrowing_detail (borrowing_id, copy_id) VALUES (1, 1)");

			statement.executeUpdate("CREATE TABLE book_classification (classification_id STRING PRIMARY KEY,"
					+ " classification_name STRING)");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('TH', 'Toan hoc')");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('VL', 'Vat ly')");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('HH', 'Hoa hoc')");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('VH', 'Van hoc')");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('LS', 'Lich Su')");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('TA', 'Tieng Anh')");
			statement.executeUpdate(
					"INSERT INTO book_classification (classification_id, classification_name) VALUES ('SH', 'Sinh hoc')");

			statement.executeUpdate("CREATE TABLE book_publisher (publisher_id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " publisher_name STRING)");
			statement.executeUpdate("INSERT INTO book_publisher (publisher_name) VALUES ('NXB 1')");
			statement.executeUpdate("INSERT INTO book_publisher (publisher_name) VALUES ('NXB 2')");
			statement.executeUpdate("INSERT INTO book_publisher (publisher_name) VALUES ('NXB 3')");
			statement.executeUpdate("INSERT INTO book_publisher (publisher_name) VALUES ('NXB 4')");
			statement.executeUpdate("INSERT INTO book_publisher (publisher_name) VALUES ('NXB 5')");

			statement.executeUpdate("CREATE TABLE book (book_id STRING PRIMARY KEY," + " title STRING, author STRING, "
					+ "isbn STRING, " + "publisher_id INTEGER)");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'TH0001'" + ", " + "'Sach toan 1'" + ", " + "'NTK'" + ", " + "'ISDVN2016'" + ", " + 1 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'TH0002'" + ", " + "'Sach toan 2'" + ", " + "'NTK'" + ", " + "'ISDVN2016'" + ", " + 2 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'VL0001'" + ", " + "'Sach ly 1'" + ", " + "'TTT'" + ", " + "'ISDVN2016'" + ", " + 3 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'VH0001'" + ", " + "'Sach van 1'" + ", " + "'NMC'" + ", " + "'ISDVN2016'" + ", " + 2 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'LS0001'" + ", " + "'Sach lich su 1'" + ", " + "'VHS'" + ", " + "'ISDVN2016'" + ", " + 3 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'HH0001'" + ", " + "'Sach hoa 1'" + ", " + "'DTK'" + ", " + "'ISDVN2016'" + ", " + 4 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'TA0001'" + ", " + "'Sach anh 1'" + ", " + "'DTK'" + ", " + "'ISDVN2016'" + ", " + 5 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'SH0001'" + ", " + "'Sach sinh 1'" + ", " + "'HHT'" + ", " + "'ISDVN2016'" + ", " + 1 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'VH0002'" + ", " + "'Sach van 2'" + ", " + "'NTK'" + ", " + "'ISDVN2016'" + ", " + 2 + ")");
			statement.executeUpdate("INSERT INTO book (book_id, title, author, isbn, publisher_id) VALUES ("
					+ "'VH0003'" + ", " + "'Sach van 3'" + ", " + "'NTTT'" + ", " + "'ISDVN2016'" + ", " + 4 + ")");

			statement.executeUpdate("CREATE TABLE book_copy (copy_id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ " price INTEGER, book_id STRING, status INTEGER)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '010001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '010001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '010002', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '020001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '030001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '040001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '050001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '060001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '070001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '040002', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '040003', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '010001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '020001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '030001', 0)");
			statement.executeUpdate("INSERT INTO book_copy (price, book_id, status) VALUES (100000, '050001', 0)");

			// ResultSet rs = statement.executeQuery("SELECT * FROM
			// book_classification");
			// while(rs.next()){
			// System.out.println(rs.getString("classification_id"));
			// }
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		} finally {
			System.out.println("Da xong!");
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				System.err.println(e);
			}
		}
	}

	private static void testDatabase() throws SQLException {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:db/library.db");
			Statement statement = connection.createStatement();
			statement.setQueryTimeout(30); // set timeout to 30 sec.
			String sql = "SELECT b.book_id, b.title, b.author, b.isbn,"
					+ " p.publisher_id, p.publisher_name FROM book AS b "
					+ "JOIN book_publisher AS p ON b.publisher_id = p.publisher_id ";
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				System.out.println(rs.getString("publisher_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		} finally {
			System.out.println("Da xong!");
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				System.err.println(e);
			}
		}
	}

	private static void testSearchBook() throws SQLException {
		BorrowCard card = new BorrowCard();
		System.out.println(card.getNumberBorrowingBook(1));
	}

	private static void testCheckOverDue() throws SQLException {
			Borrowing detail = new Borrowing();
			System.out.println(detail.checkOverDue(1));
	}

	public static void main(String[] args) throws Exception {
		try {
			createDatabase();
			// testDatabase();
			// testSearchBook();
			//testCheckOverDue();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
