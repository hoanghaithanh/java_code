package controller;

import java.sql.SQLException;
import java.util.ArrayList;

import entity.Book;
import entity.BookClassification;
import entity.BookPublisher;

public class BookController {
	// private ArrayList<Book> listBook;

	// public ArrayList<Book> getAllBook() throws SQLException {
	// Book book = new Book();
	// return book.getListBook();
	// }

	public ArrayList<Book> searchBook(String classificationID, String title, String author, String publisherName)
			throws SQLException {
		Book book = new Book();
		return book.getListBook(title, author, classificationID, publisherName);
	}

	public ArrayList<BookClassification> getAllClassification() {
		BookClassification bc = new BookClassification();
		try {
			return bc.getListBookClassification();
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<BookClassification>();
		}
	}
	
	public ArrayList<BookPublisher> getAllPublisher() {
		BookPublisher p = new BookPublisher();
		try {
			return p.getListBookPublisher();
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<BookPublisher>();
		}
	}
}
