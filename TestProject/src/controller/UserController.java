package controller;

import java.sql.SQLException;

import entity.Borrower;

public class UserController {
	private Borrower borrower = new Borrower();

	public boolean login(String username, String password) {
		try {
			return borrower.checkLogin(username, password);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
}
