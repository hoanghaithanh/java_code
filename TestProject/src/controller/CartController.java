package controller;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;

import entity.BookCopy;
import entity.BorrowCard;
import entity.Cart;

public class CartController {
	
	private Cart cart;
	
	public boolean checkExpireCard(BorrowCard card) throws ParseException {
		Date expireDate = card.getExpireDate();
		Date today = new Date();
		if (expireDate.after(today))
			return true;
		else
			return false;
	}

	public boolean addToCart(String bookID) throws SQLException {
		BookCopy copy = new BookCopy();
		int copyID = copy.getAvailableCopy(bookID);
		if (copyID != -1) {
			
			return true;
		} else
			return false;
	}
	
	private boolean checkNumberOfBook(BorrowCard card) throws SQLException{
		int num = card.getNumberBorrowingBook();
		if(num>=5)
			return false;
		else
			return true;
	}
	
	public boolean createBorrowing(){
//		if(checkNumberOfBook())
		return true;
	}
}
