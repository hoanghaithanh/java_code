/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package user.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import entity.User;
/**
 * Class này điều khiển các chức năng liên quan tới user
 * @author CoTrang-Lecture
 */
public class UserController {
    User user = new User();
    
    private static UserController me;

    /**
     *Hàm khởi tạo là private để không đối tượng nào bên ngoài có thể khởi tạo tuỳ ý đối tượng của lớp này
     */ 
    private UserController(){}
	
    /**
     *Hàm này khởi tạo cho đối tượng dùng chung duy nhất của UserController nếu đối tượng đố null
     * @return đối tượng dùng chung duy nhất của UserController
     */ 
    public static UserController getInstance(){
            if (me == null)
                    me = new UserController();
            return me;
    }
    /**
     *Hàm này để kiểm tra người dùng có thể đăng nhập vào hệ thống không
     * @param email là email người dùng
     * @param password  là mật khẩu người dùng
     *  *@return int return là : 0. tài khoản bị khóa, 1 yêu cầu thay đổi pass
     * 2 sai mật khẩu, 3 là đúng, 4 là email không tồn tại.
     */
    public int  checkLogin(String email,String password) throws SQLException, ClassNotFoundException{
       return  user.checkLogin(email, password);
    }
    
    /**
     *Hàm này để đăng ký người dùng
     * @param User tất cả thông tin của người dùng
     * @return đúng sai, có đăng kí được hay k
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */    
    public boolean register(User user) throws ClassNotFoundException, SQLException{
            return user.addUser();
    }
    
    /**
     *Hàm này để tìm kiếm những người dùng phù hợp với thông tin đầu vào
     * @param lastName là tên người dùng
     * @param firstNamem là họ người dùng
     * @param email là email người dùng
     * @param roleId là id của role trong bảng Role
     * @return  ArrayList<User> là list các user
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */    
    public ArrayList<User> searchUser(String lastName,String firstName, String email,int roleId) throws SQLException, ClassNotFoundException{
        User user = new User();
        return  user.searchUser(lastName, firstName, email, roleId);
    }
    
    /**
     *Hàm này để thay đổi mật khẩu
     * @param email
     * @param newPassword mật khẩu mới
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */     
    public void  changePasswordByRequest(String email,String newPassword) throws SQLException, ClassNotFoundException{
        User user = new User();
        user.updatePassword(email, newPassword);
    }
    /**
     *Hàm này để tìm user bằng email
     * @param email
     * @return User là user có email tương ứng
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    public User findUserByEmail(String email) throws SQLException, ClassNotFoundException{
        User user = new User();
        if(user.findByEmail(email))
            return user;
        return null;
    }

    /**
     *Hàm này để cập nhật thông tin người dùng
     * @param user tất cả thông tin của người dùng
     * @param oldEmail email ban đầu của user (Không đổi email thì vẫn nhập email hiện tại vào)
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */      
    public void updateProfile(User user,String oldEmail) throws SQLException, ClassNotFoundException{
        user.updateProfile(oldEmail);
    }
}
