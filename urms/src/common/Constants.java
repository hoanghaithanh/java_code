package common;

public class Constants {
	public static final int LOGIN_BLOCKED = 0;
	public static final int LOGIN_CHANGE_PASSWORD = 1;
	public static final int LOGIN_SUCCESS = 2;
	public static final int LOGIN_WRONG_PASSWORD = 3;
	public static final int LOGIN_WRONG_EMAIL = 4;
	
}
