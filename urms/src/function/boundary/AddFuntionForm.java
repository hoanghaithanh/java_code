/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package function.boundary;

import java.sql.SQLException;

import javax.swing.JOptionPane;

import function.controller.FunctionController;
import entity.Function;
import entity.FunctionType;

/**
 * Class này là form add function của chức năng quản lý chức năng
 * @author CoTrang-Lecture
 */
public class AddFuntionForm extends javax.swing.JFrame {
    ViewFunctionsForm1 viewFunctionsForm;
    /**
     * Creates new form AddFuntionForm
     */
    public AddFuntionForm(ViewFunctionsForm1 viewFunctionForm) {
        try {
            this.viewFunctionsForm=viewFunctionForm;
            initComponents();
            FunctionType functionType = new FunctionType();
            String[] typeNames = functionType.getAllFunctionTypeName();
            cbbGroup.removeAllItems();
            cbbGroup.addItem("");
            for(int i = 0; i<typeNames.length;i++){
                cbbGroup.addItem(typeNames[i]);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtFunctionName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtBoundaryClass = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cbbGroup = new javax.swing.JComboBox<>();
        btnAdd = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Function Name:");

        jLabel2.setText("Boundary Class:");

        jLabel3.setText("Group:");

        cbbGroup.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtFunctionName, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(txtBoundaryClass, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbbGroup, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAdd)
                .addGap(18, 18, 18)
                .addComponent(btnCancel)
                .addGap(36, 36, 36))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtFunctionName))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtBoundaryClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbbGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCancel)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        add();
    }//GEN-LAST:event_btnAddActionPerformed
     /**
     *Hàm này thực hiện chức năng tạo bảng thêm chức năng khi ấn vào nút add
     */
    public void add(){
        boolean check=true;
        if(cbbGroup.getSelectedIndex()==0){
            JOptionPane.showMessageDialog(rootPane, "You must chose group before add function");
            check=false;
        }
        if(check&&txtBoundaryClass.getText().isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "You must fill boundary class before add function");
            check=false;
        }
        
        if(check&&txtFunctionName.getText().isEmpty()){
            JOptionPane.showMessageDialog(rootPane, "You must fill function name before add function");
            check=false;
        }
        if(check){
            Function function = new Function();
            function.setBoundaryClass(txtBoundaryClass.getText());
            function.setFunctionName(txtFunctionName.getText());
            function.setFunctionTypeId(cbbGroup.getSelectedIndex());
            function.setDescription(txtFunctionName.getText());
            try {
                FunctionController.getInstance().addFunction(function);
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
            } catch (ClassNotFoundException ex) {
                JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
            }
            viewFunctionsForm.genTableContent();
            this.dispose();
        }
    }
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnCancel;
    private javax.swing.JComboBox<String> cbbGroup;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField txtBoundaryClass;
    private javax.swing.JTextField txtFunctionName;
    // End of variables declaration//GEN-END:variables
}
