/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package function.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import entity.Function;
import entity.Role;
import entity.RoleFunction;

/**
 * Class này điều khiển các chức năng liên quan đến Function
 * @author CoTrang-Lecture
 */
public class FunctionController {
    private static FunctionController me;

    /**
     *Hàm khởi tạo là private để không đối tượng nào bên ngoài có thể khởi tạo tuỳ ý đối tượng của lớp này
     */ 
    private FunctionController(){}
	
    /**
     *Hàm này khởi tạo cho đối tượng dùng chung duy nhất của FunctionController nếu đối tượng đố null
     * @return đối tượng dùng chung duy nhất của FunctionController
     */ 
    public static FunctionController getInstance(){
            if (me == null)
                    me = new FunctionController();
            return me;
    }
    /**
     *Hàm này để lấy ra list function theo roleId và query
     * @param roleId là id của vai trò
     * @param query là câu query select
     * @return ArrayList<Function> là list các function
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */   
    public ArrayList<Function> getFunctionsByRoleId(int roleId, String query) throws ClassNotFoundException, SQLException{
        return new Function().getFunctionByRoleId(roleId,query);
    }
    /**
     *Hàm này để thêm 1 bản ghi vào bảng RoleFunction
     * @param functionId  là id của function
     * @param roleId là id của vai trò
     * true nếu không có lỗi
     * false nếu có lỗi trong cơ sở dữ liệu
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */   
    public void addRoleFunction(int functionId, int roleId) throws SQLException, ClassNotFoundException{
        RoleFunction roleFunction = new RoleFunction();
        roleFunction.setFunctionId(functionId);
        roleFunction.setRoleId(roleId);
        roleFunction.add();
    }
     /**Hàm này để xóa các bản ghi có functionId và roleId tương ứng trong bảng RoleFunction
     * @param functionId  là id của function
     * @param roleId là id của vai trò
     * true nếu không có lỗi
     * false nếu có lỗi trong cơ sở dữ liệu
     */    
    public void deleteRoleFunctionByRoleIdAndFunctionId(int functionId, int roleId) throws SQLException, ClassNotFoundException{
        new RoleFunction().delete(functionId, roleId);
    }
     /**Hàm này để xóa các bản ghi có functionName tương ứng trong bảng RoleFunction
     * @param functionName  là tên của function
     * true nếu không có lỗi
     * false nếu có lỗi trong cơ sở dữ liệu
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */        
    public void deleteFunctionByFunctionName(String functionName) throws SQLException, ClassNotFoundException{
        new Function().deleteFunctionByFunctionName(functionName);
    }
     /**
     *lấy Map function theo email
     * @param email, newPassword email
     * @return Map<String,ArrayList<Function>> key là typeFunctionName, value là  list function
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */   
    public Map<String,ArrayList<Function>> getFunctionsWithType(String email) throws SQLException, ClassNotFoundException{
        Role role = new Role();
        Function function = new Function();
        String roles =role.getRoleIdsByUser(email);
        return function.getFunctionsWithTypeByRoles(roles);        
    } 
     /**
     * Hàm này để lấy list function theo id của FunctionType
     * @param functionType là id của functionTypeId trong bảng function
     * @return ArrayList<Function> là  list function
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */       
    public ArrayList<Function> getFunctions(int functionType) throws SQLException, ClassNotFoundException{
        Function function = new Function();
        ArrayList<Function> functions = function.getFunctionByTypeId(functionType);
        return functions;
    }
     /**
     * Hàm này để thêm chức năng, chức năng đó được thêm vào bảng function
     * @param func là 1 chức năng cần được thêm vào
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */  
    public void addFunction(Function func) throws SQLException, ClassNotFoundException{
        func.add();
    }
     /**
     * Hàm này để cập nhật chức năng, chức năng đó được cập nhật trong bảng function
     * @param  functionName là tên lớp biên mà lúc trước của lớp biên của func
     * @param  roleId là id trường vai trò trong FunctionRole
     * @return boolean cho biết có sửa được hay không
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */  
    public void updateFunction(Function func,String functionName,int roleId) throws SQLException, ClassNotFoundException{
        func.modify(functionName,roleId);
    }
}
