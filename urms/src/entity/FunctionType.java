/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class này quản lý FunctionType trong cơ sở dữ liệu
 * @author CoTrang-Lecture
 */
public class FunctionType extends DataAccessHelper{
    private int functionTypeId;
    private String functionTypeName;
    private String description;
    private final String GET_BY_ID = "select *from FunctionType where functionTypeId = ?";
    private final String GET_ALL_TYPE_NAME = "select functionTypeName from FunctionType";
    /**
    *Hàm này lấy tất cả các functionTypeName trong bảng FunctionType
    *@return String là chuỗi các functionTypeName cách nhau bởi dấu phẩy
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */  
    public String[] getAllFunctionTypeName() throws SQLException, ClassNotFoundException{
        String functionTypeNames = new String();
        connectDB();
        PreparedStatement ps = conn.prepareStatement(GET_ALL_TYPE_NAME);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            functionTypeNames+= rs.getString("functionTypeName")+",";
        }
        functionTypeNames = functionTypeNames.substring(0, functionTypeNames.length()-1);
        closeDB();
        String result[] = functionTypeNames.split(",");//Utils.split(functionTypeNames,",");
        return result;
    }
    /**
    *Hàm này tìm functionTypeName theo id
    *@param functionTypeId là id của 1 bản ghi trong bảng FunctionType 
    *@return String functionTypeName tên của loại chức năng
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */  
    public String getFunctionTypeName(int functionTypeId) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(GET_BY_ID);
        ps.setInt(1, functionTypeId);
        ResultSet rs = ps.executeQuery();
        if(rs!= null && rs.next()){
            return rs.getString("fucntionTypeName");
        }
        closeDB();
        return null;
    }
    /**
    * Hàm này để lấy ra functionTypeId trong bảng FunctionType
    * @return int là functionTypeId
    */ 
    public int getFunctionTypeId() {
        return functionTypeId;
    }
    /**
    * Hàm này để gán giá trị cho functionTypeid trong bảng FunctionType
    * @param functionTypeid là functionTypeid
    */ 
    public void setFunctionTypeId(int functionTypeid) {
        this.functionTypeId = functionTypeid;
    }
    /**
    * Hàm này để lấy ra functionTypeName trong bảng FunctionType
    * @return String là functionTypeName
    */ 
    public String getFunctionTypeName() {
        return functionTypeName;
    }
    /**
    * Hàm này để gán giá trị cho functionTypeName trong bảng FunctionType
    * @param functionTypeName là functionTypeName
    */ 
    public void setFunctionTypeName(String functionTypeName) {
        this.functionTypeName = functionTypeName;
    }
    /**
    * Hàm này để lấy ra description trong bảng FunctionType
    * @return String là description
    */ 
    public String getDescription() {
        return description;
    }
    /**
    * Hàm này để gán giá trị cho description trong bảng FunctionType
    * @param description là description
    */ 
    public void setDescription(String description) {
        this.description = description;
    }
    
}
