/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class này quản lý Role trong cơ sở dữ liệu
 * @author CoTrang-Lecture
 */
public class Role extends DataAccessHelper{
    private int roleId;
    private String roleName;
    private String description;
    private static final String GET_BY_EMAIL = "select Role.* from User, UserRole, Role where User.email = UserRole.email and Role.roleId = UserRole.roleId and User.email = ?";
    private static final String GET_ALL = "select roleName from Role";
    /**
    * Hàm này để lấy ra roleId trong bảng Role
    * @return int là roleId
    */ 
    public int getRoleId() {
        return roleId;
    }
    /**
    * Hàm này để gán giá trị cho roleId trong bảng Role
    * @param roleId là roleId
    */ 
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    /**
    * Hàm này để lấy ra roleName trong bảng Role
    * @return String là roleName
    */ 
    public String getRoleName() {
        return roleName;
    }
    /**
    * Hàm này để gán giá trị cho roleName trong bảng Role
    * @param roleName là roleName
    */ 
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    /**
    * Hàm này để lấy ra description trong bảng Role
    * @return String là description
    */ 
    public String getDescription() {
        return description;
    }
    /**
    * Hàm này để gán giá trị cho description trong bảng Role
    * @param description là description
    */ 
    public void setDescription(String description) {
        this.description = description;
    }
    /**
    *Hàm này lấy tất cả role name trong csdl
    *@return String là mảng roleName cách nhau bởi dấu phẩy
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */ 
    public String[] getAllRoleName() throws SQLException, ClassNotFoundException{
        String roleNames = new String();
        connectDB();
        PreparedStatement ps = conn.prepareStatement(GET_ALL);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            roleNames+= rs.getString("roleName")+",";
        }
        roleNames = roleNames.substring(0, roleNames.length()-1);
        closeDB();
        String result[] = roleNames.split(",");//Utils.split(roleNames,",");
        return result;
    }
    
    /**
    *Hàm này tìm ra các vai trò của người dùng theo email
    *@param email email của người cần tìm vài trò
    *@return  String là mảng roleName cách nhau bởi dấu phẩy
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */ 
    public String getRoleIdsByUser(String email) throws SQLException, ClassNotFoundException{
        String roles = new String();
        connectDB();
        PreparedStatement ps = conn.prepareStatement(GET_BY_EMAIL);
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
                roles+= rs.getInt("roleId")+",";
        }
        roles = roles.substring(0, roles.length()-1);
        closeDB();
        return roles;
    }
    
}
