/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Class này quản lý Function trong cơ sở dữ liệu
 * @author CoTrang-Lecture
 */
public class Function extends DataAccessHelper{
    private int functionId;
    private String functionName;
    private String functionTypeName;
    private int functionTypeId;
    private String description;
    private String boundaryClass;
    private int roleId;
    private final String GET_BY_TYPE = "select functionName,boundaryClass,functionTypeName from FunctionType,Function where FunctionType.functionTypeId = Function.functionTypeId";
    private final String GET_BY_ROLES = "select functionName,boundaryClass,functionTypeName from  FunctionType,Function where FunctionType.functionTypeId = Function.functionTypeId  and Function.functionId in (select functionId from RoleFunction where RoleFunction.roleId in ("; 
    public static final String GET_BY_ONE_ROLE = "select functionId,functionName from  Function where Function.functionId in (select functionId from RoleFunction where RoleFunction.roleId = ?)"; 
    public static final String GET_BY_DIFF_ONE_ROLE = "select functionId,functionName from  Function where Function.functionId not in (select functionId from RoleFunction where RoleFunction.roleId = ?)"; 
    private final String DELETE = "delete from Function where functionName = ?";
    private final String ADD="INSERT INTO Function (functionName,functionTypeId,description,boundaryClass) VALUES (?,?,?,?)";
    private final String GET_BY_FUNCTION_NAME = "select functionId,functionName,boundaryClass,functionTypeId from Function where functionName = ?";
    private final String UPDATE = "update Function set functionName = ?, description = ?, functionTypeId=?, boundaryClass = ? where functionName=? ";
    /**
    * Hàm này để lấy ra list function theo role và query select của nó.
    * @param roleId là id của trường vai trò
    * @param query  là một câu truy vấn
    * @return ArrayList<Function> là list function trả về từ query
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */    
    public ArrayList<Function> getFunctionByRoleId(int roleId, String query) throws SQLException, ClassNotFoundException{
        ArrayList<Function> functions = new ArrayList<Function>();
        connectDB();      
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, roleId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){            
            Function function = new Function();
            function.setFunctionId(rs.getInt("functionId"));
            function.setFunctionName(rs.getString("functionName"));
            functions.add(function);
        }
        closeDB();
        return functions;
    }
    /**
    *Hàm này để chỉnh sửa thông tin của 1 function theo tên function và vai trò của nó.
    *@param functionName là tên function khi chưa sửa 
    * @param roleId là id của trường vai trò
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */
    public void modify(String functionName,int roleId) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(UPDATE);
        ps.setString(1,this.functionName);
        ps.setInt(3, functionTypeId);
        ps.setString(2, description);
        ps.setString(4, this.boundaryClass);
        ps.setString(5, functionName);

        ps.executeUpdate();
        closeDB();
    }
    /**
    *Hàm này để thêm function vào csdl
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */
    public void add() throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(ADD);
        ps.setString(1,functionName);
        ps.setInt(2, functionTypeId);
        ps.setString(3, description);
        ps.setString(4, boundaryClass);
        ps.executeUpdate();
        closeDB();
    }
    
    /**
    * Hàm này để lấy ra roleId 
    * roleId này thực chất không phải của Function 
    * mà chỉ lưu vào với mục đích sử dụng khác
    * @return int roleId
    */    
    public int getRoleId() {
        return roleId;
    }
    /**
    * Hàm này để gán giá trị cho roleId 
    * roleId này thực chất không phải của Function 
    * mà chỉ lưu vào với mục đích sử dụng khác
    * @param roleId là roleId
    */  
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
    /**
    * Hàm này để lấy ra functionTypeName
    * functionTypeName này thực chất không phải của Function 
    * mà chỉ lưu vào với mục đích sử dụng khác
    * @return String functionTypeName
    */  
    public String getFunctionTypeName() {
        return functionTypeName;
    }
    /**
    * Hàm này để gán giá trị cho functionTypeName
    * functionTypeName này thực chất không phải của Function 
    * mà chỉ lưu vào với mục đích sử dụng khác
    * @param functionTypeName là functionTypeName
    */ 
    public void setFunctionTypeName(String functionTypeName) {
        this.functionTypeName = functionTypeName;
    }
    /**
    * Hàm này để lấy ra functionId trong bảng Funtion
    * @return int functionId
    */ 
    public int getFunctionId() {
        return functionId;
    }
    /**
    * Hàm này để gán giá trị cho functionId trong bảng Function
    * @param functionId là functionId
    */ 
    public void setFunctionId(int functionId) {
        this.functionId = functionId;
    }
    /**
    * Hàm này để lấy ra getFunctionName trong bảng Function
    * @return String là getFunctionName 
    */ 
    public String getFunctionName() {
        return functionName;
    }
    /**
    * Hàm này để gán giá trị cho functionName trong bảng Function
    * @param functionName là functionName
    */ 
    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }
    /**
    * Hàm này để lấy ra functionTypeId trong bảng Function
    * @return int là functionTypeId
    */ 
    public int getFunctionTypeId() {
        return functionTypeId;
    }
    /**
    * Hàm này để gán giá trị cho functionTypeId trong bảng Function
    * @param functionTypeId là functionTypeId
    */ 
    public void setFunctionTypeId(int functionTypeId) {
        this.functionTypeId = functionTypeId;
    }
    /**
    * Hàm này để lấy ra description trong bảng Function
    * @return String là description
    */ 
    public String getDescription() {
        return description;
    }
    /**
    * Hàm này để gán giá trị cho description trong bảng Function
    * @param description là description
    */ 
    public void setDescription(String description) {
        this.description = description;
    }
    /**
    * Hàm này để lấy ra boundaryClass trong bảng Function
    * @return String là boundaryClass
    */ 
    public String getBoundaryClass() {
        return boundaryClass;
    }
    /**
    * Hàm này để gán giá trị cho boundaryClass trong bảng Function
    * @param boundaryClass là boundaryClass
    */ 
    public void setBoundaryClass(String boundaryClass) {
        this.boundaryClass = boundaryClass;
    }
    /**
    *Hàm này để lấy list chức năng theo list các tên vai trò (roles)
    *@param roles list các roleName 
    *@return Map<String,ArrayList<Function>> key là typFunctionName value là list function
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */
    public Map<String,ArrayList<Function>> getFunctionsWithTypeByRoles(String roles) throws SQLException, ClassNotFoundException{
        Map<String,ArrayList<Function>> typeFunctionMap = new  HashMap<String,ArrayList<Function>>();
        connectDB();
        String typeName = new String();
        String query = new String();
        query+= GET_BY_ROLES+roles+"))";
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Function item = new Function();
            item.setFunctionName(rs.getString("functionName"));
            item.setBoundaryClass(rs.getString("boundaryClass"));
            typeName= rs.getString("functionTypeName");
            if(typeFunctionMap.containsKey(typeName)){
                typeFunctionMap.get(typeName).add(item);
            }else{    
                ArrayList<Function> functions = new ArrayList<Function>();
                functions.add(item);
                typeFunctionMap.put(typeName, functions);
            }
        }
        closeDB();
        return typeFunctionMap;
    }
    /**
    *Hàm này để lấy list chức năng theo id của FunctionType
    *@param functionTypeId id của FunctionType 
    *@return ArrayList<Function> là list các function
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */    
    public ArrayList<Function> getFunctionByTypeId(int functionTypeId) throws SQLException, ClassNotFoundException{
        ArrayList<Function> functions = new  ArrayList<Function>();
        String queryForType = " and function.functionTypeId = ? ";
        String query = GET_BY_TYPE;
        if(functionTypeId !=0)
            query+=queryForType;
        connectDB();
        PreparedStatement ps = conn.prepareStatement(query);
        if(functionTypeId !=0)
            ps.setInt(1, functionTypeId);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Function item = new Function();
            item.setFunctionName(rs.getString("functionName"));
            item.setBoundaryClass(rs.getString("boundaryClass"));
            item.setFunctionTypeName(rs.getString("functionTypeName"));
            functions.add(item);
        }            
        closeDB();
        return functions;
    }
    /**
    *Hàm này để xóa chức năng theo tên function
    *@param functionName là tên function 
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */  
    public void deleteFunctionByFunctionName(String functionName) throws SQLException, ClassNotFoundException{
        RoleFunction roleFunction = new RoleFunction();
        getFunctionByFunctionName(functionName);
        roleFunction.deleteByFunctionId(functionId);
        connectDB();
        PreparedStatement ps = conn.prepareStatement(DELETE);
        ps.setString(1, functionName);
        ps.executeUpdate();
        closeDB();
    }
    /**
    *Hàm này để tìm chức năng theo tên Function
    *@param functionName là tên function 
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
    */      
    public void getFunctionByFunctionName(String functionName) throws SQLException, ClassNotFoundException{
        connectDB();           
        PreparedStatement ps = conn.prepareStatement(GET_BY_FUNCTION_NAME);
        ps.setString(1, functionName);
        ResultSet rs = ps.executeQuery();
        if(rs!=null && rs.next()){
            this.functionId= rs.getInt("functionId");
            this.boundaryClass=rs.getString("boundaryClass");
            this.functionName=rs.getString("functionName");
            this.functionTypeId = rs.getInt("functionTypeId");
        }
        closeDB();
    }
}
