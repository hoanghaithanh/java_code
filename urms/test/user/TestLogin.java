/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package user;

import common.TestHelper;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import user.boundary.AccountHelper;
import user.controller.UserController;

/**
 * Class này để kiểm tra chức năng đăng nhập
 * @author CoTrang-Lecture
 */
public class TestLogin {

    /**
     *  Hàm này để khởi tạo test 
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
    @BeforeClass
    public static void setup() throws ParserConfigurationException, SQLException, ClassNotFoundException{
        new TestHelper().intiTest();
    }

    /**
     *  Hàm này để test chức năng đăng nhập
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */
    @Test
    public void testLogin() throws SQLException, ClassNotFoundException{
        assertEquals(4, UserController.getInstance().checkLogin("aaaaaaaaaaaa@gmail.com", "abcd")); 
        assertEquals(3, UserController.getInstance().checkLogin("abc@gmail.com", "abcd")); 
        assertEquals(3, UserController.getInstance().checkLogin("abc1@gmail.com", "abcd")); 
        assertEquals(3, UserController.getInstance().checkLogin("abc2@gmail.com", "abcd")); 
        assertEquals(3, UserController.getInstance().checkLogin("abc3@gmail.com", "abcd")); 
        assertEquals(1, UserController.getInstance().checkLogin("abc@gmail.com", AccountHelper.MD5encrypt("123456A@a")));
        assertEquals(0, UserController.getInstance().checkLogin("abc1@gmail.com", AccountHelper.MD5encrypt("123456A@a"))); 
        assertEquals(0, UserController.getInstance().checkLogin("abc2@gmail.com", AccountHelper.MD5encrypt("123456A@a"))); 
        assertEquals(2, UserController.getInstance().checkLogin("abc3@gmail.com", AccountHelper.MD5encrypt("123456A@a")));
    }
}
