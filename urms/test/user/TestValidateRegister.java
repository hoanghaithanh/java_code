/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package user;

import common.TestHelper;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;
import user.boundary.AccountHelper;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 * Class này để kiểm tra các phương thức validate
 * @author CoTrang-Lecture
 */
public class TestValidateRegister {
    
    /**
     *  Hàm này để kiểm tra phần validate Email
     */
    @Test
    public void TestValidateEmail(){
        assertFalse(AccountHelper.validateEmail("")); 
        assertFalse(AccountHelper.validateEmail("aaaa"));
        assertTrue(AccountHelper.validateEmail("aaaaa@gmail.com")); 
    }
    /**
     *  Hàm này để kiểm tra phần validate PhoneNumber
     */
    @Test
    public void TestValidatePhoneNumber(){
        assertTrue(AccountHelper.validatePhoneNumber("")); 
        assertFalse(AccountHelper.validatePhoneNumber("----------"));
        assertFalse(AccountHelper.validatePhoneNumber(".........."));
        assertFalse(AccountHelper.validatePhoneNumber("......... "));
        assertTrue(AccountHelper.validatePhoneNumber("9.9 9-"));
        assertTrue(AccountHelper.validatePhoneNumber(" 9.9 9-"));
    }
    /**
     *  Hàm này để kiểm tra phần validate Password
     */    
    @Test
    public void TestValidatePassword(){
        // Không nhập gì
        assertFalse(AccountHelper.validatePassword("")); 
        //ít hơn 8 kí tự
        assertFalse(AccountHelper.validatePassword("aa")); 
        //Không có kí tự viết hoa
        assertFalse(AccountHelper.validatePassword("123456a@a"));         
        //Không có kí tự số
        assertFalse(AccountHelper.validatePassword("adfasdfasdf")); 
        // Không có kí tự đặc biệt
        assertFalse(AccountHelper.validatePassword("123456Aaa"));
        // Không có kí tự viêt thường
        assertFalse(AccountHelper.validatePassword("123456A@A"));
        // Đúng định dạng
        assertTrue(AccountHelper.validatePassword("123456A@a"));
        
    }

    /**
     *  Hàm này để kiểm tra phần validate First name hoặc last name
     */    
    @Test
    public void testValidateFirstNameOrLastName(){
        assertEquals(1,AccountHelper.validateFirstNameOrLastName(""));
        String str ="";
        for(int i=0;i<25;i++){
            str+="a";
            assertEquals(0,AccountHelper.validateFirstNameOrLastName(str));
        }
        assertEquals(2,AccountHelper.validateFirstNameOrLastName("123456789123456789123456789"));
    }
}
