/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package common;

import java.io.File;
import java.sql.SQLException;
import javax.xml.parsers.ParserConfigurationException;


/**
 * Class này để điều chỉnh lại đường dẫn cho cơ sở dữ liệu test 
 * và đường dẫn file xml để làm dữ liệu cho cơ sở dữ liệu test
 * @author CoTrang-Lecture
 */
public class TestHelper extends DatabaseTestHelper {
    private  String DBPATH = "jdbc:sqlite:db"+File.separator+"CodeExampleTest.sqlite";    
    private  String []TABLES={"RoleFunction","Function","UserRole","Role","User","FunctionType"};
    private  String xmlPath = "test/common/dbunitData.xml";

    /**
     * Hàm này để đổi đường dẫn cơ sở dữ liệu
     * @param DBPATH là đường dẫn cơ sở dữ liệu
     */  
    public void setDBPATH(String DBPATH) {
        this.DBPATH = DBPATH;
    }
    /**
     * Hàm này để đổi đường dẫn cơ sở dữ liệu
     * @param tables là các bảng trong cơ sở dữ liệu
     * nhập bảng theo thứ tự bảng có reference đến bảng tiếp theo trước
     */  
    public void setTABLES(String[] tables) {
        this.TABLES = tables;
    }
    /**
     * Hàm này để đổi đường dẫn file xml
     * @param xmlPath là đường dẫn file xml
     */  
    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }
    /**
     * Hàm này để khởi tạo một test với các dư liệu DBPATH, TABLES, xmlPath
     * nếu không setDBPATH, setTABLES, setXmlPath thì sẽ
     * sử dụng mặc định.
     * @throws ParserConfigurationException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @see ClassNotFoundException
     * @see SQLException
     * @see ParserConfigurationException
     */  
    public void intiTest() throws ParserConfigurationException, SQLException, ClassNotFoundException {
        setDbPath(DBPATH);
        clear_insert_database(TABLES);
        insert_data_from_xml_file(xmlPath);
    }
    
}
