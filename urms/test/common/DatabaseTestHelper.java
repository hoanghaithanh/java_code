/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package common;

import entity.DataAccessHelper;
import java.sql.SQLException;
import java.sql.Statement;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Class này để xóa các insert lúc trước 
 * và lấy dữ liệu từ file xml cho vào cơ sở dữ liệu test
 * @author CoTrang-Lecture
 */
public class DatabaseTestHelper extends DataAccessHelper{
    private final String DELETE_ALL_RECORD="delete from ";
    /**
     * Hàm này để xóa tất cả các bản ghi trong cơ sở dữ liệu
     * @param tables là các bảng trong cơ sở dữ liệu
     * nhập bảng theo thứ tự bảng có reference đến bảng tiếp theo trước
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     */
    public void clear_insert_database(String[] tables) throws SQLException, ClassNotFoundException{
        connectDB();
        for(String table:tables){
            Statement stmt =conn.createStatement();
            stmt.executeUpdate(DELETE_ALL_RECORD+table);
        }
        closeDB();
    }
    /**
     * Hàm này để đọc file xml và đưa vào cơ sở dữ liệu
     * @param xmlPath là đường dẫn file xml cần đọc
     * @throws javax.xml.parsers.ParserConfigurationException
     * @throws ClassNotFoundException
     * @throws SQLException
     * @see ClassNotFoundException
     * @see SQLException
     * @see javax.xml.parsers.ParserConfigurationException
     */    
    public void insert_data_from_xml_file(String xmlPath) throws ParserConfigurationException, SQLException, ClassNotFoundException{
        ReadXML readXML = new ReadXML();
        readXML.readXmlStoreInDataBase(xmlPath);
    }
}
