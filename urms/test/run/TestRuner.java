/*
 * Bản quyền thuộc về TS. Nguyễn Thị Thu Trang, Giảng viên Viện CNTT-TT, Trường ĐHBK Hà Nội
 * Đây là mã nguồn mẫu được tạo ra nhằm hỗ trợ sinh viên trong các lớp học của TS. N.T.T.Trang 
 * Nếu bất kỳ ai sử dụng hoặc sửa đổi ngoài mục đích học tập trên lớp học của cô Trang, cần có sự cho phép của TS. N.T.T.Trang .
 * Bất kỳ việc sử dụng hoặc sửa đổi chưa được phép đều bị coi là vi phạm bản quyền.
 */
package run;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import user.TestLogin;
import user.TestRegister;
import user.TestValidateRegister;

@RunWith(Suite.class)
@Suite.SuiteClasses({
   TestRegister.class,
   TestValidateRegister.class,
   TestLogin.class
})
/**
 * Class này để chạy tất cả các test
 * muốn chạy được thì phải add các class test cần chạy vào @Suite.SuiteClasses ở trên
 * @author CoTrang-Lecture
 */
public class TestRuner {
}
