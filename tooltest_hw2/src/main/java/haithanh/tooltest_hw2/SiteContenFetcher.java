package haithanh.tooltest_hw2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

public class SiteContenFetcher {
	private String url = "http://dantri.com.vn";
	private String charset = "UTF-8";
	private HttpURLConnection connection;
	private InputStream response;
	public SiteContenFetcher()
	{
		try {
			this.connection = (HttpURLConnection) new  URL(url).openConnection();
			connection.setRequestProperty("Accept-Charset", charset);
			this.connection.setRequestMethod("GET");
			this.connection.connect();
			for (Entry<String, List<String>> header : connection.getHeaderFields().entrySet()) {
			    System.out.println(header.getKey() + "=" + header.getValue());
			    response = connection.getInputStream();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void printSite() throws IOException
	{
		String contentType = connection.getContentType();
		String charset = null;
		String fileName = null;
		for(String param:contentType.replace(" ", "").split(";"))
		{
			if(param.startsWith("charset"))
			{
				charset=param.split("=",2)[1];
			}
		}
		if(charset!=null)
		{
			DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss");
			Date date = new Date();
			fileName = dateFormat.format(date);
			Writer out = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream("./"+fileName+".html"), "UTF-8"));
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(response,charset));
				for(String line; (line=reader.readLine())!=null;)
				{
					System.out.println(line);
					out.write(line);
				}
				out.close();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
