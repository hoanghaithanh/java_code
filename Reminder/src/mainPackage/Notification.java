package mainPackage;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Notification extends JFrame{
	private JLabel lb;
	public Notification(Reminder remind){
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	setSize(300,200);
	setLayout(new FlowLayout());
	
	lb = new JLabel("My label");
	lb.setHorizontalAlignment(JLabel.CENTER);
	add(lb);
	
	JButton btn = new JButton("Continue");
	btn.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent arg0){
			remind.reset();
			setVisible(false);
	}});
	add(btn);
	}
}
